Analysing HLT2 output
=====================

After :doc:`hlt2_line`, it can be useful to analyse the candidates produced
by some HLT2 lines. This means telling Moore to produce an output file, and
then running over this file with a subsequent application, typically DaVinci.

Enabling HLT2 output
--------------------

Moore will write output if it's given a name and type for the output file.
This is done by setting properties on the
`PyConf.application.ApplicationOptions` object passed to `Moore.run_moore`::

    from Moore import options, run_moore

    options.output_file = 'hlt2_example.dst'
    options.output_type = 'ROOT'

    # Assuming the `all_lines` function is defined elsewhere
    run_moore(options, all_lines)

The file ``hlt2_example.dst`` will be produced in the directory you run Moore in.

Output format
-------------

Output objects created by HLT2 lines are placed in the transient event store
(TES) in predefined locations. The lines _candidates_, whose existence
defines whether or not the line fired, are placed in the location::

    /Event/HLT2/<HLT2 line name>/Particles

The HLT2 line name is defined by the string passed to the initialisation of
`Moore.config.HltLine`::

    def d0_to_kpi():
        dzeros = make_dzeros()
        return HltLine(name="Hlt2CharmD0ToKmPipLine", algs=[dzeros])

In this example, the line has the name ``Hlt2CharmD0ToKmPipLine``, so the
candidates (the objects created by the ``dzeros`` algorithm) will be stored
in the output file at::

    /Event/HLT2/Hlt2CharmD0ToKmPipLine/Particles

Extra outputs are stored in a similar hierarchy::

    /Event/HLT2/<HLT2 line name>/<extra selection name>/Particles

Given this line maker::

    def d0_to_kpi():
        dzeros = make_dzeros()
        # Select pions and kaons that vertex well with the trigger candidate
        soft_pions = make_soft_pions(dzeros)
        soft_kaons = make_soft_kaons(dzeros)
        return HltLine(
            name="Hlt2CharmD0ToKmPipLine",
            algs=[dzeros],
            extra_outputs=[
                ("SoftPions", soft_pions),
                ("SoftKaons", soft_kaons),
            ]
        )

The first element of each 2-tuple in `extra_outputs` defines the extra
selection's name. So, the soft pions will be available in the output file
at::

    /Event/HLT2/Hlt2CharmD0ToKmPipLine/SoftPions/Particles

Currently, the locations of other objects in the event, such tracks and
primary vertices, are not guaranteed by Moore and may change in future
versions.

Reading data with DaVinci
-------------------------

Today, the HLT2 output format is similar to that produced by the Run 2
application Tesla. If you've `analysed Turbo data`_ this configuration will look
familiar to you::

    from Configurables import (
        DaVinci,
        DecayTreeTuple,
        UnpackParticlesAndVertices,
    )
    from Gaudi.Configuration import appendPostConfigAction
    from DecayTreeTuple import Configuration
    from PhysSelPython.Selections import (
        AutomaticData,
        CombineSelection,
        SelectionSequence,
    )

    # The addTupleTool machinery tries to mutate this in place, so have to make
    # sure to copy it whenever we need it
    # TODO: explain why we don't just use the default list
    DEFAULT_TUPLE_TOOLS = (
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolGeometry",
    )
    ROOT_IN_TES = "/Event/HLT2"

    # The output of the HLT2 line
    kpi_line = AutomaticData("Hlt2CharmD0ToKmPipLine/Particles")
    # Extra pions
    soft_pions = AutomaticData("Hlt2CharmD0ToKmPipLine/SoftPions/Particles")
    dstars = CombineSelection(
        "CombineD0pi",
        inputs=[kpi_line, soft_pions],
        DecayDescriptors=[
            "D*(2010)+ -> D0 pi+",
            "D*(2010)- -> D0 pi+",
        ],
        DaughtersCuts={
            "pi+": "PT > 250 * MeV",
        },
        CombinationCut=(
            "in_range(0, (AM - AM1 - AM2), 170)"
        ),
        MotherCut=(
            "(VFASPF(VCHI2PDOF) < 10) &"
            "in_range(0, (M - M1 - M2), 150)"
        )
    )
    selseq = SelectionSequence(
        dstars.name() + "Sequence",
        TopSelection=dstars
    )

    dtt_kpi = DecayTreeTuple(
        "TupleD0ToKpi",
        Inputs=[kpi_line.outputLocation()],
        Decay="D0 -> ^K- ^pi+",
        ToolList=list(DEFAULT_TUPLE_TOOLS),
    )
    dtt_kpi.addBranches({
        "D0": "D0 -> K- pi+",
        "D0_h1": "D0 -> ^K- pi+",
        "D0_h2": "D0 -> K- ^pi+",
    })
    dtt_kpi.addTupleTool("TupleToolANNPID").ANNPIDTunes = ["MC15TuneV1"]

    dtt_kpi_dst = DecayTreeTuple(
        "TupleDstToD0pi_D0ToKpi",
        Inputs=[selseq.outputLocation()],
        Decay="[D*(2010)+ -> ^([D0]cc -> ^K- ^pi+) ^pi+]CC",
        ToolList=list(DEFAULT_TUPLE_TOOLS),
    )
    dtt_kpi_dst.addBranches({
        "Dst": "[D*(2010)+ -> ([D0]cc -> K- pi+) pi+]CC",
        "Dst_pi": "[D*(2010)+ -> ([D0]cc -> K- pi+) ^pi+]CC",
        "D0": "[D*(2010)+ -> ^([D0]cc -> K- pi+) pi+]CC",
        "D0_h1": "[D*(2010)+ -> ([D0]cc -> ^K- pi+) pi+]CC",
        "D0_h2": "[D*(2010)+ -> ([D0]cc -> K- ^pi+) pi+]CC",
    })
    dtt_kpi_dst.addTupleTool("TupleToolANNPID").ANNPIDTunes = ["MC15TuneV1"]

    DaVinci().DataType = "Upgrade"
    DaVinci().InputType = "MDST"
    DaVinci().RootInTES = ROOT_IN_TES
    DaVinci().UserAlgorithms = [selseq.sequence(), dtt_kpi, dtt_kpi_dst]
    DaVinci().TupleFile = "hlt2_example.root"

    @appendPostConfigAction
    def unpack_hlt2():
        """Run UnpackParticlesAndVertices on /Event/HLT2.

        This is a temporary measure until support for Run 3 HLT2 output is added to
        DaVinci.
        """
        from Configurables import GaudiSequencer
        unpacker = UnpackParticlesAndVertices(
            InputStream=ROOT_IN_TES,
        )
        GaudiSequencer('DaVinciEventInitSeq').Members.append(unpacker)

These options demonstrate using the output of extra selections. If your line
doesn't produce these, the corresponding options will look a lot simpler (you
will only need the `dtt_kpi` algorithm).

Additional options are needed to define the input data, the database tags, and
the simulation flag. These typically looks like this, but may be different for
your specific use-case::

    from GaudiConf import IOHelper

    DaVinci().Simulation = True
    DaVinci().CondDBtag = "sim-20171127-vc-md100"
    DaVinci().DDDBtag = "dddb-20171126"
    IOHelper().inputFiles(["./hlt2_example.dst"])

.. _analysed Turbo data: https://twiki.cern.ch/twiki/bin/view/LHCb/MakeNTupleFromTurbo
