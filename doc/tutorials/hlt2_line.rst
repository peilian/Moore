Writing an HLT2 line
====================

.. |lb-decay| replace:: :math:`\Lambda_{b}^{0} \to \Lambda_{c}^{+} \pi^{+}`
.. |lc-decay| replace:: :math:`\Lambda_{c}^{+} \to p K^{-} \pi^{+}`
.. |lb| replace:: :math:`\Lambda_{b}^{0}`
.. |lc| replace:: :math:`\Lambda_{c}^{+}`

An HLT *line* is a sequence of steps that collectively define whether an event
contains an object of interest which should be kept for later analysis. This
object is typically a reconstructed candidate physics process, such as an
exclusive particle decay.

This page will walk you through defining an HLT2 line step by step. We'll
reconstruct candidate |lb-decay| decays with |lc-decay|, explaining the details
of how to encode this within Moore.

To follow along, it's expected that you have a :doc:`development setup <developing>` built and
ready to go.

File structure
--------------

Each HLT2 line is defined by a single small Python function, a 'line
definition'. Line definitions for HLT2 live in files under this directory in
the Moore project::

    Hlt/Hlt2Conf/python/Hlt2Conf/lines

First look there to get a sense of how things are structured. Some files are
further organised into sub-folders. For us, we can just create a file directly
under ``lines``::

    touch Hlt/Hlt2Conf/python/Hlt2Conf/lines/LbToLcPi.py

Open the newly-created file in your text editor of choice.

Prototyping
-----------

Think about how you would reconstruct candidates for our decay of interest,
|lb-decay| with |lc-decay|. This is always the first step before writing any
code. How could we do it?

1. Reconstruct protons, kaons, and pions needed for the |lc| candidates.
2. Filter those candidates if necessary.
3. Reconstruct |lc-decay| candidates.
4. Reconstruct pion candidates needed for the |lb| candidates.
5. Filter the pion candidates if necessary.
6. Reconstruct |lb-decay| candidates.

Each step, like 'reconstruct proton', 'filter proton', and 'reconstruct |lc|',
represents the running of an *algorithm*, a C++ component defined within the
LHCb selection framework. We want to *configure* these algorithms so they
behave in a way that creates the candidates we want.

But there is one step missing here, which historically has been implicit:
reconstruct primary vertex (PV) candidates! These are necessary if we want to
cut on quantities releted to PVs such as the impact parameter and flight
distance.

So, let's outline a function that does these steps::

    def lbtolcpi_lctopkpi_line():
        pvs = make_pvs()
        protons = filter_protons(make_protons(), pvs)
        kaons = filter_kaons(make_kaons(), pvs)
        pions = filter_pions(make_pions(), pvs)
        lcs = make_lambdacs(protons, kaons, pions, pvs)
        lbs = make_lambdabs(lcs, pions, pvs)

        return lbs

This is a step-by-step encoding of what we want our line to do. Of course, this
version doesn't run yet because we haven't defined the various ``make_`` and
``filter_`` functions yet, and it's not clear what will happen to the return
value ``lbs``, but this function is already very close to what our final
function will look like.

.. note::

    We 'skipped' steps 4 and 5 in our outline because we'll assume that for
    *this* line the pions used for the |lc| also meet our criteria for |lb|
    pions.

    What would our function look like if this wasn't the case?

There are just a few changes we need to make to our file and function to be
consistent with what Moore expects.

1. Return an object that Moore understands from the function.
2. Define a 'line registry' object that will hold all of the lines defined
   within this file. Moore will expect this object to be present, and will use
   it to discover all lines it should run.
3. Add our line definition function to this registry.

Line declaration
^^^^^^^^^^^^^^^^

The first step means returning a `Moore.config.HltLine` object. This contains
some metadata about the information that Moore will use, such as a name, in
addition to the :doc:`control flow <../pyconf/control_flow>` defining the
line. Let's return that object first and then discuss it::

    from Moore.config import HltLine


    def lbtolcpi_lctopkpi_line(name="Hlt2LbToLcpPim_LcToPpKmPipLine", prescale=1):
        pvs = make_pvs()
        protons = filter_protons(make_protons(), pvs)
        kaons = filter_kaons(make_kaons(), pvs)
        pions = filter_pions(make_pions(), pvs)
        lcs = make_lambdacs(protons, kaons, pions, pvs)
        lbs = make_lambdabs(lcs, pions, pvs)

        return HltLine(
            name=name,
            algs=[lbs],
            prescale=prescale,
         )

There are three new things going on:

1. The `HltLine` object needs to be created with a name, so we've parameterised
   this as a function argument, with a default value, and passed it to
   `HltLine`.
2. The `HltLine` object also needs to be created with a prescale, so we've
   parameterised this in a similar way as for the name.
3. Finally, we defined the *control flow* of the line as the ``algs`` parameter
   of `HltLine`. The control flow specifies how Moore should evaluate whether
   this line made a positive *decision* or not. The filters of a line are
   executed in the order defined by the control flow, and the execution of a
   line is aborted if a filter fails. The control flow order can be used to
   optimize the execution time of a line. For our line, the decision is decided
   solely by the presence of |lb| candidates: if we created a non-zero number
   of |lb| candidates, this line should be considered as having 'passed' (also
   called 'fired').

Parameterising the function in the way we have allows us to easily create
multiple lines with different names and prescales, just by calling the
function with different arguments. We'll see later :ref:`how to run multiple
instances of the line with different cuts <modifying_thresholds>`.

.. note::

    How should you decide what name to give your line? The `conventions are
    outlined in Moore#60 <https://gitlab.cern.ch/lhcb/Moore/issues/60>`_. If
    you're still unsure, just open your merge request and someone will make
    suggestions.

Control and data flow
"""""""""""""""""""""

We didn't need to specify that the other algorithms should run, like the
creation of the |lc| candidate. Why is this? It's because Moore makes the
distinction between control flow and data flow.

:doc:`Control flow <../pyconf/control_flow>` defines whether decisions are
positive (passed) or negative (failed). The control flow may, for example,
specify that several algorithms should be run and that the decision should be
positive if at least one algorithm reports as passing.

The :doc:`data flow <../pyconf/algorithms_tools_data_flow>` defines which
inputs are necessary to create a given output.  In order to evaluate the
control flow in our line, Moore needs to run the |lb| making algorithm. Before
doing that it will *automatically deduce* what other algorithms it needs to run
in order to satisfy the inputs to the |lb| algorithm. One input is the output
of the |lc| algorithm, and Moore will likewise automatically deduce what
algorithms need to run to produce the required input (that is: the proton,
kaon, and pion makers).  This automatic data flow resolution goes all the way
up through the reconstruction to the raw event.

We could choose to impose additional requirements on the control flow if it
makes physics sense for our line. For example:

1. At least one PV must be present in the event; or
2. Some global event cut (GEC) must be satisfied, for example the number of
   clusters in the tracking stations should be below some value.

Because we already have a PV making algorithm in our prototype, we could
include this in our control flow already::

        return HltLine(
            name=name,
            algs=[pvs, lbs],
            prescale=prescale,
         )

Moore will define the control flow for this line to be:

    "First require the ``pvs`` algorithm to pass, and then require the ``lbs``
    algorithm to pass; if both pass then the line decision is positive."

We will omit this extra requirement for simplicity here, though.

.. note::

    The entries in our ``algs`` list define the entries in a single *control
    flow node*. This node has a 'logic' of 'lazy and'. All possible node logics
    are defined by `PyConf.control_flow.NodeLogic`.

    The total HLT2 decision is defined by a control flow node containing each
    individual line's node, with a logic of 'non-lazy or'. Most line authors
    don't have to worry about the different types.

Line registration
^^^^^^^^^^^^^^^^^

The last two steps we need to make to our prototype function are rather
straightforward. The line registry object is just a Python dictionary, and this
is populated by using the Python decorator syntax with the
`Moore.config.register_line_builder` helper::

    from Moore.config import HltLine, register_line_builder

    all_lines = {}


    @register_line_builder(all_lines)
    def lbtolcpi_lctopkpi_line(name="Hlt2LbToLcpPim_LcToPpKmPipLine", prescale=1):
        pvs = make_pvs()
        protons = filter_protons(make_protons(), pvs)
        kaons = filter_kaons(make_kaons(), pvs)
        pions = filter_pions(make_pions(), pvs)
        lcs = make_lambdacs(protons, kaons, pions, pvs)
        lbs = make_lambdabs(lcs, pions, pvs)

        return HltLine(
            name=name,
            algs=[lbs],
            prescale=prescale,
         )

The decorator just adds the line *function* to the dictionary. We'll see later
how this dictionary is used to run the line.

TODO: link to 'later'.

.. note::

    It's now worth taking a step back to look at the function we've written,
    because this contains all the core ideas we'll need. All other HLT2 lines
    you'll see look very similar to what we have now.

Standard objects
----------------

In HLT2, several maker functions are already defined for general usage. These
'standard makers' take the output of the reconstruction and make objects common
to many HLT2 lines. These standard makers produces objects such as:

* Charged tracks with predefined mass hypotheses and associated PID objects
* Neutral objects such as photons and neutral pions
* Composite objects, representing candidate particle decays, such as
  :math:`J/\psi \to \mu^{+} \mu^{-}` and :math:`\Lambda^{0} \to p \pi^{-}`.

Before writing your own maker, you should always first browse :ref:`the list of
standard makers <hlt2_standard_makers>` to see if something already exists that
suits your needs. Having all HLT2 lines re-use the same makers reduces the
number of unique algorithms that Moore has to run, reducing the total
time-per-event.

For our use case, we can see there are already makers we can use for charged
non-composite inputs:

* Protons: `make_has_rich_long_protons <Hlt2Conf.standard_particles.make_has_rich_long_protons>`
* Kaons: `make_has_rich_long_kaons <Hlt2Conf.standard_particles.make_has_rich_long_kaons>`
* Pions: `make_has_rich_long_pions <Hlt2Conf.standard_particles.make_has_rich_long_pions>`

We've chosen the `has_rich` variant because in this example we will apply PID
cuts to all non-composite particles, so it makes sense to first require that
the objects have passed the requirements needed to assign PID likelihood
values.

Primary vertices are also part of the set of standard objects, produced by the
`make_pvs <RecoConf.reco_objects_from_file.make_pvs>` function.

Given this information, we can flesh out our function a little bit more::

    from Moore.config import HltLine, register_line_builder

    from ..standard_particles import (
        make_has_rich_long_kaons,
        make_has_rich_long_pions,
        make_has_rich_long_protons,
    )
    from RecoConf.reco_objects_from_file import make_pvs

    all_lines = {}


    @register_line_builder(all_lines)
    def lbtolcpi_lctopkpi_line(name="Hlt2LbToLcpPim_LcToPpKmPipLine", prescale=1):
        pvs = make_pvs()
        protons = filter_protons(make_has_rich_long_protons(), pvs)
        kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
        pions = filter_pions(make_has_rich_long_pions(), pvs)
        lcs = make_lambdacs(protons, kaons, pions, pvs)
        lbs = make_lambdabs(lcs, pions, pvs)

        return HltLine(
            name=name,
            algs=[lbs],
            prescale=prescale,
         )

Filters and combiners
---------------------

We're nearly there! What's left is to define the various ``filter_`` and
``make_`` placeholders.

Think about what these functions should do. They need to take input, as we've
written it in our prototype, configure the correct type of algorithm, and then
return something.

Let's start with the particle filters.
We could consider allowing for customisation of the
algorithms with additional arguments::

    from GaudiKernel.SystemOfUnits import GeV

    from ..algorithms import require_all, ParticleFilterWithPVs


    def filter_protons(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, dllp_min=5):
        code = require_all(
            'PT > {pt_min}',
            'MIPCHI2DV(PRIMARY) > {mipchi2_min}',
            'PIDp > {dllp_min}').format(
                pt_min=pt_min,
                mipchi2_min=mipchi2_min,
                dllp_min=dllp_min)
        return ParticleFilterWithPVs(particles, pvs, Code=code)

We've used the `require_all <Hlt2Conf.algorithms.require_all>` helper to define
the cut string here.

.. note::

    Why parameterise exactly these things as arguments, and not others, such as the
    full ``Code`` string? Like writing any other function, you need to consider
    what behaviour should be configurable. For defining physics selections, there
    is typically some particular *intent* an author has when writing code. In this
    instance, we *want* these particles to be displaced with respect to any PV, so
    there's an impact parameter cut. Another physicist reading this function will
    understand that the *intent* is to produce displaced protons. Thinking about
    the 'interface' of a function in this way can help make the code easier to
    understand and reason about.

The return value is the configured algorithm. This can be used as an 'input' to
other algorithms as the framework knows how to extract the (single) output the
``ParticleFilterWithPVs`` algorithm produces.

Define similar functions for the remaining particle filters, for kaons and for
pions.

Next is a function which combines its input to a composite |lc| candidate::

    import math

    from GaudiKernel.SystemOfUnits import MeV, mm

    from ..algorithms import ParticleCombinerWithPVs


    def make_lambdacs(protons,
                      kaons,
                      pions,
                      pvs,
                      am_min=2080 * MeV,
                      am_max=2480 * MeV,
                      apt_min=2000 * MeV,
                      amindoca_max=0.1 * mm,
                      vchi2pdof_max=10,
                      bpvvdchi2_min=25):
        combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                       "APT > {apt_min}",
                                       "AMINDOCA('') < {amindoca_max}").format(
                                           am_min=am_min,
                                           am_max=am_max,
                                           apt_min=apt_min,
                                           amindoca_max=amindoca_max)

        vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                                  "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                      vchi2pdof_max=vchi2pdof_max,
                                      bpvvdchi2_min=bpvvdchi2_min)

        return ParticleCombinerWithPVs(
            particles=[protons, kaons, pions],
            pvs=pvs,
            DecayDescriptors=["[Lambda_c+ -> p+ K- pi+]cc"],
            CombinationCut=combination_code,
            MotherCut=vertex_code)

The concepts here follow on from the filter example, so we won't dwell on it
for long, other than to point out that we chose *not* to parameterise the
``DecayDescriptors`` property of the combiner algorithm. As explained above,
that's because our *intent* in defining this function is to make a combiner for
a specific |lc| decay.

Finally, define a maker function for the |lb| candidates.

.. note::

    You might be asking what these ``ParticleFilterWithPVs`` and 
    ``ParticleCombinerWithPVs`` algorithms are. They are wrappers around the
    ``FilterDesktop`` and ``CombineParticles`` algorithms, created to allow the
    PyConf framework to understand these legacy implementations. The wrappers
    will eventually be replaced by functional counterparts.

    The counterpart algorithms without ``WithPVs`` are identical, except the
    wrappers don't require the ``pvs`` argument. Eventually these wrappers will
    be replaced by new algorithms that have explicit data dependencies (unlike
    ``CombineParticles`` and friends, which are implicit). If you're not sure
    which one to use, choose the ``WithPVs`` variant, as there's no cost for
    now.

Running
-------

With everything in place, we have prety much everything we need to run the
line. The remaining piece is an options file that configures Moore with our
line maker function. Place this in a file called ``test_line.py`` in your
working directory::

    from Moore import options, run_moore
    from Hlt2Conf.lines.LbToLcPi import lbtolcpi_lctopkpi_line


    def all_lines():
        return [lbtolcpi_lctopkpi_line()]


    options.set_conds_from_testfiledb('Upgrade_MinBias_LDST')
    options.set_input_from_testfiledb('Upgrade_MinBias_LDST')
    options.input_raw_format = 4.3
    options.evt_max = 100

    run_moore(options, all_lines)

Most of the pieces we've used here are explained in the :doc:`running` page.
All we've done is tell Moore to run with its default configuration, using our
line maker function to create the only line that it should run, and defined an
input to use from the test file database.

Moore needs to know the input file when running, so we'll just an Upgrade
minimum bias input data options file that comes with Moore::

    ./Moore/run gaudirun.py '$MOOREROOT/tests/options/default_input_and_conds_hlt2.py' test_line.py

With any luck this will run, but it will soon fail with an error.

.. _upfront_reco:

Upfront reconstruction
^^^^^^^^^^^^^^^^^^^^^^

The example we've made will run, but upon inspecting the logfile you'll notice
that no candidates are produced by any algorithm.

This is because the HLT2 reconstruction is not fully defined in a way that
allows us to use it with our example as-is. Instead, we need to modify our line
definition only slightly to explicitly create the reconstruction before we start
building our candidate::

    from RecoConf.reco_objects_from_file import upfront_reconstruction

    # ...

    @register_line_builder(all_lines)
    def lbtolcpi_lctopkpi_line(name="Hlt2LbToLcpPim_LcToPpKmPipLine", prescale=1):
        # ...

        return HltLine(
            name=name,
            algs=upfront_reconstruction() + [lbs],
            prescale=prescale,
         )

This is just a temporary measure until the full HLT2 reconstruction is defined
in Moore. You should include it for now.

Re-running
^^^^^^^^^^

Run again and you'll see the command complete successfully. Look at the log and
see how many candidates were created. Seeing as we're running over minimum bias
data, you should expect to see very few candidates (ideally zero).

Use the instructions in :ref:`analysing-output` section to find the commands
for generating and inspecting the control and data flow graphs that are
produced when the options were ran. The data flow for our example looks like
this:

.. graphviz:: ../graphviz/hlt2_line_example_data_flow.gv

Whilst the control flow looks like this:

.. graphviz:: ../graphviz/hlt2_line_example_control_flow.gv

The control flow node for our HLT2 line contains quite a few steps. Most of
these are the :ref:`upfront reconstruction we added earlier <upfront_reco>`.
Again, the presence of these is just a detail for now; in the near future the
control flow for a line will look much simpler, for our line being just the
|lb| combiner algorithm.

Full example
------------

A full implementation example of the line described here can be found at
``doc/scripts/hlt2_line_example.py``. Have a look at this and see how it
differs from yours. In particuar, see how the imports have been organised near
the top of the file, and everything has a consistent look.

Try to follow these elements from the example in your own line. Remember that
it is *your* line, and you should feel free to really *own* it. Show off and
make it nice!

.. _modifying_thresholds:

Modifying thresholds
--------------------

A common task when developing lines and when running the trigger is to modify
cut values. This can be to increase or decrease the rate of the selection when
running, or to fix a bug.

There are two ways to do this. The first is simple: edit the source! The values
in the source signal your intent, and if your intent changes, so should the
source. (The alternative is to have the values in the source and the 'actual
values' that are used by running elsewhere, which can be confusing.)

What if you wanted to run a couple of instances of this line, but one with the
standard cuts and one with some thresholds slightly modified? This can be
acheived by using the `@configurable <PyConf.tonic.configurable>` decorator::

    @configurable
    def filter_protons(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, dllp_min=5):
        # ...

Functions decorated in this way can be configured using the ``with...bind``
syntax::

    from LbToLcPi import filter_protons,


    def all_lines():
        standard_line = lbtolcpi_lctopkpi_line()

        with filter_protons.bind(dllp_min=-999):
            modified_line = lbtolcpi_lctopkpi_line(name="Hlt2DifferentNameLine")

        return [standard_line, modified_line]

    run_moore(options, all_lines)

This changes any calls to the function ``filter_protons`` within the ``with``
context to override the value of the ``dllp_min=-999``. The advantages of using
this approach are:

1. You don't need to modify the source code (but it is often better to do
   that!).
2. You don't need to 'expose' everything you want to change on the top-level
   line maker, you just modify the behaviour of ``@configurable`` functions
   directly.

.. note::

    Many of the components of the two lines we are now running are identical,
    such as the kaon and pion makers. Will this configuration end up running
    two versions of each maker that do exactly the same thing? No!

    The configuration framework resolves two identically-configured algorithms
    to the same underlying object. This means that the second time the kaon
    maker is configured (identically to the first time), we will get exactly
    the same instance back as the first time. There is then only one instance
    that's added in the dataflow, and the scheduler will see this and know not
    to run the same thing twice.

    The algorithms 'downstream' of the proton makers, such as the |lc|
    combiner, *will* be different, because "configuration" includes the tree of
    all inputs, and one of them (the proton maker) is different between the two
    lines.

Further work
------------

An important aspect of authoring an HLT2 line is stepping back and
spotting instances of code duplication. Multiple instances of the
same intent can be refactored into a common function. This reduces
any maintanence burden and decreases the likelihood of two
implementations slowly drifting apart over time (if someone changes
one but does not know about the existence of the other).

There is some redundancy is our functions, particularly in the ``filter_`` ones
where some selections and thresholds are duplicated. If our *intent* is that
the momentum and impact parameters cut should be the same across all final
state particles (protons, kaons, and pions) then it makes sense to encode this
as a common function::

    @configurable
    def filter_particles(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, pid=None):
        code = require_all(
            'PT > {pt_min}',
            'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                pt_min=pt_min,
                mipchi2_min=mipchi2_min)
        # Add the PID cut
        if pid is not None:
            code = require_all(code, pid)
        return ParticleFilterWithPVs(particles, pvs, Code=code)

We still want to expose the PID cut for configuration, so we change the
original ``filter_protons`` function to delegate to the new filter maker::

    @configurable
    def filter_protons(particles, pvs, dllp_min=5):
        code = 'PIDp > {dllp_min}'.format(dllp_min=dllp_min)
        return filter_particles(particles, pvs, pid=code)

There are a few things to note about this implementation:

1. The way we've refactored the common logic means that there is still only a
   single filter algorithm. We could have ran one filter for the 'common'
   selection, and a second for the PID selection, but our way is more
   efficient.
2. Both functions are decorated as ``@configurable``, so we can still use the
   ``with...bind`` syntax to configure all aspects of the selection.
3. We've ended up with two very small functions with very little logic in them.
   This style is typical of HLT2 lines, and is done on purpose to try to
   improve readability and allow for simpler debugging and reasoning.

Conventions
-----------

Using conventions gives our file a consistent style throughout, helping future
readers (including ourselves) to parse what's happening, and know how to change
things. The :ref:`Moore coding conventions <coding_conventions>` give an
overview of Python-specific things, and you should try to get a feeling for
these conventions by looking at the files already in Moore. We can also look at
the file and see what conventions we've followed. 

Naming
^^^^^^

Almost of our names, like function names, function arguments, variables within
functions, and file names, are in lowercase `snake case`_ (like
``snake_case``). Exceptions are module-level constants, which are in uppercase
snake case (like ``SNAKE_CASE``), and class names, which are in `camel case`_
(like ``CamelCase``).

Threshold names are of the format ``<variable>_<threshold type>``. Examples are:

* ``pt_min``/``pt_max`` for a lower/upper bound transverse momentum cut ``PT >
  {pt_min}``.
* ``hasrich`` for a predicate (true/false) requirement ``HASRICH ==
  {hasrich}``.

HLT2 line names, the ``name`` argument you give to the `HltLine` constructor,
also have their own conventions. These are discussed in `Moore#60`_.

Style
^^^^^

Units are given in the definition of the default value, not in the cut. Don't
do this::

    def make_kaons(pt_min=500):
        return KaonMaker(Cut="PT > {pt_min} * MeV".format(pt_min=pt_min))

Do this::

    def make_kaons(pt_min=500 * MeV):
        return KaonMaker(Cut="PT > {pt_min} * MeV".format(pt_min=pt_min))

Python module imports should be grouped together at the top of the file. These
are typically grouped by 'source' (first Python standard library, then from
Gaudi/``Configurables``, then ``PyConf`` or Moore, then your own code) and then
alphabetically within groups.

Structure
^^^^^^^^^

Feel free to use Python modules to structure your code. For example, if you're
defining many lines that use common maker functions or helpers, you can move
these to other files and import them into your line definition files. The
``b_to_open_charm`` HLT2 line module demonstrates these practices rather well.

Next steps
----------

When writing lines it's extremely useful to be able to be able to
:ref:`analyse the output files and log <analysing-output>`. It's also helpful
to refer to the documentation on :doc:`debugging`. Once your ready to start
physics performance studies, you can start :doc:`hlt2_analysis`.

Differences from Run 2 configurations
-------------------------------------

This section is yet to be written. If you have some tips and tricks on porting
Run 2 Stripping and HLT2 lines to Moore, please open a merge request!

.. _snake case: https://en.wikipedia.org/wiki/Snake_case
.. _camel case: https://en.wikipedia.org/wiki/Camel_case
.. _Moore#60: https://gitlab.cern.ch/lhcb/Moore/issues/60
