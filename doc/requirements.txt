# Versions 2 and up require Python 3.5+
sphinx==1.8.5
sphinx_rtd_theme==0.4.3
# PyConf dependencies, usually taken from LCG96
wrapt==1.11.1
