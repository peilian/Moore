Welcome to Moore's documentation!
=================================

Moore is the LHCb high-level trigger (HLT) application. It is responsible for
filtering an input rate of 40 million collisions per second down to an output
rate of around 100 kHz. It does this in two stages:

1. HLT1, which performs a fast track reconstruction and makes a decision based
   on one- and two-track objects.
2. HLT2, which performs a high-fidelity reconstruction and makes a decision
   based on the full detector read-out information.

This site documents the various aspects of Moore, which is fundametally a group
of Python packages that configure algorithms, tools, data flow, and control
flow in order to run a `Gaudi`_-based application.

.. _Gaudi: https://gitlab.cern.ch/gaudi/Gaudi

.. toctree::
   :caption: Reconstruction
   :hidden:
   :maxdepth: 1

   reconstruction/hlt1
   reconstruction/hlt2

.. toctree::
   :caption: Selection
   :hidden:
   :maxdepth: 1

   selection/hlt1
   selection/hlt2

.. toctree::
   :caption: Tutorials
   :hidden:
   :maxdepth: 1

   tutorials/running
   tutorials/debugging
   tutorials/developing
   tutorials/hlt2_line
   tutorials/hlt2_analysis
   tutorials/documentation
   tutorials/hlt1_tracking_performance 

.. toctree::
   :caption: Moore
   :hidden:
   :maxdepth: 1

   moore/application
   moore/lines

.. toctree::
   :caption: PyConf
   :hidden:
   :maxdepth: 1

   pyconf/summary
   pyconf/tonic
   pyconf/algorithms_tools_data_flow
   pyconf/control_flow

.. toctree::
  :caption: RecoConf
  :hidden:
  :maxdepth: 1

  recoconf/recoconf
  recoconf/standalone
  recoconf/hlt1_tracking
  recoconf/hlt2_tracking


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
