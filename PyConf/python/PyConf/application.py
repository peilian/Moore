###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import datetime
import logging
import math
import os
from collections import OrderedDict
from six import string_types
import pydot
import six

from Gaudi.Configuration import appendPostConfigAction, ConfigurableUser, INFO
import GaudiKernel.ProcessJobOptions
from GaudiConf import IOHelper
from RawEventFormat import Raw_location_db

from . import ConfigurationError
from .components import Algorithm, setup_component, is_algorithm, force_location
from .control_flow import CompositeNode
from .dataflow import dataflow_config
from .tonic import configurable

from .Algorithms import (
    createODIN,
    LHCb__MDFWriter,
    OutputStream,
    CallgrindProfile,
)
from Configurables import (
    # FIXME(NN): We shouldn't need to refer to IOV explicitly in our framework
    LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV,
    LHCb__MDF__IOAlg,
    LHCb__Tests__FakeEventTimeProducer as DummyEventTime,
    Gaudi__Hive__FetchDataFromFile,
    ApplicationMgr,
    NTupleSvc,
    DataOnDemandSvc,
)

log = logging.getLogger(__name__)
MDF_KEY = 'MDF'
ROOT_KEY = 'ROOT'
# Writer algorithm and configuration used for a given file type
KNOWN_WRITERS = {
    MDF_KEY: (LHCb__MDFWriter, (('BankLocation', '/Event/DAQ/RawEvent'), )),
    # FIXME(AP): InputCopyStream is broken because it uses incidents to
    # try to ensure all data it writes originated from the same input
    # file
    # ROOT_KEY: (InputCopyStream, ()),
    ROOT_KEY: (OutputStream, (('ItemList', []), )),
}


def output_writers(options, item_list=None):
    """Return a list of configured file writer instances.

    Args:
      options (ApplicationOptions): :class:`ApplicationOptions` containing type and path of the output file.
      item_list (list of str): Mandatory locations to write when using ROOT writers.
        Ignored when writing MDF files.
    """
    options.finalize()
    if not options.output_file:
        return []
    writer_cls, kwargset = KNOWN_WRITERS[options.output_type]
    # Convert immutable set of defaults to mutable dict
    kwargs = dict(kwargset)
    if options.output_type == ROOT_KEY:
        output = "DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
            options.output_file)
        kwargs['Output'] = output
        kwargs['OptItemList'] = item_list or []
    elif options.output_type == MDF_KEY:
        connection = 'file://{}'.format(options.output_file)
        kwargs['Connection'] = connection
    else:
        assert False, 'Do not know output type {}'.format(options.output_type)
    return [writer_cls(**kwargs)]


def setup_python_logging(level):
    """Configure the python logging verbosity."""
    log.info('Disabling python logging messages below threshold')
    # Never print messages below threshold. Using step=99999 ensures
    # that PrintOn elsewhere calls will most likely not change this.
    GaudiKernel.ProcessJobOptions.PrintOff(step=99999)
    # Set the desired level of messages to be printed
    GaudiKernel.ProcessJobOptions.GetConsoleHandler().enable(level)


_ApplicationOptions_locked = False


class ApplicationOptions(ConfigurableUser):
    """Holder for application configuration.

    Configuration can be mutated until `.finalize()` is called. At that
    point any dynamic defaults based on other properties are resolved.
    """
    # FIXME(RM) we can improve upon the base class ConfigurableUser by creating
    # a new base clase that
    # - it more pythonic (e.g. no __slots__, __doc__)
    # - does better type checking of the properties
    # - makes `c.prop` work if prop hasn't been set
    # - supports namespaced/grouped options
    # - forbids a subsequent instantiation of ApplicationOptions such that one
    #   has to do `from ... import app` rather than
    #   `from ... import ApplicationOptions; ApplicationOptions().xxx`
    # - nicer printout of the options
    # - by default do not overwrite, i.e.
    #   opts.evt_max = 1; opts.evt_max = 2; assert opts.evt_max == 1;
    #   opts.evt_max.force(3); assert opts.evt_max == 3
    #   OR
    #   opts.evt_max.set_if_not_set(x) OR opts.evt_max.default(x)
    # - ...

    __slots__ = {
        # input related
        'input_files': [],  # type is list of str
        'input_type': '',  # TODO use enum
        'input_raw_format': 0.3,
        'use_iosvc': False,
        'evt_max': -1,
        'print_freq': 10000,
        # number of events to pre-fetch if use_iosvc=True, the default value
        # is reasonable for most machines; it might need to be increased for
        # more modern/powerful machines
        'buffer_events': 20000,
        # conditions for processing
        'data_type': 'Upgrade',
        'dddb_tag': '',
        'conddb_tag': '',
        'simulation': True,
        # output related
        'output_file': '',
        'output_type': '',
        'histo_file': '',
        'ntuple_file': '',
        'output_level': INFO,
        'python_logging_level': logging.WARNING,
        # multithreaded processing
        'n_threads': 1,
        'event_store': 'HiveWhiteBoard',  # one of EvtStoreSvc, HiveWhiteBoard
        'n_event_slots': -1,  # defaults to 1.2 * n_threads
        # if false scheduler calls Algorithm::execute instead of Algorithm::sysExecute,
        # which breaks some non-functional algorithms
        'scheduler_legacy_mode': True,
        # estimated size of the per-event memory pool. set to zero to disable the pool.
        'memory_pool_size': 10 * 1024 * 1024,
        # TODO such defaults can be expressed here explicitly
        # debugging
        # Control flow file name; not generated if empty
        'control_flow_file': 'control_flow.gv',
        # Data flow file name; not generated if empty
        'data_flow_file': 'data_flow.gv',
        # Enables callgrind profiling
        'callgrind_profile': False,
    }

    _propertyDocDct = {
        'input_raw_format':
        ('sets the expected raw input format (splitting)'
         'See definitions at '
         'https://gitlab.cern.ch/lhcb-datapkg/RawEventFormat/blob/master/python/RawEventFormat/__init__.py'
         ),
        'use_iosvc':
        'Use an alternative, faster IIOSvc implementation for MDFs.',
        'event_store':
        'Event store implementation: HiveWhiteBoard (default) or EvtStoreSvc (faster).'
    }

    def _validate(self):
        """Raise an exception if the options are not consistent."""
        if self.input_files:
            if self.input_type != 'MDF' and self.n_event_slots > 1:
                # TODO use a different exception here
                raise ConfigurationError(
                    "only MDF files can run in multithreaded mode, please "
                    "change number of eventslots to 1")

        if self.output_file:
            assert self.n_event_slots == 1, 'Cannot write output multithreaded'
            assert self.n_threads == 1, 'Cannot write output multithreaded'

            assert self.output_type in KNOWN_WRITERS, (
                'Output filetype not supported: {}'.format(self.output_type))

    def _set_defaults(self):
        if self.n_event_slots <= 0:
            self.n_event_slots = (math.ceil(1.2 * self.n_threads)
                                  if self.n_threads > 1 else 1)

    def finalize(self):
        """Finalize configuration and prevent further changes."""
        global _ApplicationOptions_locked
        if _ApplicationOptions_locked:
            return
        _ApplicationOptions_locked = True

        # Workaround ConfigurableUser limitation
        for name, default in self.getDefaultProperties().items():
            if not self.isPropertySet(name):
                self.setProp(name, default)

        setup_python_logging(self.python_logging_level)

        self._set_defaults()
        self._validate()
        print(self)

    def set_conds_from_testfiledb(self, key):
        """Set tags, data type, etc. from a TestFileDB entry.

           Args:
             key (str): Key in the TestFileDB.
        """
        from PRConfig.TestFileDB import test_file_db

        qualifiers = test_file_db[key].qualifiers
        self.data_type = qualifiers['DataType']
        self.simulation = qualifiers['Simulation']
        self.dddb_tag = qualifiers['DDDB']
        self.conddb_tag = qualifiers['CondDB']

    def set_input_from_testfiledb(self, key):
        """Set input and tags, data type, etc. from a TestFileDB entry.

        Args:
          key (str): Key in the TestFileDB.
        """
        from PRConfig.TestFileDB import test_file_db
        file_format = test_file_db[key].qualifiers['Format']
        self.input_files = test_file_db[key].filenames
        self.input_type = 'ROOT' if file_format != 'MDF' else file_format

    def applyConf(self):
        # we should not do anything here
        raise NotImplementedError(
            'The {} configurable should not be called'.format(
                self.__class__.__name__))


def all_algs(alg):
    return sum((all_algs(i.producer) for i in alg.inputs.values()), [alg])


def all_nodes_and_algs(node, recurse_algs=False):
    """Return the list of all reachable nodes and algorithms."""
    if not isinstance(node, CompositeNode):
        raise TypeError('{} is not of type CompositeNode'.format(node))
    # use OrderedDict as there is no OrderedSet is the standard library
    nodes = OrderedDict()
    algs = OrderedDict()
    _all_nodes_and_algs(node, recurse_algs, nodes, algs)
    return list(nodes), list(algs)


def _all_nodes_and_algs(node, recurse_algs, nodes, algs):
    if node in nodes:
        return
    nodes[node] = None
    for c in node.children:
        if isinstance(c, CompositeNode):
            _all_nodes_and_algs(c, recurse_algs, nodes, algs)
        elif is_algorithm(c):
            algs[c] = None
            if recurse_algs:
                algs.update({a: None for a in all_algs(c)})
        else:
            raise TypeError(
                'Child {!r} of {!r} is neither a CompositeNode nor an '
                'Algorithm'.format(c, node))


class ComponentConfig(dict):
    """Objects holding configuration of Gaudi Configurables.


    """

    def add(self, component):
        self[component.getFullName()] = component
        return component

    def update(self, other):
        for c in other.values():
            self.add(c)


def make_data_with_FetchDataFromFile(location):
    """Return input data read using `FetchDataFromFile`.

    `FetchDataFromFile` preloads data on the transient event store for
    a given list of locations that must be present in the input file.
    This maker configures an instance to read a single given location
    and returns the `DataHandle` corresponding to the output.

    Args:
        location (str): Location to be fetched from input data.
    """
    return Algorithm(
        Gaudi__Hive__FetchDataFromFile,
        outputs={'Output': force_location(location)},
        output_transform=lambda Output: {"DataKeys": [Output]},
        require_IOVLock=False).Output


def make_raw_event_with_IOAlg(location):
    # TODO what if we're called twice with a different location?
    return Algorithm(
        LHCb__MDF__IOAlg,
        outputs={
            "RawEventLocation": force_location(location)
        },
        IOSvc="LHCb::MDF::IOSvcMM/LHCb__MDF__IOSvcMM").RawEventLocation


@configurable
def default_raw_event(bank_types=[],
                      raw_event_format=None,
                      maker=make_data_with_FetchDataFromFile):
    """Return a raw event that contains a given set of banks.

    Args:
        bank_types (list): Required raw bank types.
            Defaults to all types (empty list).
        raw_event_format: RawEventFormat key in `Raw_location_db`.
        maker: Maker of input data.
            Defaults to `make_data_with_FetchDataFromFile`.

    Returns:
        DataHandle: Raw event containing banks of the requested types.
    """

    if isinstance(bank_types, six.string_types):
        raise TypeError('bank_types must be an iterable of str')
    bank_types = list(bank_types)

    if raw_event_format is None:
        raise ValueError('raw_event_format is required (must be bound)')

    raw_bank_locations = Raw_location_db[raw_event_format]
    # Raw_location_db entries might be lists => make everything a tuple
    raw_bank_locations = {
        k: (v, ) if isinstance(v, string_types) else tuple(v)
        for k, v in raw_bank_locations.items()
    }

    if bank_types:
        locations = set(raw_bank_locations[bt] for bt in bank_types)
    else:
        locations = set(raw_bank_locations.values())

    if len(locations) > 1:
        raise ValueError(
            'Requested raw banks ({}) are split among multiple locations ({}) '
            'for raw event format {}. Restrict raw bank types or merge raw '
            'events manually.'.format(bank_types, locations, raw_event_format))
    location = locations.pop()
    if len(location) > 1:
        raise NotImplementedError(
            'Alternatives are not supported ({} -> {})'.format(
                bank_types, location))
    return maker(location[0])


@configurable
def make_odin(make_raw=default_raw_event):
    return createODIN(RawEvent=make_raw(['ODIN'])).ODIN


@configurable
def make_callgrind_profile(start=10,
                           stop=90,
                           dump=90,
                           dumpName='CALLGRIND-OUT'):
    """Return algorithm that allows to use callgrind profiling.
        For more info see CodeAnalysisTools_.
        .. _CodeAnalysisTools: https://twiki.cern.ch/twiki/bin/view/LHCb/CodeAnalysisTools

    Args:
        start: Event count at which callgrind starts profiling.
            Defaults to 10 events.
        stop: Event count at which callgrind stops profiling.
            Defaults to 90 events.
        dump: Event count at which callgrind dumps results.
            Defaults to 90 events.
        dumpName: Name of dump.
            Defaults to 'CALLGRIND-OUT'.
    """
    return CallgrindProfile(
        StartFromEventN=start,
        StopAtEventN=stop,
        DumpAtEventN=dump,
        DumpName=dumpName)


def configure_input(options):
    options.finalize()
    config = ComponentConfig()
    config.add(
        ApplicationMgr(
            EvtSel="EventSelector" if not options.use_iosvc else "NONE",
            EvtMax=options.evt_max))

    # TODO split out conditions from configure_input
    config.add(
        setup_component(
            'DDDBConf',
            Simulation=options.simulation,
            DataType=options.data_type))
    config.add(
        setup_component(
            'CondDB',
            Upgrade=True,
            Tags={
                'DDDB': options.dddb_tag,
                'SIMCOND': options.conddb_tag,
            }))

    if options.input_files:
        if options.use_iosvc:
            config.add(
                setup_component(
                    'LHCb__MDF__IOSvcMM',
                    Input=options.input_files,
                    BufferNbEvents=options.buffer_events))

            default_raw_event.global_bind(
                raw_event_format=options.input_raw_format,
                maker=make_raw_event_with_IOAlg)

        else:
            # TODO do not use IOHelper here but setup directly
            input_iohelper = IOHelper(options.input_type, options.output_type
                                      or None)
            # setupServices may create (the wrong) EventDataSvc, so do it first
            setup_component(options.event_store, instanceName='EventDataSvc')
            input_iohelper.setupServices()
            evtSel = input_iohelper.inputFiles(options.input_files, clear=True)
            evtSel.Input = [
                inp + " IgnoreChecksum='YES'" for inp in evtSel.Input
            ]
            evtSel.PrintFreq = options.print_freq
            config.add(evtSel)
            config.add(
                setup_component('IODataManager', DisablePFNWarning=True))
            default_raw_event.global_bind(
                raw_event_format=options.input_raw_format)

    return config


def assert_empty_dataondemand_service():
    assert DataOnDemandSvc().AlgMap == {}, DataOnDemandSvc().AlgMap
    assert DataOnDemandSvc().NodeMap == {}, DataOnDemandSvc().NodeMap


def configure(options, control_flow_node, public_tools=[]):
    # TODO get rid of magic initial time (must go to configure_input)
    INITIAL_TIME = 1433509200

    options.finalize()
    config = ComponentConfig()
    if options.callgrind_profile:
        control_flow_node = CompositeNode(
            'profile_' + control_flow_node.name,
            [make_callgrind_profile(), control_flow_node])
    nodes, algs = all_nodes_and_algs(control_flow_node)
    configuration = dataflow_config()
    for alg in algs:
        configuration.update(alg.configuration())
    for tool in public_tools:
        configuration.update(tool.configuration())
    configurable_algs, configurable_tools = configuration.apply()

    # TODO what is this dummy stuff?
    odin_loc = '/Event/DAQ/DummyODIN'
    configurable_algs += [
        setup_component(
            DummyEventTime,
            "DummyEventTime",
            Start=INITIAL_TIME,
            Step=0,
            ODIN=odin_loc,
            IOVLockDep=False),
        setup_component(
            reserveIOV, "reserveIOV", IOVLockDep=False, ODIN=odin_loc)
    ]

    whiteboard = config.add(
        setup_component(
            options.event_store,
            instanceName='EventDataSvc',
            EventSlots=options.n_event_slots,
            ForceLeaves=True))
    scheduler = config.add(
        setup_component(
            'HLTControlFlowMgr',
            CompositeCFNodes=[node.represent() for node in nodes],
            MemoryPoolSize=options.memory_pool_size,
            ThreadPoolSize=options.n_threads,
            EnableLegacyMode=options.scheduler_legacy_mode))
    appMgr = config.add(
        ApplicationMgr(OutputLevel=options.output_level, EventLoop=scheduler))
    appMgr.ExtSvc.insert(
        0, whiteboard
    )  # FIXME this cannot work when configurables are not singletons
    appMgr.ExtSvc.insert(0, NTupleSvc())

    config.add(setup_component('UpdateManagerSvc', WithoutBeginEvent=True))
    config.add(
        setup_component('HiveDataBrokerSvc', DataProducers=configurable_algs))
    # for LoKi
    config.add(setup_component('AlgContextSvc', BypassIncidents=True))
    config.add(setup_component('LoKiSvc', Welcome=False))
    # Give more space for component names in stdout
    config.add(setup_component('MessageSvc', Format='% F%35W%S %7W%R%T %0W%M'))
    config.add(
        setup_component('EventClockSvc', InitialTime=int(INITIAL_TIME * 1e9)))

    # Instantiate the histogram service if we're creating histograms or ntuples,
    # preferring the histo_name if set, otherwise the ntuple_name
    histo_name = options.histo_file or options.ntuple_file
    if histo_name:
        config.add(
            setup_component('HistogramPersistencySvc', OutputFile=histo_name))
        config.add(ApplicationMgr(HistogramPersistency="ROOT"))

    if options.ntuple_file:
        config.add(
            setup_component(
                'NTupleSvc',
                Output=[
                    "FILE1 DATAFILE='{}' TYPE='ROOT' OPT='NEW'".format(
                        options.ntuple_file)
                ]))

    if options.control_flow_file:
        fn_root, fn_ext = os.path.splitext(options.control_flow_file)
        plot_control_flow(
            control_flow_node, filename=fn_root, extensions=[fn_ext[1:]])
    if options.data_flow_file:
        fn_root, fn_ext = os.path.splitext(options.data_flow_file)
        plot_data_flow(algs, filename=fn_root, extensions=[fn_ext[1:]])

    # Make sure nothing's configuring the DoD behind our back, e.g. LHCbApp
    appendPostConfigAction(assert_empty_dataondemand_service)

    # TODO add configurable_tools and configurable_algs to config
    return config


def _gaudi_datetime_format(dt):
    """Return the datetime object formatted as Gaudi does it.

    As seen in the application "Welcome" banner.
    """
    return dt.strftime('%a %h %d %H:%M:%S %Y')


def plot_data_flow(algs, filename='data_flow', extensions=('gv', )):
    """Save a visualisation of the current data flow.

    Parameters
    ----------
    algs : list
        List of Algorithm instances.
    filename : str
        Basename of the file to create.
    extensions : list
        List of file extensions to create. One file is created per
        extensions. Possible values include `'gv'` for saving the raw
        graphviz representation, and `'png'` and `'pdf'` for saving
        graphics.

        **Note**: The `dot` binary must be present on the system for saving
        files with graphical extensions. The raw `.gv` format can be
        convert be hand like::

            dot -Tpdf data_flow.gv > data_flow.pdf
    """
    now = _gaudi_datetime_format(datetime.datetime.now())
    label = 'Data flow generated at {}'.format(now)
    top = pydot.Dot(
        graph_name='Data flow', label=label, strict=True, rankdir='LR')
    top.set_node_defaults(shape='box')
    for alg in algs:
        alg._graph(top)
    for ext in extensions:
        format = 'raw' if ext == 'gv' else ext
        top.write('{}.{}'.format(filename, ext), format=format)


def plot_control_flow(top_node, filename='control_flow', extensions=('gv', )):
    """Save a visualisation of the current control flow.

    Parameters
    ----------
    filename : str
        Basename of the file to create.
    extensions : list
        List of file extensions to create. One file is created per
        extensions. Possible values include `'gv'` for saving the raw
        graphviz representation, and `'png'` and `'pdf'` for saving
        graphics.

        **Note**: The `dot` binary must be present on the system for saving
        files with graphical extensions. The raw `.gv` format can be
        convert be hand like::

            dot -Tpdf data_flow.gv > data_flow.pdf
    """
    now = _gaudi_datetime_format(datetime.datetime.now())
    label = 'Control flow generated at {}'.format(now)
    graph = pydot.Dot(
        graph_name='control_flow', label=label, strict=True, compound=True)
    top_node._graph(graph)

    for ext in extensions:
        format = 'raw' if ext == 'gv' else ext
        graph.write('{}.{}'.format(filename, ext), format=format)
