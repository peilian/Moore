###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import sys


class ToolImportModule(object):
    """Magic module that wraps retrieved objects in PyConf.components.Tool.

    Requested objects are imported from <_packageName>. Every tool that
    you can import from <_packageName>, you can also import from this module.

        >>> from PyConf.Tools import Gaudi__Examples__FloatTool

    Importing objects that are not tools, like algorithms, will fail.

    See PyConf.Algorithms for importing wrapped algorithms.
    """
    _packageName = "Configurables"
    _original__file__ = __file__
    _original__name__ = __name__
    __imported_tools = dict()

    def __getattr__(self, name):
        retval = None
        if name == "__all__":
            retval = getattr(__import__(self._packageName), '__all__')
        elif name == "__path__":
            raise AttributeError("'module' object has no attribute '__path__'")
        elif name == "__file__":
            retval = self._original__file__
        elif name == "__name__":
            retval = self._original__name__
        else:
            try:
                retval = self.__imported_tools[name]
            except KeyError:
                retval = self._wrap_tool(
                    getattr(
                        __import__(self._packageName, fromlist=[name]), name))
                self.__imported_tools[name] = retval
        return retval

    @staticmethod
    def _wrap_tool(tool_type, **defaults):
        """Return the Configurable class `tool_type` wrapped as a `PyConf.components.Tool`.

        See the `PyConf.components.Tool.__new__` method for details on the
        keyword arguments.
        """
        from PyConf import configurable
        from PyConf.components import Tool, _is_configurable_tool

        if not _is_configurable_tool(tool_type):
            raise TypeError(
                'cannot declare a Tool with {}, which is of type {}'.format(
                    tool_type, tool_type.getGaudiType()))

        @configurable
        def wrapped(**kwargs):
            props = dict(defaults, **kwargs)
            return Tool(tool_type, **props)

        wrapped.getDefaultProperties = tool_type.getDefaultProperties

        return wrapped


sys.modules[__name__] = ToolImportModule()
