###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import math
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from Moore.config import HltLine
from RecoConf.hlt1_tracking import (
    require_gec,
    require_pvs,
    make_pvs,
)
from RecoConf.hlt1_muonid import make_fitted_tracks_with_muon_id
from ..algorithms import (CombineTracks, Filter)

from Functors import P, PT, CHI2DOF, MINIPCHI2, MINIPCHI2CUT, ISMUON, MASS, DOCA, DOCACHI2
import Functors.math as fmath


@configurable
def track_muon_prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def one_track_muon_mva_line(
        name='Hlt1TrackMuonMVALine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        # TrackMuonLoose cuts from ZombieMoore to be done
        max_chi2dof=100.0,
        min_pt=2.0 * GeV,
        max_pt=26 * GeV,
        min_ipchi2=7.4,
        param1=1.0,
        param2=2.0,
        param3=1.248):
    pvs = make_pvs().location
    pre_sel = (ISMUON) & (PT > min_pt) & (CHI2DOF < max_chi2dof)
    hard_sel = (PT > max_pt) & MINIPCHI2CUT(IPChi2Cut=min_ipchi2, Vertices=pvs)
    bulk_sel = fmath.in_range(min_pt, PT, max_pt) & (
        fmath.log(MINIPCHI2(pvs)) >
        (param1 / ((PT / GeV - param2) * (PT / GeV - param2)) +
         (param3 / max_pt) * (max_pt - PT) + math.log(min_ipchi2)))
    full_sel = pre_sel & (hard_sel | bulk_sel)
    tracks_with_muon_id = make_input_tracks()
    track_filter = Filter(tracks_with_muon_id,
                          full_sel)['PrFittedForwardWithMuonID']
    return HltLine(
        name=name,
        algs=track_muon_prefilters() + [track_filter],
        prescale=prescale)


@configurable
def low_mass_dimuon_line(
        name='Hlt1DiMuonLowMassLine',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        make_pvs=make_pvs,
        min_pt=500. * MeV,
        #this is the min pt cut for the selection, the default min pt thresholds in reconstruction remain
        min_p=3. * GeV,
        max_track_chi2dof=100.,
        min_track_ipchi2=4.,
        max_doca=0.2 * mm,
        max_vertex_chi2=25.,
        min_mass=0. * GeV):

    pvs = make_pvs().location

    tracks_with_muon_id = make_input_tracks()
    sel = (ISMUON) & (P > min_p) & (PT > min_pt) & MINIPCHI2CUT(
        IPChi2Cut=min_track_ipchi2,
        Vertices=pvs) & (CHI2DOF < max_track_chi2dof)
    children = Filter(tracks_with_muon_id, sel)
    CombinationCut = (DOCA < max_doca) & (DOCACHI2 < max_vertex_chi2)
    VertexCut = (CHI2DOF < max_vertex_chi2) & (MASS(Masses=[105., 105.]) >
                                               min_mass)

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        TracksWithMuonID=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithMuonID'],
        CombinationCut=CombinationCut)
    return HltLine(
        name=name,
        algs=track_muon_prefilters() + [combination_filter],
        prescale=prescale,
    )


@configurable
def high_mass_dimuon_line(name='Hlt1DiMuonHighMassLine',
                          prescale=1,
                          make_input_tracks=make_fitted_tracks_with_muon_id,
                          make_pvs=make_pvs,
                          min_pt=500. * MeV,
                          min_p=6. * GeV,
                          max_track_chi2dof=100.,
                          max_doca=0.2 * mm,
                          max_vertex_chi2=25.,
                          min_mass=2.7 * GeV):

    pvs = make_pvs().location

    tracks_with_muon_id = make_input_tracks()
    sel = (ISMUON) & (P > min_p) & (PT > min_pt) & (CHI2DOF <
                                                    max_track_chi2dof)
    children = Filter(tracks_with_muon_id, sel)
    CombinationCut = (DOCA < max_doca) & (DOCACHI2 < max_vertex_chi2)
    VertexCut = (CHI2DOF < max_vertex_chi2) & (MASS(Masses=[105., 105.]) >
                                               min_mass)

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        TracksWithMuonID=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithMuonID'],
        CombinationCut=CombinationCut)
    return HltLine(
        name=name,
        algs=track_muon_prefilters() + [combination_filter],
        prescale=prescale,
    )
