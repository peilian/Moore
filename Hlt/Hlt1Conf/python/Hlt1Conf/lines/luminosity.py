###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Lines that running luminosity counters on a fraction of all events."""
import cppyy

from Configurables import VoidFilter as _VoidFilter
from PyConf.Algorithms import (
    LumiCounterFromContainer__PrVeloTracks,
    LumiCounterFromContainer__RecVertex_v2,
    LumiCounterMerger,
    VoidFilter,
)

from PyConf.application import make_odin
from Moore.config import HltLine

from RecoConf.hlt1_tracking import (
    make_hlt1_tracks,
    make_reco_pvs,
)

from Functors import EVENTTYPE, SIZE
from Functors import math as fmath


def odin_lumi_filter(make_odin=make_odin):
    lumi_bit = cppyy.gbl.LHCb.ODIN.Lumi

    def input_transform(InputLocation):
        return {"Cut": fmath.test_bit(EVENTTYPE(InputLocation), lumi_bit)}

    odin = make_odin()
    return VoidFilter(InputLocation=odin, input_transform=input_transform)


def luminosity_line(name="Hlt1LuminosityLine", prescale=1):
    """Fills lumi counters for all crossings marked with the Lumi ODIN bit."""
    lumi = odin_lumi_filter()

    velo_tracks = make_hlt1_tracks()["Velo"]
    velo_summary = LumiCounterFromContainer__PrVeloTracks(
        CounterName="RZVelo", InputContainer=velo_tracks["Pr"])

    pvs = make_reco_pvs(velo_tracks)
    pv_summary = LumiCounterFromContainer__RecVertex_v2(
        CounterName="PV3D", InputContainer=pvs)

    merged_summary = LumiCounterMerger(
        InputSummaries=[velo_summary, pv_summary])

    return HltLine(name=name, algs=[lumi, merged_summary], prescale=prescale)
