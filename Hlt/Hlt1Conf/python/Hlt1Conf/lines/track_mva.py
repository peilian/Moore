###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import math
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from PyConf.application import make_odin
from RecoConf.hlt1_tracking import (
    require_gec, require_pvs, make_pvs, make_hlt1_tracks,
    make_hlt1_fitted_tracks, make_fitted_forward_tracks_with_pv_relations)
from ..algorithms import (CombineTracks, Filter)

from Moore.config import HltLine

from Functors import (
    ALL,
    BPVDIRA,
    BPVCORRM,
    BPVETA,
    BPVFDCHI2,
    BPVIPCHI2,
    BPVIPCHI2STATE,
    CHI2DOF,
    COMB,
    DOCA,
    DOCACHI2,
    ETA,
    EVENTNUMBER,
    EVENTTYPE,
    MINIP,
    MVA,
    P,
    PHI,
    PT,
    RUNNUMBER,
    SUM,
)
import Functors.math as fmath
from GaudiKernel.SystemOfUnits import MeV, GeV


def make_tracks_mva_tracks():
    tracks = make_hlt1_fitted_tracks(make_hlt1_tracks())
    return make_fitted_forward_tracks_with_pv_relations(tracks)


@configurable
def track_mva_prefilters(make_pvs=make_pvs):
    return [require_gec(), require_pvs(make_pvs())]


@configurable
def one_track_mva_line(
        name='Hlt1TrackMVALine',
        prescale=1,
        make_input_tracks=make_tracks_mva_tracks,
        make_pvs=make_pvs,
        # TrackMVALoose cuts from ZombieMoore
        max_chi2dof=2.5,
        min_pt=2.0 * GeV,
        max_pt=26 * GeV,
        min_ipchi2=7.4,
        param1=1.0,
        param2=2.0,
        param3=1.248):
    pvs = make_pvs().location
    pre_sel = (CHI2DOF < max_chi2dof)
    hard_sel = (PT > max_pt) & (BPVIPCHI2(pvs) > min_ipchi2)
    bulk_sel = fmath.in_range(min_pt, PT, max_pt) & (
        fmath.log(BPVIPCHI2(pvs)) >
        (param1 / ((PT / GeV - param2) * (PT / GeV - param2)) +
         (param3 / max_pt) * (max_pt - PT) + math.log(min_ipchi2)))
    full_sel = pre_sel & (hard_sel | bulk_sel)
    track_filter = Filter(make_input_tracks(),
                          full_sel)['PrFittedForwardWithPVs']
    return HltLine(
        name=name,
        algs=track_mva_prefilters() + [track_filter],
        prescale=prescale)


@configurable
def two_track_mva_line(name='Hlt1TwoTrackMVALine',
                       prescale=1,
                       make_input_tracks=make_tracks_mva_tracks,
                       make_pvs=make_pvs):
    # This corresponds to TwoTrackMVALoose in ZombieMoore
    pvs = make_pvs().location
    ChildCut = ((PT > 800. * MeV) & (P > 5. * GeV) & (CHI2DOF < 2.5) &
                (BPVIPCHI2(pvs) > 4.))
    # returns a dict of different types
    children = Filter(make_input_tracks(), ChildCut)
    CombinationCut = (PT > 2. * GeV) & (DOCACHI2 < 10.)

    def apply_to_combination(functor):
        """Helper for applying 'Combination' cuts to a composite."""
        return COMB(
            Functor=functor,
            ChildContainers=[
                children['Scalar__PrFittedForwardWithPVs'].location
            ],
            ChildContainerTypes=[
                ('LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs',
                 'SelKernel/TrackZips.h')
            ])

    # TODO there seems to be some magic that substitutes the value of
    #      $PARAMFILESROOT before it gets to the C++. This is probably not what
    #      we want -- better to resolve it at runtime than bake a value into
    #      the functor cache, for example
    VertexCut = ((CHI2DOF < 10.) & (BPVDIRA(pvs) > 0) & (
        BPVCORRM(Mass=139.57018 * MeV, Vertices=pvs) > 1. * GeV
    ) & fmath.in_range(2., BPVETA(pvs), 5.) & (MVA(
        MVAType='MatrixNet',
        Config={'MatrixnetFile': "${PARAMFILESROOT}/data/Hlt1TwoTrackMVA.mx"},
        Inputs={
            'chi2': CHI2DOF,
            'fdchi2': BPVFDCHI2(pvs),
            'sumpt': apply_to_combination(SUM(PT)),
            'nlt16': apply_to_combination(SUM(BPVIPCHI2(pvs) < 16.)),
        }) > 0.96))

    combination_filter = CombineTracks(
        NBodies=2,
        PrTracks=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithPVs'],
        CombinationCut=CombinationCut)
    return HltLine(
        name=name,
        algs=track_mva_prefilters() + [combination_filter],
        prescale=prescale)


@configurable
def debug_two_track_mva_line(name='Hlt1DebugTwoTrackMVALine',
                             prescale=1,
                             make_input_tracks=make_tracks_mva_tracks,
                             make_pvs=make_pvs,
                             VertexCut=None,
                             ChildCut=None,
                             CombinationCut=None,
                             VoidDump=None,
                             ChildDump=None,
                             CombinationDump=None,
                             VertexDump=None,
                             dump_filename='PrCombineTracks.root'):
    pv_loc, odin_loc = make_pvs().location, make_odin().location
    if VoidDump is None:
        VoidDump = {
            'runNumber': RUNNUMBER(odin_loc),
            'eventType': EVENTTYPE(odin_loc),
            'eventNumber': EVENTNUMBER(odin_loc),
        }
    if ChildCut is None:
        ChildCut = (PT > 250. * MeV)
    # returns a dict of different types
    children = Filter(make_input_tracks(), ChildCut)
    if ChildDump is None:
        ChildDump = {
            'P': P,
            'PT': PT,
            'ETA': ETA,
            'PHI': PHI,
            'IP': MINIP(pv_loc),
            'IPCHI2': BPVIPCHI2(pv_loc),
            'CHI2DOF': CHI2DOF,
        }

    def apply_to_combination(functor):
        """Helper for applying 'Combination' cuts to a composite."""
        return COMB(
            Functor=functor,
            ChildContainers=[
                children['Scalar__PrFittedForwardWithPVs'].location
            ],
            ChildContainerTypes=[
                ('LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs',
                 'SelKernel/TrackZips.h')
            ])

    if VertexCut is None:
        VertexCut = (CHI2DOF < 1e3)
    if VertexDump is None:
        VertexDump = {
            'PT': PT,
            'ETA': ETA,
            'PHI': PHI,
            'CHI2DOF': CHI2DOF,
            'DIRA': BPVDIRA(pv_loc),
            'IPCHI2': BPVIPCHI2(pv_loc),
            'IPCHI2_STATE': BPVIPCHI2STATE(pv_loc),
            'SUMPT': apply_to_combination(SUM(PT)),
        }
    if CombinationCut is None:
        CombinationCut = ALL
    if CombinationDump is None:
        CombinationDump = {
            'PT': PT,
            'DOCA': DOCA,
            'SUMPT': SUM(PT),
            'DOCACHI2': DOCACHI2,
        }
    combiner = CombineTracks(
        NBodies=2,
        PrTracks=True,
        VertexCut=VertexCut,
        InputTracks=children['Scalar__PrFittedForwardWithPVs'],
        CombinationCut=CombinationCut,
        VoidDump=VoidDump,
        ChildDump=ChildDump,
        CombinationDump=CombinationDump,
        VertexDump=VertexDump,
        DumpFileName=dump_filename)
    # TODO remove make_odin() from the control flow when configuration
    #      starts handling data flow in functors, see
    #      https://gitlab.cern.ch/lhcb/Moore/issues/51
    return HltLine(
        name=name,
        algs=track_mva_prefilters() + [make_odin(), combiner],
        prescale=prescale)
