###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for creating the SelReports object.

SelReports summarise the set of objects that fired a line. These objects are
assumed to be produced by the last algorithm in the line's control flow.

The SelReports maker algorithm accepts v1 Track and v1 Vertex objects, and so
this module contains some logic for traversing the dataflow tree of the
decision algorithm and instantiating converters required to go from whatever
object the algorithm produces to the v1 objects.

To support additional algorithms, add a converter function and the supported
algorithm type to the `CONVERTERS` dict.

The logic of the converter functions is somewhat brittle, as it depends on the
structure of the dataflow that enters into each line's decision algorithm.
"""
import logging

# Need to import some algorithms from Configurables so that we can determine
# the underlying type of the PyConf.Algorithms instance
from Configurables import (
    CombineTracks__2Body__PrFittedForwardTracksWithMuonID,
    CombineTracks__2Body__PrFittedForwardTracksWithPVs,
    PrFilter__PrFittedForwardTracksWithMuonID,
    PrFilter__PrFittedForwardTracksWithPVs,
)

from PyConf.Algorithms import (
    LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as
    trackV1FromV2TrackV1TrackVector,
    LHCb__Converters__Track__v2__fromPrFittedForwardTrackWithMuonID as
    trackV2FromPrFittedForwardTrackWithMuonID,
    LHCb__Converters__Track__v2__fromPrFittedForwardTrackWithPVs as
    trackV2FromPrFittedForwardTrackWithPVs,
    LHCb__Converters__TrackCompactVertex__VectorOf2TrackPrFittedWithMuonIDCompactVertexToVectorOfRecVertex
    as VectorOf2TrackPrFittedWithMuonIDCompactVertexToVectorOfRecVertex,
    LHCb__Converters__TrackCompactVertex__VectorOf2TrackPrFittedWithPVsCompactVertexToVectorOfRecVertex
    as VectorOf2TrackPrFittedWithPVsCompactVertexToVectorOfRecVertex,
    SelReportsMaker,
)

__all__ = ["make_selreports"]
log = logging.getLogger(__name__)

# C++ type of the VELO hits container
VELO_HITS_TYPE = "LHCb::Pr::Velo::Hits"


class UnconvertableAlgorithmError(Exception):
    pass


class AmbiguousVeloHitsError(Exception):
    pass


def find_velo_hits(alg):
    """Return a data handle to VELO hits from the dependencies of `alg`.

    Traverses up the dataflow tree of `alg` until it finds data handle with
    type `VELO_HITS_TYPE`.

    Raises:
        AmbiguousVeloHitsError: If multiple data handles with the
        `VELO_HITS_TYPE` type are found.
    """
    all_inputs = alg.all_inputs
    velo_hits = {i for i in all_inputs if i.type == VELO_HITS_TYPE}
    if len(velo_hits) != 1:
        raise AmbiguousVeloHitsError(
            "Expected one VELO hits object, found {}".format(len(velo_hits)))
    return velo_hits.pop()


def convert_pr_fitted_tracks_with_muon_id(alg):
    velo_hits = find_velo_hits(alg)
    v2_tracks = trackV2FromPrFittedForwardTrackWithMuonID(
        FittedTracks=alg.outputs["Output"], VeloHits=velo_hits)
    v1_tracks = trackV1FromV2TrackV1TrackVector(InputTracksName=v2_tracks, )
    return v1_tracks


def convert_pr_fitted_tracks_with_pvs(alg):
    velo_hits = find_velo_hits(alg)
    v2_tracks = trackV2FromPrFittedForwardTrackWithPVs(
        FittedTracks=alg.outputs["Output"], VeloHits=velo_hits)
    v1_tracks = trackV1FromV2TrackV1TrackVector(InputTracksName=v2_tracks, )
    return v1_tracks


def convert_vertices(alg, track_converter, vertex_converter):
    """Convert the output of a TrackCompactVertex maker.

    Args:
        alg: Foo.
        track_converter: Takes the SoA track type that is input to the vertex
        maker and return v1 tracks.
        vertex_converter: Foo.
    """
    # The input to the algorithm are 'unwrapped' tracks; we go up to the
    # unwrapping algorithm to find the original 'wrapped' (i.e. zipped) tracks
    unwrapped_fitted_tracks = alg.inputs["InputTracks"]
    fitted_tracks_maker = unwrapped_fitted_tracks.producer.inputs[
        "Input"].producer
    v1_tracks = track_converter(fitted_tracks_maker)
    velo_hits = find_velo_hits(fitted_tracks_maker)
    converter = vertex_converter(
        InputVertices=alg.outputs["OutputVertices"],
        TracksInVertices=fitted_tracks_maker.outputs["Output"],
        VeloHits=velo_hits,
        ConvertedTracks=v1_tracks)
    return converter


def convert_vertices_with_pr_fitted_tracks_with_muon_id(alg):
    return convert_vertices(
        alg,
        track_converter=convert_pr_fitted_tracks_with_muon_id,
        vertex_converter=
        VectorOf2TrackPrFittedWithMuonIDCompactVertexToVectorOfRecVertex)


def convert_vertices_with_pr_fitted_tracks_with_pvs(alg):
    return convert_vertices(
        alg,
        track_converter=convert_pr_fitted_tracks_with_pvs,
        vertex_converter=
        VectorOf2TrackPrFittedWithPVsCompactVertexToVectorOfRecVertex)


# Map algorithm types to a converter function
# Each convert accepts the algorithm instance and returns an algorithm whose
# output(s) are the converted (v1) objects
CONVERTERS = {
    PrFilter__PrFittedForwardTracksWithMuonID:
    convert_pr_fitted_tracks_with_muon_id,
    PrFilter__PrFittedForwardTracksWithPVs:
    convert_pr_fitted_tracks_with_pvs,
    CombineTracks__2Body__PrFittedForwardTracksWithMuonID:
    convert_vertices_with_pr_fitted_tracks_with_muon_id,
    CombineTracks__2Body__PrFittedForwardTracksWithPVs:
    convert_vertices_with_pr_fitted_tracks_with_pvs,
}


def convert_output(alg):
    """Return an algorithm that produces the output(s) of `alg` in v1 format.

    Raises:
        UnconvertableAlgorithmError: If the type of `alg` is not listed in
        `CONVERTERS`.
    """
    try:
        return CONVERTERS[alg.type](alg)
    except KeyError:
        raise UnconvertableAlgorithmError(
            "Unsupported algorithm type {!r}".format(alg.type))


def line_outputs_for_selreports(line):
    """Return a line's output objects converted to v1 objects.

    A line's "output" is defined in this method as the output data of the last
    algorithm in a line's control flow. Given this, a line can have multiple
    output data.

    The conversion is done according to the logic in `convert_output`. If that
    method raises a ValueError, this method will log a warning that notes the
    line output that cannot be converted.

    Args:
        line (HltLine): Line object whose output should be converted

    Returns:
        List of DataHandle of converted outputs.
    """
    alg = line.output_producer
    try:
        converted = convert_output(alg)
    except UnconvertableAlgorithmError as e:
        log.warning("Cannot create SelReports for line {}: {}".format(
            line.name, e))
        return []
    return converted.outputs.values()


def make_selreports(lines, decreports, **kwargs):
    """Return a SelReportsMaker instance configured for the given lines.

    Args:
        lines (list of HltLine): The lines to create SelReport objects
        for.
        decreports: DecReports data used as input to the SelReports maker.
        kwargs: Passed to the SelReportsMaker.
    """
    # Gather all outputs along with their type and decision names
    info = [(line.decision_name, output, output.type) for line in lines
            for output in line_outputs_for_selreports(line)]
    if info:
        # Configurable properties expect list types, not tuples
        names, outputs, types = map(list, zip(*info))
    else:
        # No lines we can handle, so do nothing
        names, outputs, types = [], [], []
    # Each line output should be unique; we expect no two lines to do exactly
    # the same work
    assert len(outputs) == len(
        set(outputs)), "multiple lines with identical output"

    return SelReportsMaker(
        DecReports=decreports,
        SelectionNames=names,
        CandidateTypes=types,
        Inputs=outputs,
        **kwargs)
