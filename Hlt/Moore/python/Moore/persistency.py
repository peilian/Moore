###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Logic for clone HLT2 line output and persisting them to raw banks.

This is largely a port of the Run 2 logic, up until the serialisation of the
packed containers into raw banks. The serialisation is skipped as we assume
that HLT2 is always writing a DST, and so we can write packed containers
directly.

The Run 2 logic is defined in the Hlt project. Reading the `_persistRecoSeq`
method in the `HltAfterburner.py` file is a good place to start if you want to
understand more.
"""
import logging
import os
import re

from Configurables import (
    HltLinePersistenceSvc, )
from PyConf.Algorithms import (
    CopyLinePersistenceLocations,
    CopyParticle2PVRelationsFromLinePersistenceLocations,
    FilterDesktop,
    HltPackedDataWriter,
    PackCaloHypo,
    PackParticlesAndVertices,
)
from PyConf.Tools import (
    CaloClusterCloner,
    CaloHypoCloner,
    ProtoParticleCloner,
)
from PyConf.components import force_location
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.dataflow import DataHandle
from PyConf.utilities import ConfigurationError

from Hlt2Conf.data_from_file import unpacked_reco_locations
from RecoConf.reco_objects_from_file import make_pvs

#: TES prefix under which all persisted objects will be stored
DEFAULT_OUTPUT_PREFIX = "/Event/HLT2"
#: Final component of a TES location holding Particle objects
PARTICLES_LOCATION_SUFFIX = "Particles"
# Set of locations we've forced to specific values; must keep track of these to
# ensure we never force the same location twice
_FORCED_LOCATIONS = set()

log = logging.getLogger(__name__)


class UnknownOutputError(ConfigurationError):
    """Raised if an algorithm output cannot be handled."""


def _prefix(loc, tes_prefix='/Event'):
    """Add a prefix to a location.

    `/Event` and `tes_prefix` are stripped from the beginning of `loc`, and
    then `tes_prefix` is prepended.
    Args:
        loc (str): Location to prefix.
        tes_prefix (str): Prefix to add to `loc`.
    """
    unprefixed = re.sub('^({0}|/Event)/'.format(tes_prefix), '', loc)
    return os.path.join(tes_prefix, unprefixed)


def _dvalgorithm_locations(producer, stream):
    """Return a list of location 2-tuples for all DVAlgorithm outputs.

    The 2-tuple has the form::

        (source_location, cloned_location)

    where `source_location` is an output location of the producer, and
    `cloned_location` is the location the output should be cloned to.

    Assumes `producer` represents a DVAlgorithm.
    """
    assert len(producer.outputs) == 1
    particle_loc = producer.outputs.values()[0].location
    particles_suffix = '/{}'.format(PARTICLES_LOCATION_SUFFIX)
    assert particle_loc.endswith(particles_suffix), particle_loc

    # Algorithms that create '/Particle' locations can also create some
    # additional locations under the same parent path; we want those too
    sneaky_suffixes = [
        'decayVertices', '_RefitPVs', 'Particle2VertexRelations'
    ]

    locations = []
    for suffix in [PARTICLES_LOCATION_SUFFIX] + sneaky_suffixes:
        loc = re.sub(particles_suffix + '$', '/' + suffix, particle_loc)
        locations.append((_prefix(loc), _prefix(loc, stream)))

    return locations


def _container_cloners(stream, line_outputs, persistence_svc, decreports):
    """Return a list of cloners that will copy line outputs under `stream`.

    Args:
        stream (str): TES root under which objects will be cloned.
        line_outputs (dict): Maps HLT line name (str) to list of associated
        output locations (list of str).
        persistence_svc (Configurable): Instance of ILinePersistenceSvc Configurable.
        decreports (DataHandle): HLT2 decision reports.
    """
    # TODO ideally we configure the cluster and digit/ADC cloning here, but
    # running HLT2 from the Brunel reconstruction means we don't actually have
    # access to these, only the CaloHypo objects are available, so we have to
    # ban cluster cloning altogether
    # We only support running on Brunel output at the moment, so hard-code this
    # path choice
    calo_hack = True
    if calo_hack:
        # The cluster cloner should never run given the configuration of the
        # hypo cloner below, but let's be sure
        cluster_cloner = CaloClusterCloner(
            CloneEntriesNeuP=False,
            CloneEntriesChP=False,
            CloneEntriesAlways=False,
        )
        hypo_cloner = CaloHypoCloner(
            CloneClustersNeuP=False,
            CloneClustersChP=False,
            CloneDigitsNeuP=False,
            CloneDigitsChP=False,
            CloneClustersAlways=False,
            CloneDigitsAlways=False,
        )
    else:
        # Always clone all information associated to CALO objects
        # This may end up being impossible due to bandwidth constraints, but
        # let's enable it for now and profile later
        cluster_cloner = CaloClusterCloner(CloneEntriesAlways=True)
        hypo_cloner = CaloHypoCloner(
            CloneClustersAlways=True,
            CloneDigitsAlways=True,
        )

    protoparticle_cloner = ProtoParticleCloner(
        # Clone clusters associated to tracks
        # TODO The TrackClonerWithClusters tool does not support the Run 3
        # detector
        # ICloneTrack=TrackClonerWithClusters(
        #     CloneAncestors=False
        # )
    )

    container_cloner = CopyLinePersistenceLocations(
        OutputPrefix=stream,
        LinesToCopy=line_outputs.keys(),
        ILinePersistenceSvc=persistence_svc.getFullName(),
        Hlt2DecReportsLocation=decreports,
        ICloneCaloHypo=hypo_cloner,
        ICloneCaloCluster=cluster_cloner,
        ICloneProtoParticle=protoparticle_cloner,
    )

    p2pv_cloner = CopyParticle2PVRelationsFromLinePersistenceLocations(
        # Clone PVs, but not the tracks they reference
        ClonerType='VertexBaseFromRecVertexClonerNoTracks',
        OutputPrefix=stream,
        LinesToCopy=line_outputs.keys(),
        ILinePersistenceSvc=persistence_svc.getFullName(),
        Hlt2DecReportsLocation=decreports,
    )

    return [container_cloner, p2pv_cloner]


def _container_packers(stream):
    """Return a list of packers that will produce all packed output.

    Args:
        stream (str): TES root containing objects to be packed.
    """
    packer = PackParticlesAndVertices(
        InputStream=stream,
        EnableCheck=False,
        # Always create all output containers
        # This list was taken by scanning the packer source code to see what
        # containers it creates
        AlwaysCreateContainers=[
            os.path.join(stream, p) for p in [
                'pPhys/Particles',
                'pPhys/Vertices',
                'pPhys/RecVertices',
                'pPhys/FlavourTags',
                'pPhys/Relations',
                'pPhys/PartToRelatedInfoRelations',
                'pPhys/P2IntRelations',
                'pRec/ProtoP/Custom',
                'pRec/Muon/CustomPIDs',
                'pRec/Rich/CustomPIDs',
                'pRec/Track/Custom',
            ]
        ],
        AlwaysCreateOutput=True,
        DeleteInput=False,
    )

    # PackParticlesAndVertices doesn't pack calo objects (obviously!) So do it
    # with a separate algorithm
    calo_packers = [
        PackCaloHypo(
            InputName=loc.replace("/Event", stream),
            OutputName=loc.replace("/Event", stream).replace(
                "/Rec/", "/pRec/"),
            AlwaysCreateOutput=True,
            DeleteInput=False,
            EnableCheck=False,
        ) for key, loc in unpacked_reco_locations().items() if 'Calo' in key
    ]

    return [packer] + calo_packers


def _line_output_mover_output_transform(Output):
    """Map arguments to FilterDesktop output properties."""
    return dict(Output=Output, ExtraOutputs=[Output])


def _line_output_mover_input_transform(Particles, PrimaryVertices):
    """Map arguments to FilterDesktop input properties."""
    return dict(
        Inputs=[Particles],
        InputPrimaryVertices=PrimaryVertices,
        ExtraInputs=[Particles, PrimaryVertices],
    )


def CopyParticles(particle_producer, pvs, path_components):
    """Return an algorithm that copies Particle objects.

    Args:
        particle_producer (Algorithm): Producer of article container to be copied.
        pvs (DataHandle): Primary vertices used to generated P2PV relations
        tables for the copied Particle objects.
        path_components (list of str): Definition of TES path, relative to
        '/Event', under which to store copied Particle objects.

    Raises:
        UnknownOutputError: If the output of the `particle_producer` algorithm
        does not match the API of DVCommonBase.
    """
    # Path components should not contain slashes
    assert all('/' not in pc for pc in path_components)
    # We will add the suffix in this method to ensure consistency
    assert path_components[-1] != PARTICLES_LOCATION_SUFFIX

    # We only support algorithms that produce KeyedContainers of
    # LHCb::Particle, and assume these inherit from DVCommonBase.  In this case
    # the output property is called `Particles` and output type cannot be
    # deduced  by the framework, so is marked as "unknown_t"
    # If we get an algorithm type that does not match these requirements, raise
    # an exception
    try:
        particles = particle_producer.Particles
    except AttributeError:
        raise UnknownOutputError(
            "Producer {} has no Particles output".format(particle_producer))
    if particles.type != "unknown_t":
        raise UnknownOutputError(
            "Producer {} creates unsupported type {}".format(
                particle_producer, particles.type))

    output_loc = os.path.join(
        os.path.join("/Event", *path_components), PARTICLES_LOCATION_SUFFIX)
    # Keep track of forced output locations to make sure we don't write to the
    # same place twice
    assert output_loc not in _FORCED_LOCATIONS, "Duplicate CopyParticles instantiation"
    _FORCED_LOCATIONS.add(output_loc)

    # Define a FilterDesktop derivative whose output is forced to go to a
    # specific location
    return FilterDesktop(
        input_transform=_line_output_mover_input_transform,
        output_transform=_line_output_mover_output_transform,
        outputs=dict(Output=force_location(output_loc)),
        Particles=particles,
        PrimaryVertices=pvs,
        # FilterDesktop doesn't create 'new' particles by
        # default, but pointers to the inputs passing the
        # filters. We override that default behaviour here
        CloneFilteredParticles=True,
        # Ensure that P2PV relations are made; we'll persist these later
        Code='BPVVALID() | ALL',
        # Use 'default' DVAlgorithm behaviour for consistency when we read
        # back the data offline
        WriteP2PVRelations=True,
        # Allow DVCommonBase to ensure`/Particles` suffix to the locations
        ModifyLocations=True,
    )


def clone_candidates(lines,
                     decreports,
                     stream=DEFAULT_OUTPUT_PREFIX,
                     make_pvs=make_pvs):
    """Return list of algorithms to clone and pack line outputs to stream and their outputs.

    There are a few steps to the persistency.

    1. Copy ('move') the output of each line (a container of `LHCb::Particle` objects)
       to a TES location that includes the name of the line. This is done so
       that we don't use the PyConf-generated location, which can change as the
       configuration evolves.
    2. Clone each of the named outputs, and the objects in their data
       dependency tree, to a location prefixed by `stream`.
    3. Pack the TES contents under `stream`.

    Because of the final step, the list of locations to persist all begin
    with `<stream>/pPhys/` and `<stream>/pRec/`.

    Args:
        lines (list of HltLine)
        decreports (DataHandle): HLT2 decision reports.
        stream (str): TES root under which objects will be cloned.
        make_pvs (function): Returns a `DataHandle` to the primary vertex
            container that should be saved be all lines.

            TODO need to ensure these are the same PVs the lines used (or
            handle P2PV relations in a nicer way). Or perhaps this method
            explicitly accepts 'pvs', such that Moore is responsible for
            defining them (this would require Moore to define PVs as part of
            an 'API').

    Returns:
        algs (list): Algorithms to run the cloning and packing.
        outputs (list of str): Locations that should be persisted, in the
        specification used by ROOT output writers (e.g. OutputStream).
    """
    # Define the PV location every line will request
    pvs = make_pvs()
    pv_location = pvs.location
    pv_locations = [(_prefix(pv_location), _prefix(pv_location, stream))]

    # Line name to list of mover algorithms
    movers = {}
    # Line name to list of (original, copy) 2-tuples locations
    # TODO also collect PersistReco locations for lines that request it
    line_locations = {}
    for line in lines:
        # Locations to be cloned are indexed by the *decision name* of the line
        key = line.decision_name
        # The locations themselves use only the *name* of the line
        location = line.name
        # Move the producer algorithm that governs the line decision
        try:
            decision_mover = CopyParticles(line.output_producer, pvs,
                                           [location])
        except UnknownOutputError:
            log.warning(
                "Skipping persistency for line {}; unsupported output producer type"
                .format(line.name))
            continue
        line_locations[key] = _dvalgorithm_locations(decision_mover, stream)
        # All lines get PVs
        line_locations[key] += pv_locations
        movers[key] = [decision_mover]
        # Move each extra output producer
        for extra_name, extra_output in line.extra_outputs:
            # Extra outputs are persisted to a location relative to the line
            # output
            extra_producer = extra_output.producer if isinstance(
                extra_output, DataHandle) else extra_output
            extra_mover = CopyParticles(extra_producer, pvs,
                                        [location, extra_name])
            line_locations[key] += _dvalgorithm_locations(extra_mover, stream)
            # Mover algorithms for extra output are associated to the line
            # XXX This will cause duplication if multiple lines define the same
            # extra outputs, e.g. some set of objects that is not dependent on
            # the line candidate
            movers[key] += [extra_mover]

    # Create the control flow node for all movers
    # Note that all movers will run irrespective of whether lines fired or not.
    movers_cf = CompositeNode(
        "LineMovers",
        children=[
            CompositeNode(
                "{}Movers".format(decision_name),
                children=line_movers,
                combineLogic=NodeLogic.NONLAZY_OR,
            ) for decision_name, line_movers in movers.items()
        ],
        combineLogic=NodeLogic.NONLAZY_OR,
    )

    # Line name to line output locations, given to the persistence service so
    # that it can tell the cloners which locations to copy
    line_outputs = {
        name: [loc for (loc, _) in pairs]
        for name, pairs in line_locations.items()
    }
    persistence_svc = HltLinePersistenceSvc(
        Locations=line_outputs,
        TurboPPLines=[],
    )
    container_cloners = _container_cloners(
        stream,
        line_outputs,
        persistence_svc,
        decreports,
    )
    container_packers = _container_packers(stream)

    # Move outputs to canonical locations, copy to prefixed location, then pack
    algs = [movers_cf] + container_cloners + container_packers
    # TODO Very gross to hardcode this; ideally we would know the outputs
    # created by the packers explicitly in the configuration
    outputs = [
        os.path.join(stream, "pPhys#*"),
        os.path.join(stream, "pRec#*"),
    ]
    return algs, outputs
