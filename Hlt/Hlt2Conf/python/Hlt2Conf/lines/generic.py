###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Some generic lines

mainly to test consistency combiners and filters and estimate timing
"""

from __future__ import absolute_import, division, print_function
import math

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction, make_tracks

from ..algorithms import require_all, ParticleCombiner, ParticleFilterWithPVs, N3BodyCombiner, N4BodyCombiner, make_dvalgorithm
from PyConf.Algorithms import TwoBodyCombiner, ThreeBodyCombiner, FourBodyCombiner
from PyConf.Algorithms import ChargedBasicsFilter
from PyConf.Algorithms import LHCb__Converters__RecVertices__LHCbRecVerticesToVectorV2RecVertex as Vertex_v1_to_v2
from PyConf.Algorithms import LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as Vertex_v2_to_v1
from PyConf.Algorithms import LHCb__Converters__Track__v2__fromV1TrackV2Track as v2TrackConverter
from PyConf.Algorithms import LHCb__Converters__SharedToVectorParticle as SharedToVector
from PyConf.Algorithms import LHCb__Converters__KeyedToVectorParticle as KeyedToVector
from PyConf.Algorithms import ThOrTwoBodyCombiner, ThOrThreeBodyCombiner, ThOrFourBodyCombiner
from PyConf.Algorithms import BasicsPropertyDumper, CompositePropertyDumper, LegacyParticlePropertyDumper

from ..framework import configurable
from ..standard_particles import make_long_pions, make_long_kaons, make_long_cb_muons, make_long_cb_pions, make_long_muons

from Functors import (ALL, DOCACHI2, DOCA, NONE, MAXDOCACHI2CUT, PT, BPVIPCHI2,
                      MINIPCUT, MINIP, MINIPCHI2, FILTER, ISMUON, PID_MU,
                      PID_K, PID_E, PID_P, PID_PI, COMPOSITEMASS, CHI2NDOF)


def __transform_functor_dict(input_dict):
    return {
        k: [v.code(), v.code_repr()] + v.headers()
        for k, v in input_dict.items()
    }


two_body_dds = ['J/psi(1S) -> mu+ mu-']
three_body_dds = ['J/psi(1S) -> mu+ mu- pi+']
four_body_dds = ['J/psi(1S) -> mu+ mu- pi- pi+']

new_lines = {}


def make_v2_pvs():
    v2_tracks = v2TrackConverter(
        InputTracksName=make_tracks()).OutputTracksName
    v2pvs = Vertex_v1_to_v2(
        InputVertices=make_pvs(), InputTracks=v2_tracks).OutputVertices
    return v2pvs


def make_pvs_from_v2_pvs():
    v1pvs = Vertex_v2_to_v1(
        InputVerticesName=make_v2_pvs(), InputTracksName=make_tracks())
    return v1pvs


@configurable
def filter_new_particles(particles,
                         make_pvs=make_v2_pvs,
                         make_tracks=make_tracks):
    '''
    testing the new particle class with a filter
    '''

    v2pvs = make_pvs()

    # code = FILTER(PT>1000)
    # code = FILTER(MINIP(Vertices=v2pvs.location) > 3.)
    # code = FILTER(MINIPCHI2(v2pvs.location) > 8)
    code = FILTER(ISMUON)
    # code = FILTER((PID_PI > 0) | (PID_P > 0) | (PID_E > 0))

    return ChargedBasicsFilter(
        name="filter_new_particles", Input=particles, Cut=code).Output


@configurable
def combine_old_particles_trg(name, dd, combiner=ParticleCombiner, **kwargs):
    '''combining with the old and mold combineparticles'''
    pions = make_long_pions()
    muons = make_long_muons()
    return combiner(
        name=name,
        particles=[pions, muons],
        DecayDescriptors=dd,
        CombinationCut="ADOCACHI2CUT(10,'')",
        # CombinationCut="AALL",
        MotherCut="ALL",
        ParticleCombiners={"": "ParticleVertexFitter"},
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
        **kwargs)


@configurable
def combine_new_2particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_cb_pions()
    muons = make_long_cb_muons()
    return ThOrTwoBodyCombiner(
        Inputs=[pions, muons],
        DaughterCuts=__transform_functor_dict({
            "mu+": FILTER(ISMUON)
        }),
        DecayDescriptors=two_body_dds,
        Comb2Cut=MAXDOCACHI2CUT(10.),
        # Comb2Cut = ALL,
        MotherCut=FILTER(ALL)).Particles


@configurable
def combine_new_3particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_cb_pions()
    muons = make_long_cb_muons()
    return ThOrThreeBodyCombiner(
        Inputs=[pions, muons],
        DaughterCuts=__transform_functor_dict({
            "mu+": FILTER(ISMUON)
        }),
        DecayDescriptors=three_body_dds,
        Comb3Cut=MAXDOCACHI2CUT(10.),
        Comb2Cut=MAXDOCACHI2CUT(10.),
        MotherCut=FILTER(ALL)).Particles


@configurable
def combine_new_4particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_cb_pions()
    muons = make_long_cb_muons()
    return ThOrFourBodyCombiner(
        Inputs=[pions, muons],
        DaughterCuts=__transform_functor_dict({
            "mu+": FILTER(ISMUON)
        }),
        DecayDescriptors=four_body_dds,
        Comb4Cut=MAXDOCACHI2CUT(10.),
        Comb3Cut=MAXDOCACHI2CUT(10.),
        Comb2Cut=MAXDOCACHI2CUT(10.),
        MotherCut=FILTER(ALL)).Particles


@configurable
def combine_shiny_2particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_pions()
    muons = make_long_muons()
    return TwoBodyCombiner(
        Inputs=[pions, muons],
        DaughterCuts=__transform_functor_dict({
            "mu+": ISMUON
        }),
        DecayDescriptors=two_body_dds,
        Comb12Cut=MAXDOCACHI2CUT(10.),
        ParticleCombiner="ParticleVertexFitter",
        DistanceCalculator="LoKi::TrgDistanceCalculator",
        MotherCut=ALL).OutputParticles


@configurable
def combine_shiny_3particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_pions()
    muons = make_long_muons()
    return ThreeBodyCombiner(
        Inputs=[pions, muons],
        DecayDescriptors=three_body_dds,
        DaughterCuts=__transform_functor_dict({
            "mu+": ISMUON
        }),
        Comb123Cut=MAXDOCACHI2CUT(10.),
        Comb12Cut=MAXDOCACHI2CUT(10.),
        ParticleCombiner="ParticleVertexFitter",
        DistanceCalculator="LoKi::TrgDistanceCalculator",
        MotherCut=ALL).OutputParticles


@configurable
def combine_shiny_4particles():
    '''combining with the even newer shiny combiny'''
    pions = make_long_pions()
    muons = make_long_muons()
    return FourBodyCombiner(
        Inputs=[pions, muons],
        DecayDescriptors=four_body_dds,
        DaughterCuts=__transform_functor_dict({
            "mu+": ISMUON
        }),
        Comb1234Cut=MAXDOCACHI2CUT(10.),
        Comb123Cut=MAXDOCACHI2CUT(10.),
        Comb12Cut=MAXDOCACHI2CUT(10.),
        ParticleCombiner="ParticleVertexFitter",
        DistanceCalculator="LoKi::TrgDistanceCalculator",
        MotherCut=ALL).OutputParticles


@configurable
def dump_particle_properties(particles,
                             filename,
                             properties={
                                 "m": COMPOSITEMASS,
                                 "chi2ndof": CHI2NDOF
                             },
                             dumper=CompositePropertyDumper):
    return dumper(
        Branches=__transform_functor_dict(properties),
        Input=particles,
        DumpFileName=filename)


@configurable
def filter_old_particles(make_particles=make_long_muons):
    '''
    reference with the old particle
    '''
    # code = "PT > 1000"
    # code = "MIPDV(PRIMARY) > 3"
    # code = "MIPCHI2DV(PRIMARY) > 8"
    code = "ISMUON"
    # code = "(PIDp > 0) | (PIDpi > 0) | (PIDe > 0)"

    return ParticleFilterWithPVs(
        name="filter_old_particles",
        particles=make_particles(),
        pvs=make_pvs(),
        Code=code)


@configurable
def filter_old_particles_trg(make_particles=make_long_muons):
    '''
    reference with the old particle and a quicker distance calculator
    '''
    # code = "PT > 1000"
    # code = "MIPDV(PRIMARY) > 3"
    # code = "MIPCHI2DV(PRIMARY) > 8"
    code = "ISMUON"
    # code = "(PIDp > 0) | (PIDpi > 0) | (PIDe > 0)"

    return ParticleFilterWithPVs(
        name="filter_old_particles_trg",
        particles=make_particles(),
        pvs=make_pvs(),
        Code=code,
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"})


@configurable
def filter_old_particles_converted(make_particles=make_long_muons):
    '''
    reference with the old particle, but convert pvs back and forth to validate that the conversion works.
    '''
    code = "MIPDV(PRIMARY) > 3"
    return ParticleFilterWithPVs(
        name="filter_old_particles_converted",
        particles=make_particles(),
        pvs=make_pvs_from_v2_pvs(),
        Code=code)


################ simple filter lines
@register_line_builder(new_lines)
def my_new_line(name="Hlt2NewFilterLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(),
         filter_new_particles(make_long_cb_pions())])


@register_line_builder(new_lines)
def my_old_line_trg(name="Hlt2OldTrgFilterLine"):
    particles = SharedToVector(Input=filter_old_particles_trg())
    output = dump_particle_properties(
        particles,
        "ISMUON_filtered.root", {"PT": PT},
        dumper=LegacyParticlePropertyDumper)
    return HltLine(name=name, algs=upfront_reconstruction() + [output])


# include again when i need to retest the pv to v2 pv converters
@register_line_builder(new_lines)
def my_old_converted_line(name="Hlt2OldConvertedFilterLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_pvs_from_v2_pvs(),
         filter_old_particles_converted()])


############## new nbody combination lines ##############
@register_line_builder(new_lines)
def my_new_2combiner_line(name="Hlt2New2CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            make_v2_pvs(),
            combine_new_2particles(),
            dump_particle_properties(combine_new_2particles(),
                                     "SoaCombiner.root")
        ])


@register_line_builder(new_lines)
def my_new_3combiner_line(name="Hlt2New3CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), combine_new_3particles()])


@register_line_builder(new_lines)
def my_new_4combiner_line(name="Hlt2New4CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), combine_new_4particles()])


@register_line_builder(new_lines)
def my_shiny_2combiner_line(name="Hlt2Shiny2CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            combine_shiny_2particles(),
            dump_particle_properties(
                combine_shiny_2particles(),
                "ShinyCombiny.root",
                dumper=LegacyParticlePropertyDumper)
        ])


@register_line_builder(new_lines)
def my_shiny_3combiner_line(name="Hlt2Shiny3CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [combine_shiny_3particles()])


@register_line_builder(new_lines)
def my_shiny_4combiner_line(name="Hlt2Shiny4CombinerLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [combine_shiny_4particles()])


############# old nbody combination lines ###############
@register_line_builder(new_lines)
def my_old_combiner_line_trg(name="Hlt2OldTrg2CombineLine"):
    composites = combine_old_particles_trg(
        "Oldy2Body", two_body_dds, DaughtersCuts={"mu+": "ISMUON"})
    return HltLine(name=name, algs=upfront_reconstruction() + [composites])


@register_line_builder(new_lines)
def my_old_combiner_line_trg(name="Hlt2OldTrg3CombineLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            combine_old_particles_trg(
                "Oldy3Body", three_body_dds, DaughtersCuts={"mu+": "ISMUON"})
        ])


@register_line_builder(new_lines)
def my_old_combiner_line_trg(name="Hlt2OldTrg4CombineLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            combine_old_particles_trg(
                "Oldy4Body", four_body_dds, DaughtersCuts={"mu+": "ISMUON"})
        ])


@register_line_builder(new_lines)
def my_old_combiner_line_trg(name="Hlt2OldTrg3CombineNBodyLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            combine_old_particles_trg(
                "Oldy3NBody",
                three_body_dds,
                N3BodyCombiner,
                Combination12Cut="ADOCACHI2CUT(10., '')",
                DaughtersCuts={"mu+": "ISMUON"})
        ])


@register_line_builder(new_lines)
def my_old_combiner_line_trg(name="Hlt2OldTrg4CombineNBodyLine"):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [
            combine_old_particles_trg(
                "Oldy4NBody",
                four_body_dds,
                N4BodyCombiner,
                Combination12Cut="ADOCACHI2CUT(10., '')",
                Combination123Cut="ADOCACHI2CUT(10., '')",
                DaughtersCuts={"mu+": "ISMUON"})
        ])
