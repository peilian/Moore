###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D0 -> h- h+ HLT2 lines.

Final states built are:

 1. D0 -> K-  pi+ and its charge conjugate
 2. D0 -> K-  K+
 3. D0 -> pi- pi+
 4. D*+ -> (D0 -> K-  pi+) pi+ and its charge conjugate
 5. D*+ -> (D0 -> pi- K+ ) pi+ and its charge conjugate
 6. D*+ -> (D0 -> K-  K+ ) pi+ and D*- -> (D0 -> K-  K+ ) pi-
 7. D*+ -> (D0 -> pi- pi+) pi+ and D*- -> (D0 -> pi- pi+) pi-
 8. D*+ -> (D0 -> K-  pi+) pi+ and its charge conjugate, lifetime unbiased
 9. D*+ -> (D0 -> pi- K+ ) pi+ and its charge conjugate, lifetime unbiased
10. D*+ -> (D0 -> K-  K+ ) pi+ and D*- -> (D0 -> K-  K+ ) pi-, lifetime
    unbiased
11. D*+ -> (D0 -> pi- pi+) pi+ and D*- -> (D0 -> pi- pi+) pi-, lifetime
    unbiased

The D0 -> h- h+, with identical h species, do not create the charge conjugate
D~0 objects because it would have identical properties and hence would be
a waste of CPU (duplicating the selection and vertexing). This is annoying when
D* tagging is eventually added, however, and one cannot create the 'physical'
charge combinations of `D*+ -> D0 pi+` and `D*- D~0 pi-`. It can be considered
a flaw in the selection framework that we cannot currently express these sorts
of combinations cleanly.
See https://gitlab.cern.ch/lhcb/Moore/issues/64
"""
from __future__ import absolute_import, division, print_function
import math

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from ..algorithms import (require_all, ParticleCombinerWithPVs, ParticleFilter,
                          ParticleFilterWithPVs)
from ..framework import configurable
from ..standard_particles import (make_long_pions, make_has_rich_long_pions,
                                  make_has_rich_long_kaons)

_PION_M = 139.57061 * MeV  # +/- 0.00024
_D0_TAU = 0.4101 * picosecond  # +/- 0.0015

all_lines = {}


@configurable
def make_selected_particles(make_particles=make_has_rich_long_pions,
                            make_pvs=make_pvs,
                            trchi2_max=3,
                            trghostprob_max=0.4,
                            mipchi2_min=4,
                            pt_min=800 * MeV,
                            p_min=5 * GeV,
                            pid_cut=None):
    """Return maker for particles filtered by thresholds common to charm decay
    product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        # 'TRGHOSTPROB < {trghostprob_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            mipchi2_min=mipchi2_min)
    if pid_cut is not None:
        code += ' & ({})'.format(pid_cut)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_selected_particles_ltunb(make_particles=make_has_rich_long_pions,
                                  trchi2_max=3,
                                  trghostprob_max=0.4,
                                  pt_min=800 * MeV,
                                  p_min=1 * GeV,
                                  pid_cut=None):
    """Return maker for particles filtered by thresholds common to
    lifetime-unbiased charm decay product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        # 'TRGHOSTPROB < {trghostprob_max}'
        'PT > {pt_min}',
        'P > {p_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max)
    if pid_cut is not None:
        code += ' & ({})'.format(pid_cut)
    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_charm_pions(pid_cut='PIDK < 5'):
    """Return maker for pions filtered by thresholds common to charm decay
    product selections.
    """
    return make_selected_particles(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_charm_kaons(pid_cut='PIDK > 5'):
    """Return maker for kaons filtered by thresholds common to charm decay
    product selections.
    """
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_charm_pions_ltunb(pid_cut='PIDK < 0'):
    """Return maker for pions filtered by thresholds common to
    lifetime-unbiased charm decay product selections.
    """
    return make_selected_particles_ltunb(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_charm_kaons_ltunb(pid_cut='PIDK > 10'):
    """Return maker for kaons filtered by thresholds common to
    lifetime-unbiased charm decay product selections.
    """
    return make_selected_particles_ltunb(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_charm_soft_pions(make_particles=make_long_pions,
                          pt_min=200 * MeV,
                          p_min=1 * GeV,
                          trchi2_max=3,
                          trghostprob_max=0.25):
    """Return maker for tagging pions filtered by thresholds common to all
    D*+ selections.

    Parameters
    ----------
    make_particles
        Particle maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        # 'TRGHOSTPROB < {trghostprob_max}'
        'PT > {pt_min}',
        'P > {p_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max)
    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_dzeros(particles,
                descriptors,
                pvs,
                am_min=1715 * MeV,
                am_max=2015 * MeV,
                amaxchild_pt_min=1000 * MeV,
                apt_min=2000 * MeV,
                amindoca_max=0.1 * mm,
                vchi2pdof_max=10,
                bpvvdchi2_min=25,
                acos_bpvdira_min=17.3 * mrad):
    """Return D0 maker with selection tailored for two-body hadronic final states.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "AMAXCHILD(PT) > {amaxchild_pt_min}",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       amaxchild_pt_min=amaxchild_pt_min,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    cos_bpvdira_min = math.cos(acos_bpvdira_min)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {cos_bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  cos_bpvdira_min=cos_bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dzeros_ltunb(particles,
                      descriptors,
                      pvs,
                      am_min=1774 * MeV,
                      am_max=1954 * MeV,
                      amaxchild_pt_min=1200 * MeV,
                      apt_min=2000 * MeV,
                      amindoca_max=0.1 * mm,
                      m_min=1804 * MeV,
                      m_max=1924 * MeV,
                      vchi2pdof_max=10,
                      bpvltime_min=0.6 * _D0_TAU,
                      acos_bpvdira_min=141.5 * mrad):
    """Return D0 maker with selection tailored for two-body lifetime-unbiased
    hadronic final states.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "AMAXCHILD(PT) > {amaxchild_pt_min}",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       amaxchild_pt_min=amaxchild_pt_min,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    cos_bpvdira_min = math.cos(acos_bpvdira_min)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVLTIME() > {bpvltime_min}",
                              "BPVDIRA() > {cos_bpvdira_min}",
                              "in_range({m_min}, M, {m_max})").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvltime_min=bpvltime_min,
                                  cos_bpvdira_min=cos_bpvdira_min,
                                  m_min=m_min,
                                  m_max=m_max)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dstars(dzeros,
                soft_pions,
                descriptors,
                pvs,
                q_am_min=130 * MeV - _PION_M,
                q_am_max=165 * MeV - _PION_M,
                q_m_min=130 * MeV - _PION_M,
                q_m_max=160 * MeV - _PION_M,
                vchi2pdof_max=25):
    """Return D*+ maker for tagging a D0 with a soft pion.

    Parameters
    ----------
    dzeros
        D0 particles.
    soft_pions
        Soft pion particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({q_am_min}, (AM - AM1 - AM2), {q_am_max})").format(
            q_am_min=q_am_min,
            q_am_max=q_am_max,
        )

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}",
        "in_range({q_m_min}, (M - M1 - M2), {q_m_max})").format(
            q_m_min=q_m_min, q_m_max=q_m_max, vchi2pdof_max=vchi2pdof_max)

    return ParticleCombinerWithPVs(
        particles=[dzeros, soft_pions],
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def charm_prefilters():
    """Return a list of prefilters common to charm HLT2 lines."""
    return [require_gec(), require_pvs(make_pvs())]


@register_line_builder(all_lines)
@configurable
def dzero2kpi_line(name='Hlt2CharmD0ToKmPipLine', prescale=1):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=[kaons, pions], descriptors=['[D0 -> K- pi+]cc'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dzero2pipi_line(name='Hlt2CharmD0ToPimPipLine', prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    pions = make_charm_pions()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=pions, descriptors=['D0 -> pi- pi+'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dzero2kk_line(name='Hlt2CharmD0ToKmKpLine', prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    kaons = make_charm_kaons()
    pvs = make_pvs()
    dzeros = make_dzeros(particles=kaons, descriptors=['D0 -> K- K+'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2kmpip_line(name='Hlt2CharmDstpToD0Pip_D0ToKmPipLine',
                                     prescale=1):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=[kaons, pions], descriptors=['[D0 -> K- pi+]cc'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2pimkp_line(name='Hlt2CharmDstpToD0Pip_D0ToPimKpLine',
                                     prescale=1):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=[kaons, pions], descriptors=['[D0 -> pi- K+]cc'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2kmkp_line(name='Hlt2CharmDstpToD0Pip_D0ToKmKpLine',
                                    prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    kaons = make_charm_kaons()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=[kaons], descriptors=['D0 -> K- K+'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2pimpip_line(
        name='Hlt2CharmDstpToD0Pip_D0ToPimPipLine', prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    pions = make_charm_pions()
    pvs = make_pvs()
    dzeros = make_dzeros(
        particles=[pions], descriptors=['D0 -> pi- pi+'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2kmpip_ltunb_line(
        name='Hlt2CharmDstpToD0Pip_D0ToKmPip_LTUNBLine', prescale=1):
    kaons = make_charm_kaons_ltunb()
    pions = make_charm_pions_ltunb()
    pvs = make_pvs()
    dzeros = make_dzeros_ltunb(
        particles=[kaons, pions], descriptors=['[D0 -> K- pi+]cc'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2pimkp_ltunb_line(
        name='Hlt2CharmDstpToD0Pip_D0ToPimKp_LTUNBLine', prescale=1):
    kaons = make_charm_kaons_ltunb()
    pions = make_charm_pions_ltunb()
    pvs = make_pvs()
    dzeros = make_dzeros_ltunb(
        particles=[kaons, pions], descriptors=['[D0 -> pi- K+]cc'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2kmkp_ltunb_line(
        name='Hlt2CharmDstpToD0Pip_D0ToKmKp_LTUNBLine', prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    kaons = make_charm_kaons_ltunb()
    pvs = make_pvs()
    dzeros = make_dzeros_ltunb(
        particles=[kaons], descriptors=['D0 -> K- K+'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstarp2dzeropip_dzero2pimpip_ltunb_line(
        name='Hlt2CharmDstpToD0Pip_D0ToPimPip_LTUNBLine', prescale=1):
    # TODO:  Update D* descriptors if the problem of duplication is solved.
    pions = make_charm_pions_ltunb()
    pvs = make_pvs()
    dzeros = make_dzeros_ltunb(
        particles=[pions], descriptors=['D0 -> pi- pi+'], pvs=pvs)
    soft_pions = make_charm_soft_pions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
        extra_outputs=[
            ('SoftPions', soft_pions),
        ])
