###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the prompt dimuon HLT2 lines
"""

from __future__ import absolute_import
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reco_objects_from_file import (make_pvs, upfront_reconstruction)

from ..algorithms import require_all, ParticleFilterWithPVs, ParticleCombinerWithPVs
from ..framework import configurable

from Moore.config import HltLine, register_line_builder

# get the basic particles
from ..standard_particles import make_dimuon_base, make_has_rich_long_kaons

all_lines = {}

# mass window [MeV] around the Jpsi and Psi2s,
# applied to Jpsi and Psi2s lines [2018: 120 MeV]
_MASSWINDOW_JPSI = 120 * MeV
_MASSWINDOW_PSI2S = 120 * MeV

# ProbNNmu requirements on the muons
# of the Jpsi and Psi2s lines [2018: 0.1]
_PROBNNMU_JPSI = 0.1
_PROBNNMU_PSI2S = 0.1


def dimuon_prefilters():
    """Basic prefilters common to all dimuon lines: PV requirements"""
    return [require_pvs(make_pvs())]


@configurable
def make_dimuon(
        dimuons,
        pvs,
        name="DiMuonFilter",
        minMass_dimuon=0 * MeV,
        minPt_dimuon=600 * MeV,
        minPt_muon=300 * MeV,
        maxVertexChi2=25,
        #maxTrackChi2_muon=10, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxTrackGhostProb_muon=0.4,  # this has to be reoptimised for the Upgrade
        minProbNN_muon=0.1):
    """Dimuon with basic kinematic and PID cuts"""

    code = require_all(
        'MM > {minMass_dimuon}',
        'PT > {minPt_dimuon}',
        'MINTREE("mu-" == ABSID, PT) > {minPt_muon}',
        'VFASPF(VCHI2PDOF) < {maxVertexChi2}',
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #'MAXTREE("mu-" == ABSID, TRCHI2DOF) < {maxTrackChi2_muon}',
        'CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)',
        'CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)',
        'MINTREE("mu-" == ABSID, PROBNNmu) > {minProbNN_muon}',
    ).format(
        minMass_dimuon=minMass_dimuon,
        minPt_dimuon=minPt_dimuon,
        minPt_muon=minPt_muon,
        maxVertexChi2=maxVertexChi2,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxTrackGhostProb_muon=maxTrackGhostProb_muon,
        minProbNN_muon=minProbNN_muon,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_dimuon_detached(
        dimuons,
        pvs,
        name="DiMuonDetachedFilter",
        minPt_muon=0 * MeV,
        minPt_dimuon=0 * MeV,
        #maxTrackChi2_muon=5, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxVertexChi2=25,
        maxTrackGhostProb_muon=0.4,
        minDecayLenghtSig=3):
    """Make the detached dimuon.
       Used in Run 1-2 by many dimuon exclusive lines, previously named DetachedCommonFilter"""

    code = require_all(
        "MINTREE('mu-' == ABSID,PT) > {minPt_muon}",
        "PT > {minPt_dimuon}",
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #"MAXTREE('mu-' == ABSID,TRCHI2DOF) < {maxTrackChi2_muon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)",
        "BPVDLS() > {minDecayLenghtSig}",
    ).format(
        minPt_muon=minPt_muon,
        minPt_dimuon=minPt_dimuon,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxVertexChi2=maxVertexChi2,
        maxTrackGhostProb_muon=maxTrackGhostProb_muon,
        minDecayLenghtSig=minDecayLenghtSig,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_dimuon_detached_geo(dimuons,
                             pvs,
                             name="DiMuonDetachedGeoFilter",
                             minIPChi2_muon=25,
                             minDecayLenghtSig=9):
    """Make the detached dimuon, with only geometrical requirements.
    Used in Run 2 by the HLT2DiMuonB2KSMuMu lines, previously named DetachedDiMuonFilter"""

    code = require_all(
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > {minIPChi2_muon}",
        "BPVDLS() > {minDecayLenghtSig}",
    ).format(
        minIPChi2_muon=minIPChi2_muon,
        minDecayLenghtSig=minDecayLenghtSig,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_dimuon_detached_soft(dimuons,
                              pvs,
                              name="DiMuonDetachedSoftFilter",
                              minIP_muon=0.3 * mm,
                              maxTrackGhostProb_muon=0.4,
                              minIPChi2_muon=9,
                              maxIPChi2_muon=10000000000000,
                              minProbNN_muon=0.05,
                              minRho=3,
                              maxVz=650 * mm,
                              maxIPdistRatio=1. / 60.,
                              maxMass_dimuon=1000 * MeV,
                              maxVertexChi2=25,
                              maxDOCA=0.3,
                              minVDz=0.,
                              minBPVDira=0.,
                              maxCosAngle=0.999998):
    """Make detached soft dimuon.
       Used by the HLT2DiMuonSoft line.
       Ref: http://cds.cern.ch/record/2297352/files/LHCb-PUB-2017-023.pdf"""

    code = require_all(
        "MINTREE('mu-' == ABSID, MIPDV(PRIMARY)) > {minIP_muon}",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)",
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > {minIPChi2_muon}",
        "MAXTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) < {maxIPChi2_muon}",
        "MINTREE('mu+'==ABSID,PROBNNmu) > {minProbNN_muon}",
        "VFASPF(sqrt(VX*VX+VY*VY)) > {minRho}",
        "VFASPF(VZ) < {maxVz}",
        "(MIPDV(PRIMARY)/BPVVDZ()) < {maxIPdistRatio}",
        "MM < {maxMass_dimuon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
        "DOCAMAX < {maxDOCA}",
        "BPVVDZ() > {minVDz}",
        "BPVDIRA() > {minBPVDira}",
        "PCUTA(ALV(1,2) < {maxCosAngle})",
    ).format(
        minIP_muon=minIP_muon,
        maxTrackGhostProb_muon=maxTrackGhostProb_muon,
        minIPChi2_muon=minIPChi2_muon,
        maxIPChi2_muon=maxIPChi2_muon,
        minProbNN_muon=minProbNN_muon,
        minRho=minRho,
        maxVz=maxVz,
        maxIPdistRatio=maxIPdistRatio,
        maxMass_dimuon=maxMass_dimuon,
        maxVertexChi2=maxVertexChi2,
        maxDOCA=maxDOCA,
        minVDz=minVDz,
        minBPVDira=minBPVDira,
        maxCosAngle=maxCosAngle,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_jpsi(name, dimuons, pvs, massWind_Jpsi=100 * MeV, minPt_Jpsi=0 * MeV):
    """Build the Jpsi"""

    code = require_all(
        'ADMASS("J/psi(1S)") < {massWind_Jpsi}',
        'PT > {minPt_Jpsi}',
    ).format(
        massWind_Jpsi=massWind_Jpsi,
        minPt_Jpsi=minPt_Jpsi,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_psi2s(name,
               dimuons,
               pvs,
               massWind_Psi2S=100 * MeV,
               minPt_Psi2S=0 * MeV,
               maxPt_Psi2S=100000 * GeV):
    """Build the Psi2S"""

    code = require_all(
        'ADMASS(3686.09) < {massWind_Psi2S}',
        'PT > {minPt_Psi2S}',
        'PT < {maxPt_Psi2S}',
    ).format(
        massWind_Psi2S=massWind_Psi2S,
        minPt_Psi2S=minPt_Psi2S,
        maxPt_Psi2S=maxPt_Psi2S,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_upsilon(name,
                 dimuons,
                 pvs,
                 minMass_dimuon=7900 * MeV,
                 maxVertexChi2=25,
                 minProbNN_muon=0.2):
    """Build the Upsilon"""

    code = require_all(
        'M > {minMass_dimuon}',
        'VFASPF(VCHI2PDOF) < {maxVertexChi2}',
        'MINTREE("mu-" == ABSID, PROBNNmu) > {minProbNN_muon}',
    ).format(
        minMass_dimuon=minMass_dimuon,
        maxVertexChi2=maxVertexChi2,
        minProbNN_muon=minProbNN_muon,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_z(name, dimuons, pvs, minMass_Z=40000 * MeV, minPt_Z=0 * MeV):
    """Build the Z"""

    code = require_all(
        'MM > {minMass_Z}',
        'PT > {minPt_Z}',
    ).format(
        minMass_Z=minMass_Z,
        minPt_Z=minPt_Z,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_b(
        name,
        minMass_dimuon=4700 * MeV,
        maxMass_dimuon=6500 * MeV,
        maxDOCA=0.9 * mm,
        minIPChi2=2,
        #maxTrackChi2_muon=5, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxVertexChi2=9):
    """Build the B"""

    code = require_all(
        "M > {minMass_dimuon}",
        "M < {maxMass_dimuon}",
        "DOCAMAX < {maxDOCA}",
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > {minIPChi2}",
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #"MAXTREE('mu-' == ABSID, TRCHI2DOF) < {maxTrackChi2_muon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
    ).format(
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxDOCA=maxDOCA,
        minIPChi2=minIPChi2,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxVertexChi2=maxVertexChi2,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_bub(
        name,
        minMass_dimuon=4700 * MeV,
        maxMass_dimuon=7000 * MeV,
        maxDOCA=0.9 * mm,
        min_lifetime_B=0.1 * picosecond,
        #maxTrackChi2_muon=5, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxVertexChi2=9):
    """Build the B"""

    code = require_all(
        "M > {minMass_dimuon}",
        "M < {maxMass_dimuon}",
        "DOCAMAX < {maxDOCA}",
        "BPVLTIME() > {min_lifetime_B}",
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #"MAXTREE('mu-' == ABSID, TRCHI2DOF) < {maxTrackChi2_muon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
    ).format(
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxDOCA=maxDOCA,
        min_lifetime_B=min_lifetime_B,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxVertexChi2=maxVertexChi2,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


@configurable
def make_b_prompt(
        name,
        minMass_dimuon=5000 * MeV,
        maxMass_dimuon=7000 * MeV,
        maxDOCA=0.1 * mm,
        maxIPChi2=1.,
        #maxTrackChi2_muon=5, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxVertexChi2=1.,
        maxTrackGhostProb_muon=0.2,
        minProbNN_muon=0.3):
    """Build the prompt B"""

    code = require_all(
        "M > {minMass_dimuon}",
        "M < {maxMass_dimuon}",
        "DOCAMAX < {maxDOCA}",
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) < {maxIPChi2}",
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #"MAXTREE('mu-' == ABSID, TRCHI2DOF) < {maxTrackChi2_muon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)",
        "MINTREE('mu-' == ABSID, PROBNNmu) > {minProbNN_muon}",
    ).format(
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxDOCA=maxDOCA,
        maxIPChi2=maxIPChi2,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxVertexChi2=maxVertexChi2,
        maxTrackGhostProb_mu=maxTrackGhostProb_muon,
        minProbNN_muon=minProbNN_muon,
    )

    return ParticleFilterWithPVs(dimuons, pvs, name=name, Code=code)


###########
# Definition of the lines
###########


@register_line_builder(all_lines)
@configurable
def soft_line(name="Hlt2DiMuonSoftLine", prescale=1):
    """Soft detached dimuon line.
    Characteristics: no pt requirements on the muons,
    no IP cuts on the dimuon, rejection of short-lived objects.
    Ref: http://cds.cern.ch/record/2297352/files/LHCb-PUB-2017-023.pdf
    Control the rate via the IP cut"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons (without pt requirements) and filter them
    base_dimuons = make_dimuon_base()
    dimuons_soft = make_dimuon_detached_soft(dimuons=base_dimuons, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [dimuons_soft],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_line(name="Hlt2DiMuonJPsiLine", prescale=1):
    """Prompt Jpsi line. Control the rate via the Jpsi mass window"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    base_dimuons = make_dimuon_base()
    filtered_dimuons = make_dimuon(
        dimuons=base_dimuons, pvs=pvs, minProbNN_muon=_PROBNNMU_JPSI)

    # get the Jpsi candidates
    Jpsi = make_jpsi(
        name="JpsiMaker_JpsiLine",
        dimuons=filtered_dimuons,
        pvs=pvs,
        massWind_Jpsi=_MASSWINDOW_JPSI,
        minPt_Jpsi=0 * MeV)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Jpsi],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def jpsihighpt_line(name="Hlt2DiMuonJPsiHighPTLine", prescale=1):
    """Prompt Jpsi line with high pt.
    Control the rate via the Jpsi mass window"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    base_dimuons = make_dimuon_base()
    filtered_dimuons = make_dimuon(
        dimuons=base_dimuons, pvs=pvs, minProbNN_muon=_PROBNNMU_JPSI)

    # get the Jpsi candidates
    Jpsi = make_jpsi(
        name="JpsiMaker_JpsiHighPTLine",
        dimuons=filtered_dimuons,
        pvs=pvs,
        massWind_Jpsi=_MASSWINDOW_JPSI,
        minPt_Jpsi=2000 * MeV)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Jpsi],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def psi2s_line(name="Hlt2DiMuonPsi2SLine", prescale=1):
    """Prompt Psi2S line. Control the rate via the Psi2s mass window"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    base_dimuons = make_dimuon_base()
    filtered_dimuons = make_dimuon(
        dimuons=base_dimuons, pvs=pvs, minProbNN_muon=_PROBNNMU_PSI2S)

    # get the Psi2s candidates
    Psi2S = make_psi2s(
        name="Psi2SMakes_Psi2SLine",
        dimuons=filtered_dimuons,
        pvs=pvs,
        massWind_Psi2S=_MASSWINDOW_PSI2S,
        minPt_Psi2S=0 * MeV)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Psi2S],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def psi2slowpt_line(name="Hlt2DiMuonPsi2SLowPTLine", prescale=1):
    """Prompt Psi2S line with low pt. Control the rate via the Psi2s mass window"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    base_dimuons = make_dimuon_base()
    filtered_dimuons = make_dimuon(
        dimuons=base_dimuons, pvs=pvs, minProbNN_muon=_PROBNNMU_PSI2S)

    # get the Psi2s candidates, with low pt
    Psi2S = make_psi2s(
        name="Psi2SMaker_Psi2SLowPTLine",
        dimuons=filtered_dimuons,
        pvs=pvs,
        massWind_Psi2S=_MASSWINDOW_PSI2S,
        minPt_Psi2S=0 * MeV,
        maxPt_Psi2S=2500 * MeV)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Psi2S],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def upsilon_line(name="Hlt2DiMuonUpsilonLine", prescale=1):
    """Prompt Upsilon line"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons
    base_dimuons = make_dimuon_base()

    # get the Upsilon candidates
    Upsilons = make_upsilon(
        name="UpsilonMaker_UpsilonLine", dimuons=base_dimuons, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Upsilons],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def z_line(name="Hlt2DiMuonZLine", prescale=1):
    """Z --> mu mu line"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons
    base_dimuons = make_dimuon_base()

    # get the Z candidates
    Z = make_z(name="ZMaker_ZLine", dimuons=base_dimuons, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Z],
        prescale=prescale,
    )


#@register_line_builder(all_lines)
#@configurable
#def b_line(name="Hlt2DiMuonBLine", prescale=1):
#    """B --> mu mu line"""
#
#    # get the PVs
#    pvs = make_pvs()
#
#    # get the basic dimuons and make the B
#    base_dimuons = make_dimuon_base()
#    filtered_dimuons = make_b(name="BMaker_BLine", dimuons=base_dimuons, pvs=pvs)
#
#    return HltLine(
#        name=name,
#        algs=upfront_reconstruction() + dimuon_prefilters() + [filtered_dimuons],
#        prescale=prescale,
#        )

#@register_line_builder(all_lines)
#@configurable
#def bub_line(name="Hlt2DiMuonBUBLine", prescale=1):
#    """B --> mu mu line"""
#
#    # get the PVs
#    pvs = make_pvs()
#
#    # get the basic dimuons and make the B
#    base_dimuons = make_dimuon_base()
#    filtered_dimuons = make_bub(name="BUBMaker_BUBLine", dimuons=base_dimuons, pvs=pvs)
#
#    return HltLine(
#        name=name,
#        algs=upfront_reconstruction() + dimuon_prefilters() + [filtered_dimuons],
#        prescale=prescale,
#        )

#@register_line_builder(all_lines)
#@configurable
#def bprompt_line(name="Hlt2DiMuonBPromptLine", prescale=1):
#    """B --> mu mu prompt line"""
#
#    # get the PVs
#    pvs = make_pvs()
#
#    # get the basic dimuons and make the B
#    base_dimuons = make_dimuon_base()
#    filtered_dimuons = make_b_prompt(name="BPromptFMaker_BPromptLine", dimuons=base_dimuons, pvs=pvs)
#
#    return HltLine(
#        name=name,
#        algs=upfront_reconstruction() + dimuon_prefilters() + [filtered_dimuons],
#        prescale=prescale,
#        )
