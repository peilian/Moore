###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive_radiative_b b builder
"""
from GaudiKernel.SystemOfUnits import GeV
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from PyConf import configurable


@configurable
def make_presel_b(kstars,
                  photons,
                  pvs,
                  descriptors,
                  apt_min=2.0 * GeV,
                  vtx_chi2_max=1000.,
                  eta_min=2.,
                  eta_max=5.,
                  corrm_min=1.0 * GeV,
                  corrm_max=11.0 * GeV,
                  dira_min=0.,
                  bpvvdchi2_min=0.):
    """Builds B->X gamma for inclusive_radiative_b selection"""
    combination_code = require_all("APT > {apt_min}",
                                   "AM < {corrm_max}").format(
                                       apt_min=apt_min, corrm_max=corrm_max)

    vertex_code = require_all(
        "HASVERTEX", "CHI2VXNDOF < {vtx_chi2_max}",
        "in_range({eta_min}, BPVETA(), {eta_max})",
        "in_range({corrm_min}, BPVCORRM(), {corrm_max})",
        "BPVDIRA() > {dira_min}", "BPVVDCHI2() > {bpvvdchi2_min}").format(
            vtx_chi2_max=vtx_chi2_max,
            eta_min=eta_min,
            eta_max=eta_max,
            corrm_min=corrm_min,
            corrm_max=corrm_max,
            dira_min=dira_min,
            bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        particles=[kstars, photons],
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)
