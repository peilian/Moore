###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive_radiative_b b TMVA selections (BDT)
"""
from Hlt2Conf.algorithms import ParticleFilterWithTMVA
from PyConf import configurable

hhg_vars = {
    "ipchi2":
    "log10(BPVIPCHI2())",
    'ipchi2_min':
    "log10(MINTREE(BPVIPCHI2(), ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0'))))",
    'gamma_pt':
    "CHILD(2, PT)",
    'm_corrected':
    "BPVCORRM()",
    'vm_corrected':
    "CHILD(1, BPVCORRM())",
    'fdchi2':
    "log10(BPVVDCHI2())",
    'vtx_chi2':
    "log10(VFASPF(VCHI2))",
    'doca':
    "CHILD(1, DOCA(1,2))"
}

hhgee_vars = {
    'mcor':
    "BPVCORRM()",
    'chi2':
    "VFASPF(VCHI2)",
    'sumpt':
    "SUMTREE(PT, ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), 0.0)/MeV",
    'eta':
    "BPVETA()",
    'fdchi2':
    "BPVVDCHI2()",
    'minpt':
    "MINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')), PT)/MeV",
    'nlt16':
    "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (BPVIPCHI2() < 16))",
    'ipchi2':
    "BPVIPCHI2()",
    'n1trk':
    "NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')|(ABSID=='gamma')) & (PT > 1*GeV) & (BPVIPCHI2() > 16))"
}

hhhg_vars = {
    'ipchi2':
    "log10(BPVIPCHI2())",
    'ipchi2_min':
    "log10(MINTREE(BPVIPCHI2(), ((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0'))))",
    'gamma_pt':
    "CHILD(2, PT)",
    'gamma_p':
    "CHILD(2, P)",
    'm_corrected':
    "BPVCORRM()",
    'fdchi2':
    "log10(BPVVDCHI2())",
    'vtx_chi2':
    "log10(VFASPF(VCHI2))",
    'chi2dof_max':
    "MAXTREE(TRCHI2DOF, ISBASIC & HASTRACK & (ABSID=='K+'))"
}

hhhgee_vars = hhgee_vars

bdt_vars = {
    "HHgamma": hhg_vars,
    "HHgammaEE": hhgee_vars,
    "HHHgamma": hhhg_vars,
    "HHHgammaEE": hhhgee_vars
}


@configurable
def make_b(presel_b, name, bdt_cut):
    mva_name = '{name}MVA'.format(name=name)
    mva_code = "VALUE('LoKi::Hybrid::DictValue/{mva_name}') > {bdt_cut}".format(
        bdt_cut=bdt_cut, mva_name=mva_name)
    xml_file = '$PARAMFILESROOT/data/Hlt2_Radiative_{name}.xml'.format(
        name=name)
    return ParticleFilterWithTMVA(name, presel_b, mva_code, mva_name, xml_file,
                                  bdt_vars[name])
