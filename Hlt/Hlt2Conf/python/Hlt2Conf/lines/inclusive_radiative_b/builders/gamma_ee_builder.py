###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive_radiative_b gamma->ee builder
"""
from GaudiKernel.SystemOfUnits import GeV, MeV
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from PyConf import configurable


@configurable
def make_gamma_ee(electrons,
                  pvs,
                  apt_min=1.5 * GeV,
                  ap_min=0.0 * GeV,
                  am_max=50.0 * MeV):

    combination_code = require_all("APT > {apt_min}", "AM < {am_max}",
                                   "AP > {ap_min}").format(
                                       apt_min=apt_min,
                                       am_max=am_max,
                                       ap_min=ap_min)

    vertex_code = require_all("ALL")

    return ParticleCombinerWithPVs(
        particles=[electrons],
        pvs=pvs,
        DecayDescriptors=["gamma -> e+ e-"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)
