# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the B2OC HLT2 lines
"""

from . import b_to_hhgamma
from . import b_to_hhgamma_gamma_to_ee
from . import b_to_hhhgamma
from . import b_to_hhhgamma_gamma_to_ee

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(b_to_hhgamma.all_lines)
all_lines.update(b_to_hhgamma_gamma_to_ee.all_lines)
all_lines.update(b_to_hhhgamma.all_lines)
all_lines.update(b_to_hhhgamma_gamma_to_ee.all_lines)
