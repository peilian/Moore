###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import HltLine, register_line_builder
from Hlt2Conf.standard_particles import make_long_electrons_no_brem, make_long_pions, make_photons
from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.inclusive_radiative_b.builders import basic_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import gamma_ee_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import hh_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import b_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import b_tmva_builder
all_lines = {}


@register_line_builder(all_lines)
def btohhgammaee_inclusive_line(name="Hlt2BToHHGammaee_Inclusive_Line",
                                prescale=1):
    pvs = make_pvs()
    electrons = basic_builder.filter_electrons(make_long_electrons_no_brem(),
                                               pvs)
    hadrons = basic_builder.filter_basic_hadrons(make_long_pions(), pvs)
    photons = gamma_ee_builder.make_gamma_ee(electrons, pvs)
    kstars = hh_builder.make_hh(hadrons, pvs)
    presel_b = b_builder.make_presel_b(kstars, photons, pvs,
                                       ["B0 -> K*(892)0 gamma"])
    b = b_tmva_builder.make_b(presel_b, "HHgammaEE", 0.084)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [b],
        prescale=prescale,
    )
