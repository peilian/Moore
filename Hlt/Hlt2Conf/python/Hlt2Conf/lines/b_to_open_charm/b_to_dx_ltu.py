###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDX LTUnbiased lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
all_lines = {}


@register_line_builder(all_lines)
@configurable
def BdToDsmK_DsmToHHH_ltu_line(name='Hlt2B2OC_BdToDsmK_DsmToHHH_LTU_Line',
                               prescale=0.1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D_s- K+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
