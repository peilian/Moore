###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DD lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}

##############################################
# B decaying to two charged charm mesons
##############################################


@register_line_builder(all_lines)
@configurable
def BdToDpDm_DpToHHH_line(name='Hlt2B2OC_BdToDpDm_DpToHHH_Line', prescale=1):
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(particles=[d], descriptors=['B0 -> D+ D-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDspDsm_DspToHHH_line(name='Hlt2B2OC_BdToDspDsm_DspToHHH_Line',
                             prescale=1):
    ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[ds], descriptors=['B0 -> D_s+ D_s-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDspDm_DspToHHH_DmToHHH_line(
        name='Hlt2B2OC_BdToDspDm_DspToHHH_DmToHHH_Line', prescale=1):
    ds = d_builder.make_dsplus_to_hhh()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[ds, d], descriptors=['[B0 -> D_s+ D-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH_line(
        name='Hlt2B2OC_BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dst, d], descriptors=['[B0 -> D*(2010)+ D-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH_line(
        name='Hlt2B2OC_BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH_Line',
        prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dst, d], descriptors=['[B0 -> D*(2010)+ D-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH_line(
        name='Hlt2B2OC_BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH_Line',
        prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dst, ds], descriptors=['[B0 -> D*(2010)+ D_s-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH_line(
        name='Hlt2B2OC_BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH_Line',
        prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dst, ds], descriptors=['[B0 -> D*(2010)+ D_s-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDstm_DstpToD0Pi_D0ToHH_line(
        name='Hlt2B2OC_BdToDstpDstm_DstpToD0Pi_D0ToHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst], descriptors=['B0 -> D*(2010)+ D*(2010)-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstpDstm_DstpToD0Pi_D0ToHHHH_line(
        name='Hlt2B2OC_BdToDstpDstm_DstpToD0Pi_D0ToHHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst], descriptors=['B0 -> D*(2010)+ D*(2010)-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# B decaying to two neutral charm mesons
##############################################


@register_line_builder(all_lines)
@configurable
def BdToD0D0_D0ToHH_line(name='Hlt2B2OC_BdToD0D0_D0ToHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[dzero], descriptors=['B0 -> D0 D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0D0_D0ToHHHH_line(name='Hlt2B2OC_BdToD0D0_D0ToHHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    line_alg = b_builder.make_b2x(
        particles=[dzero], descriptors=['B0 -> D0 D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# B decaying to one charged and one neutral charm meson
##############################################


@register_line_builder(all_lines)
@configurable
def BuToD0Dp_D0ToHH_DpToHHH_line(name='Hlt2B2OC_BuToD0Dp_D0ToHH_DpToHHH_Line',
                                 prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dp = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dzero, dp], descriptors=['B+ -> D0 D+', 'B- -> D0 D-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Dsp_D0ToHH_DspToHHH_line(
        name='Hlt2B2OC_BuToD0Dsp_D0ToHH_DspToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dp = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dzero, dp], descriptors=['B+ -> D0 D_s+', 'B- -> D0 D_s-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Dp_D0ToHHHH_DpToHHH_line(
        name='Hlt2B2OC_BuToD0Dp_D0ToHHHH_DpToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dp = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dzero, dp], descriptors=['B+ -> D0 D+', 'B- -> D0 D-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Dsp_D0ToHHHH_DspToHHH_line(
        name='Hlt2B2OC_BuToD0Dsp_D0ToHHHH_DspToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dp = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[dzero, dp], descriptors=['B+ -> D0 D_s+', 'B- -> D0 D_s-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Bc decaying to one charged and one neutral charm meson
##############################################


@register_line_builder(all_lines)
@configurable
def BcToD0Dp_D0ToHH_DpToHHH_line(name='Hlt2B2OC_BcToD0Dp_D0ToHH_DpToHHH_Line',
                                 prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dp = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dsp_D0ToHH_DspToHHH_line(
        name='Hlt2B2OC_BcToD0Dsp_D0ToHH_DspToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dp = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[dzero, dp],
        descriptors=['B_c+ -> D0 D_s+', 'B_c- -> D0 D_s-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dp_D0ToHHHH_DpToHHH_line(
        name='Hlt2B2OC_BcToD0Dp_D0ToHHHH_DpToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dp = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dsp_D0ToHHHH_DspToHHH_line(
        name='Hlt2B2OC_BcToD0Dsp_D0ToHHHH_DspToHHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dp = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[dzero, dp],
        descriptors=['B_c+ -> D0 D_s+', 'B_c- -> D0 D_s-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dst_DstToD0Pi_D0ToHH_D0ToHH_line(
        name='Hlt2B2OC_BcToD0Dst_DstToD0Pi_D0ToHH_D0ToHH_Line', prescale=1):
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_bc2x(
        particles=[dst, dzero],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dst_DstToD0Pi_D0ToHH_D0ToHHHH_line(
        name='Hlt2B2OC_BcToD0Dst_DstToD0Pi_D0ToHH_D0ToHHHH_Line', prescale=1):
    dzero1 = d_builder.make_dzero_to_hhhh()
    dzero2 = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    line_alg = b_builder.make_bc2x(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dst_D0ToHH_DstToD0Pi_D0ToHHHH_line(
        name='Hlt2B2OC_BcToD0Dst_DstToD0Pi_D0ToHHHH_D0ToHH_Line', prescale=1):
    dzero1 = d_builder.make_dzero_to_hh()
    dzero2 = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    line_alg = b_builder.make_bc2x(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)+ D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BcToD0Dst_D0ToHHHH_DstToD0Pi_D0ToHHHH_line(
        name='Hlt2B2OC_BcToD0Dst_DstToD0Pi_D0ToHHHH_D0ToHHHH_Line',
        prescale=1):
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_bc2x(
        particles=[dst, dzero],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
