###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached D/D0/Ds/... meson decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reco_objects_from_file import make_pvs

from Hlt2Conf.algorithms import (
    require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs,
    ParticleFilterWithPVs, N3BodyCombinerWithPVs, N4BodyCombinerWithPVs)
from Hlt2Conf.framework import configurable

from . import basic_builder

###############################################################################
# Generic D decay builders, defines default combination cuts                  #
###############################################################################


@configurable
def make_d_to_twobody(particles,
                      descriptors,
                      name="B2OCD2twobodyCombiner",
                      make_pvs=make_pvs,
                      asumpt_min=1800 * MeV,
                      am_min=1764.84 * MeV,
                      am_max=1964.84 * MeV,
                      adoca12_min=0.5 * mm,
                      vchi2pdof_max=10,
                      bpvvdchi2_min=36,
                      bpvdira_min=0):
    """
    A generic D->2body decay maker

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}",
                                   "ADOCA(1,2) < {adoca12_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min,
                                       adoca12_max=adoca12_min)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_d_to_threebody(particles,
                        descriptors,
                        name="B2OCD2threebodyCombiner",
                        make_pvs=make_pvs,
                        adoca12_max=0.5 * mm,
                        asumpt_min=1800 * MeV,
                        am_min=1789 * MeV,
                        am_max=1949 * MeV,
                        adoca13_max=0.5 * mm,
                        adoca23_max=0.5 * mm,
                        vchi2pdof_max=10,
                        bpvvdchi2_min=36,
                        bpvdira_min=0):
    """
    A generic D->3body decay maker. Makes use of N3BodyCombinerWithPVs
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = require_all("ADOCA(1,2) < {adoca12_max}").format(
        adoca12_max=adoca12_max)
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min)
    if adoca13_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(1,3) < {adoca13_max}").format(adoca13_max=adoca13_max)
    if adoca23_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(2,3) < {adoca23_max}").format(adoca23_max=adoca23_max)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return N3BodyCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_d_to_fourbody(particles,
                       descriptors,
                       name="B2OCD2fourbodyCombiner",
                       make_pvs=make_pvs,
                       asumpt_min=1800 * MeV,
                       am_min=1764.84 * MeV,
                       am_max=1964.84 * MeV,
                       adoca12_max=0.5 * mm,
                       adoca13_max=0.5 * mm,
                       adoca14_max=0.5 * mm,
                       adoca23_max=0.5 * mm,
                       adoca24_max=0.5 * mm,
                       adoca34_max=0.5 * mm,
                       vchi2pdof_max=10,
                       bpvvdchi2_min=36,
                       bpvdira_min=0):
    """
    A generic D->4body decay maker. Makes use of N4BodyCombinerWithPVs
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor* and then on the *3 first particles in the decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = require_all("ADOCA(1,2) < {adoca12_max}").format(
        adoca12_max=adoca12_max)
    combination123_code = require_all("ADOCA(1,3) < {adoca13_max}",
                                      "ADOCA(2,3) < {adoca23_max}").format(
                                          adoca13_max=adoca13_max,
                                          adoca23_max=adoca23_max)

    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min)
    if adoca14_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(1,4) < {adoca14_max}").format(adoca14_max=adoca14_max)
    if adoca24_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(2,4) < {adoca24_max}").format(adoca24_max=adoca24_max)
    if adoca34_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(3,4) < {adoca34_max}").format(adoca34_max=adoca34_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return N4BodyCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_excitedd_to_dneutral(particles,
                              descriptors,
                              child_meson,
                              name="B2OCDNeutralCombiner",
                              deltamass_max=250 * MeV,
                              deltamass_min=80 * MeV):

    combination_code = require_all("(AALL)")
    #mother_code = require_all(
    #    "(M-MAXTREE(ABSID=='D_s+',M) < {deltamass_max})",
    #    "(M-MINTREE(ABSID=='D_s+',M) > {deltamass_min})").format(
    #        deltamass_max=deltamass_max, deltamass_min=deltamass_min)
    #child_meson='D_s+'
    mother_code = require_all(
        "(M-MAXTREE(ABSID=='{d_meson}',M) < {deltamass_max})",
        "(M-MINTREE(ABSID=='{d_meson}',M) > {deltamass_min})").format(
            d_meson=child_meson,
            deltamass_max=deltamass_max,
            deltamass_min=deltamass_min)

    return ParticleCombiner(
        name=name,
        particles=particles,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=mother_code)


###############################################################################
# Specific D decay builders, overrides default cuts where needed              #
###############################################################################


@configurable
def make_dzero_to_hh(**decay_arguments):
    """
    Defines the default B2OC D0->h+h- decay

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        'D0 -> pi+ pi-', 'D0 -> K+ K-', 'D0 -> pi+ K-', 'D0 -> K+ pi-'
    ]
    return make_d_to_twobody(
        particles, descriptors, name="B2OCD02HHCombiner", **decay_arguments)


@configurable
def make_dzero_to_hhhh(**decay_arguments):
    """
    Defines the default B2OC D0->h+h+h-h- decay

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->4body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        'D0 -> pi+ pi+ pi- pi-',
        'D0 -> K+ pi+ pi- pi-',
        'D0 -> pi+ pi+ K- pi-',
        'D0 -> K+ pi+ K- pi-',
    ]
    return make_d_to_fourbody(
        particles, descriptors, name="B2OCD02HHHHCombiner", **decay_arguments)


@configurable
def make_dzero_to_kshh(k_shorts, **decay_arguments):
    """
    Defines the default B2OC D->Ks hh decay

    Parameters
    ----------
    k_shorts :
      selected KS candidate maker (typically choose LL or DD)
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    descriptors = [
        'D0 -> pi+ pi- KS0', 'D0 -> K+ K- KS0', 'D0 -> pi+ K- KS0',
        'D0 -> K+ pi- KS0'
    ]
    particles = [
        k_shorts,
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    return make_d_to_threebody(
        particles,
        descriptors,
        name="B2OCD02KSHHCombiner",
        adoca13_max=None,
        adoca23_max=None,
        **decay_arguments)


@configurable
def make_dplus_to_hhh(am_min=1830 * MeV, am_max=1910 * MeV, **decay_arguments):
    """
    Return a D+ -> h+h-h+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        '[D+ -> pi+ pi+ K-]cc', '[D+ -> pi+ K+ K-]cc', '[D+ -> pi+ pi+ pi-]cc'
    ]
    return make_d_to_threebody(
        particles,
        descriptors,
        name="B2OCD2HHHCombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_dplus_to_pippipkm(**decay_arguments):

    dp = make_dplus_to_hhh(**decay_arguments)
    code = "(NINTREE(ABSID=='pi+')==2) & (NINTREE(ABSID=='K-')==1)"

    return ParticleFilter(particles=dp, Code=code)


@configurable
def make_dsplus_to_hhh(am_min=1930 * MeV, am_max=2010 * MeV,
                       **decay_arguments):
    """
    Return a D_s+ -> h+h-h+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        '[D_s+ -> pi+ pi+ pi-]cc',
        '[D_s+ -> pi+ K+ pi-]cc',
        '[D_s+ -> pi+ K+ K-]cc',
    ]
    return make_d_to_threebody(
        particles,
        descriptors,
        name="B2OCDS2HHHCombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_dstar_to_dzeropi(dzero,
                          name='B2OCDstar2D0PiCombiner',
                          make_pvs=make_pvs,
                          adoca12_max=0.5 * mm,
                          mass_window=600 * MeV,
                          deltamass_max=200 * MeV,
                          deltamass_min=90 * MeV,
                          vchi2pdof_max=10,
                          bpvvdchi2_min=36,
                          bpvdira_min=0):
    """
    Return D*+ maker for tagging a D0 with a soft pion.

    Parameters
    ----------
    make_pvs : callable
        Primary vertex maker function.
        Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all("(ADAMASS('D*(2010)+') < {mass_window})",
                                   "ADOCA(1,2) < {adoca12_max}").format(
                                       mass_window=mass_window,
                                       adoca12_max=adoca12_max)
    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}",
        "(M-MAXTREE(ABSID=='D0',M) < {deltamass_max})",
        "(M-MINTREE(ABSID=='D0',M) > {deltamass_min})").format(
            vchi2pdof_max=vchi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvdira_min=bpvdira_min,
            deltamass_max=deltamass_max,
            deltamass_min=deltamass_min)

    soft_pions = basic_builder.make_pions()
    descriptors = ['D*(2010)- -> pi- D0', 'D*(2010)+ -> pi+ D0']

    return ParticleCombinerWithPVs(
        name=name,
        particles=[dzero, soft_pions],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dsst_to_dsplusgamma(ds=make_dsplus_to_hhh(), **decay_arguments):

    particles = [ds, basic_builder.make_photons()]
    descriptors = ['[D*_s+ -> D_s+ gamma]cc']
    child_meson = 'D_s+'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCDsGammaCombiner")


@configurable
def make_dzerost_to_dzerogamma(dzero, **decay_arguments):

    particles = [dzero, basic_builder.make_photons()]
    descriptors = ['D*(2007)0 -> D0 gamma']
    child_meson = 'D0'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCD0GammaCombiner")


@configurable
def make_dzerost_to_dzeropi0(dzero, pi0, **decay_arguments):

    particles = [dzero, pi0]
    descriptors = ['D*(2007)0 -> D0 pi0']
    child_meson = 'D0'

    return make_excitedd_to_dneutral(
        particles, descriptors, child_meson, name="B2OCD0Pi0Combiner")
