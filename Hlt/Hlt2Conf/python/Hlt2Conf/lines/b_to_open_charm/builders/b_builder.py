###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds B2OC B decays, and defines the common default cuts applied to the B2X combinations
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond
from Hlt2Conf.algorithms import require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs, ParticleFilterWithPVs, N3BodyCombinerWithPVs
from Hlt2Conf.framework import configurable
from RecoConf.reco_objects_from_file import make_pvs


@configurable
def make_b2x(
        particles,
        descriptors,
        name="B2OCB2XCombiner",
        am_min=5050 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20,  # was 10 in Run1+2
        bpvipchi2_max=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999):
    '''
    Default B decay maker: defines default vtc chi2 and B mass range.
    '''
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {sum_pt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       sum_pt_min=sum_pt_min)

    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}",
                              "BPVIPCHI2() < {bpvipchi2_max}",
                              "BPVLTIME() > {bpvltime_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvipchi2_max=bpvipchi2_max,
                                  bpvltime_min=bpvltime_min,
                                  bpvdira_min=bpvdira_min)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_lifetime_unbiased_b2x(particles,
                               descriptors,
                               name="B2OCB2XLTUCombiner",
                               am_min=5050 * MeV,
                               am_max=5650 * MeV,
                               am_min_vtx=5050 * MeV,
                               am_max_vtx=5650 * MeV,
                               sum_pt_min=5 * GeV,
                               vtx_chi2pdof_max=20):  # was 10 in Run1+2
    '''
    LifeTime Unbiased B decay maker: defines default vtx chi2 and B mass range.
    '''
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {sum_pt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       sum_pt_min=sum_pt_min)

    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vtx_chi2pdof_max=vtx_chi2pdof_max)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_b2dhh(
        particles,
        descriptors,
        name="B2OCB2DHHCombiner",
        am_min=5000 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=6000 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20,  # was 10 in Run1+2
        bpvipchi2_max=9,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.99995,
        hh_asumpt_min=1 * GeV,
        hh_am_max=5.2 * GeV,
        hh_adoca12_max=0.5 * mm,
        hbach_pt_min=500 * MeV,
        hbach_p_min=5 * GeV,
):
    '''
    Specialised 3-body B decay maker
    '''
    combination12_code = require_all(
        "ASUM(PT) > {asumpt_min}", "AM < {am_max}",
        "ADOCA(1,2) < {adoca12_max}",
        "AHASCHILD(ISBASIC & HASTRACK & (PT > {pt_min}) & (P > {p_min}) )"
    ).format(
        asumpt_min=hh_asumpt_min,
        am_max=hh_am_max,
        adoca12_max=hh_adoca12_max,
        pt_min=hbach_pt_min,
        p_min=hbach_p_min)

    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {sum_pt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       sum_pt_min=sum_pt_min)

    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}",
                              "BPVIPCHI2() < {bpvipchi2_max}",
                              "BPVLTIME() > {bpvltime_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvipchi2_max=bpvipchi2_max,
                                  bpvltime_min=bpvltime_min,
                                  bpvdira_min=bpvdira_min)

    return N3BodyCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_bc2x(particles,
              descriptors,
              name="B2OCBC2XCombiner",
              am_min=4800 * MeV,
              am_max=6800 * MeV,
              am_min_vtx=4800 * MeV,
              am_max_vtx=6800 * MeV,
              sum_pt_min=5 * GeV,
              vtx_chi2pdof_max=20,
              bpvipchi2_max=20,
              bpvltime_min=0.05 * picosecond,
              bpvdira_min=0.999):

    return make_b2x(particles, descriptors, name, am_min, am_max, am_min_vtx,
                    am_max_vtx, sum_pt_min, vtx_chi2pdof_max, bpvipchi2_max,
                    bpvltime_min, bpvdira_min)
