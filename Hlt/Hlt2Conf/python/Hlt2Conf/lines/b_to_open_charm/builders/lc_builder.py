###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached Lc baryon decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reco_objects_from_file import make_pvs

from Hlt2Conf.algorithms import (
    require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs,
    ParticleFilterWithPVs, N3BodyCombinerWithPVs, N4BodyCombinerWithPVs)
from Hlt2Conf.framework import configurable

from . import basic_builder

################################################################################
# Generic Lc decay builders, defines default combination cuts                  #
################################################################################


@configurable
def make_lc_to_pkpi(name="B2OCLc2PKPiCombiner",
                    make_pvs=make_pvs,
                    adoca12_max=0.5 * mm,
                    asumpt_min=1800 * MeV,
                    am_min=2188 * MeV,
                    am_max=2388 * MeV,
                    adoca13_max=0.5 * mm,
                    adoca23_max=0.5 * mm,
                    vchi2pdof_max=10,
                    bpvvdchi2_min=36,
                    bpvdira_min=0):
    """
    A Lc->pKpi decay maker. Makes use of N3BodyCombinerWithPVs
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    particles = [
        basic_builder.make_pions(),
        basic_builder.make_kaons(),
        basic_builder.make_protons()
    ]
    descriptors = ["[Lambda_c+ -> p+ K- pi+]cc"]
    combination12_code = require_all("ADOCA(1,2) < {adoca12_max}").format(
        adoca12_max=adoca12_max)
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min)
    if adoca13_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(1,3) < {adoca13_max}").format(adoca13_max=adoca13_max)
    if adoca23_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(2,3) < {adoca23_max}").format(adoca23_max=adoca23_max)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return N3BodyCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)
