###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2OC basic objects: pions, kaons, ...
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reco_objects_from_file import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs, ParticleFilterWithPVs
from Hlt2Conf.framework import configurable
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_kaons,
    make_has_rich_long_protons, make_long_pions, make_down_pions, make_photons,
    make_resolved_pi0s, make_merged_pi0s, make_KsLL, make_KsDD)

####################################
# Track selections                 #
####################################


@configurable
def make_selected_particles(make_particles=make_has_rich_long_pions,
                            make_pvs=make_pvs,
                            trchi2todof_max=3,
                            mipchi2_min=4,
                            pt_min=250 * MeV,
                            p_min=2 * GeV,
                            pid=None):

    code = require_all('PT > {pt_min}', 'P > {p_min}',
                       'TRCHI2DOF < {trchi2todof_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           trchi2todof_max=trchi2todof_max,
                           mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_pions(pid='PIDK < 5'):
    """Return pions filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions, pid=pid)


@configurable
def make_kaons(pid='PIDK > -5'):
    """Return kaons filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons, pid=pid)


@configurable
def make_protons(pid='PIDp > -5'):
    """Return protons filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_protons, pid=pid)


@configurable
def make_bachelor_pions(pid='PIDK < 20', p_min=5 * GeV, pt_min=500 * MeV):
    """Return pions filtered by thresholds common to B2OC bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


@configurable
def make_soft_bachelor_pions(pid='PIDK < 20', p_min=2 * GeV, pt_min=100 * MeV):
    """Return pions filtered by thresholds common to B2OC very soft bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


@configurable
def make_bachelor_kaons(pid='PIDK > -10', p_min=5 * GeV, pt_min=500 * MeV):
    """Return kaons filtered by thresholds common to B2OC bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


@configurable
def make_soft_bachelor_kaons(pid='PIDK > -10', p_min=2 * GeV,
                             pt_min=100 * MeV):
    """Return kaons filtered by thresholds common to B2OC very soft bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


####################################
# Neutral objects selections       #
####################################


@configurable
def make_photons(make_particles=make_photons, CL_min=0.2, et_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all('PT > {et_min}').format(et_min=et_min)

    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_resolved_pi0s(make_particles=make_resolved_pi0s, pt_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all('PT > {pt_min}').format(pt_min=pt_min)

    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_merged_pi0s(make_particles=make_merged_pi0s, pt_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all('PT > {pt_min}').format(pt_min=pt_min)

    return ParticleFilter(make_particles(), Code=code)


####################################
# 2-body decays                    #
####################################


@configurable
def make_selected_ks(input_ks,
                     name="B2OC_KS_Filter",
                     adocachi2cut=30,
                     pi_pmin=2 * GeV,
                     pi_mipchi2pv=9.,
                     chi2vx=30,
                     bpvvdchi2=None):
    '''
    Filters Kshort candidates for B2OC. Default cuts correspond to VeryLooseKSLL from the
    Run2 CommonParticles
    '''
    code = require_all(
        'DOCACHI2MAX < {adocachi2cut}',
        'NINTREE((ABSID=="pi+") & (P > {pi_pmin}))>1.5',
        'NINTREE((ABSID=="pi+") & (MIPCHI2DV(PRIMARY) > {pi_mipchi2pv}))>1.5',
        'CHI2VX < {chi2vx}').format(
            adocachi2cut=adocachi2cut,
            pi_pmin=pi_pmin,
            pi_mipchi2pv=pi_mipchi2pv,
            chi2vx=chi2vx)
    if bpvvdchi2 is not None:
        code += " & (BPVVDCHI2()>{})".format(bpvvdchi2)
    return ParticleFilterWithPVs(
        name=name, particles=input_ks, pvs=make_pvs(), Code=code)


@configurable
def make_ks_LL(make_ks=make_KsLL,
               adocachi2cut=30,
               pi_pmin=2 * GeV,
               pi_mipchi2pv=9.,
               chi2vx=30,
               bpvvdchi2=4.):
    '''
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL.
    '''
    return make_selected_ks(
        input_ks=make_ks(),
        name="B2OC_KsLL_Filter",
        adocachi2cut=adocachi2cut,
        pi_pmin=pi_pmin,
        pi_mipchi2pv=pi_mipchi2pv,
        chi2vx=chi2vx,
        bpvvdchi2=bpvvdchi2)


@configurable
def make_ks_DD(make_ks=make_KsDD,
               adocachi2cut=25,
               pi_pmin=2 * GeV,
               pi_mipchi2pv=4.,
               chi2vx=25):
    '''
    Builds DD Kshorts, currently corresponding to the Run2
    StdLooseKSDD.
    '''
    return make_selected_ks(
        input_ks=make_ks(),
        name="B2OC_KsDD_Filter",
        adocachi2cut=adocachi2cut,
        pi_pmin=pi_pmin,
        pi_mipchi2pv=pi_mipchi2pv,
        chi2vx=chi2vx)


# TODO: LD K-shorts? These will need progress on issue #102


@configurable
def make_kstar0(name="B2OCKstarBuilder",
                make_pions=make_pions,
                make_kaons=make_kaons,
                make_pvs=make_pvs,
                am_min=742 * MeV,
                am_max=1042 * MeV,
                pi_pmin=2 * GeV,
                pi_ptmin=100 * MeV,
                k_pmin=2 * GeV,
                k_ptmin=100 * MeV,
                adoca12_max=0.5 * mm,
                asumpt_min=1000 * MeV,
                bpvvdchi2_min=16,
                vchi2pdof_max=16):
    '''
    Build Kstar0 candidates. Currently corresponding to the Run2
    "HH" cuts.
    '''

    pions = make_pions()
    kaons = make_kaons()

    descriptors = ['K*(892)0 -> pi- K+', 'K*(892)~0 -> pi+ K-']
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}",
                                   "ADOCA(1,2) < {adoca12_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min,
                                       adoca12_max=adoca12_max)
    daughters_code = {
        "pi-":
        "(P > {pi_pmin}) & (PT > {pi_ptmin})".format(
            pi_pmin=pi_pmin, pi_ptmin=pi_ptmin),
        "K+":
        "(P > {k_pmin}) & (PT > {k_ptmin})".format(
            k_pmin=k_pmin, k_ptmin=k_ptmin)
    }
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                              "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min)
    return ParticleCombinerWithPVs(
        name=name,
        particles=[pions, kaons],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)
