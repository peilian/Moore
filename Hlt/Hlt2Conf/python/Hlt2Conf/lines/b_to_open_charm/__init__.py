# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the B2OC HLT2 lines
"""

from . import b_to_dh
from . import b_to_dhh
from . import b_to_dhhh
from . import b_to_dd
from . import b_to_ddh
from . import lb_to_lc
from . import b_to_dx_ltu

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(b_to_dh.all_lines)
all_lines.update(b_to_dhh.all_lines)
all_lines.update(b_to_dhhh.all_lines)
all_lines.update(b_to_dd.all_lines)
all_lines.update(b_to_ddh.all_lines)
all_lines.update(lb_to_lc.all_lines)
all_lines.update(b_to_dx_ltu.all_lines)
