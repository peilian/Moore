###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDhh lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}

##############################################
# BdToD0hh lines
##############################################


@register_line_builder(all_lines)
@configurable
def BdToD0PiPi_D0ToHH_line(name='Hlt2B2OC_BdToD0PiPi_D0ToHH_Line', prescale=1):
    bachelors = basic_builder.make_soft_bachelor_pions()
    d = d_builder.make_dzero_to_hh()
    # note the order we write the decay descriptor in because the b2dhh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2dhh(
        particles=[bachelors, d], descriptors=['B0 -> pi+ pi- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0PiPiWS_D0ToHH_line(name='Hlt2B2OC_BdToD0PiPiWS_D0ToHH_Line',
                             prescale=1):
    bachelors = basic_builder.make_soft_bachelor_pions()
    d = d_builder.make_dzero_to_hh()
    # note the order we write the decay descriptor in because the b2dhh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2dhh(
        particles=[bachelors, d],
        descriptors=['B0 -> pi+ pi+ D0', 'B0 -> pi- pi- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0KPi_D0ToHH_line(name='Hlt2B2OC_BdToD0KPi_D0ToHH_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_bachelor_pions()
    bachelork = basic_builder.make_soft_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2dhh(
        particles=[bachelork, bachelorpi, d],
        descriptors=['B0 -> K+ pi- D0', 'B0 -> K- pi+ D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH_line(
        name='Hlt2B2OC_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH_Line',
        prescale=1):
    bachelorpi = basic_builder.make_soft_bachelor_pions()
    bachelork = basic_builder.make_soft_bachelor_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2dhh(
        particles=[bachelork, bachelorpi, dzerost],
        descriptors=['B0 -> K+ pi- D*(2007)0', 'B0 -> K- pi+ D*(2007)0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDst0KPi_Dst0ToD0Pi0Merged_D0ToHH_line(
        name='Hlt2B2OC_BdToDst0KPi_Dst0ToD0Pi0Merged_D0ToHH_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_bachelor_pions()
    bachelork = basic_builder.make_soft_bachelor_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2dhh(
        particles=[bachelork, bachelorpi, dzerost],
        descriptors=['B0 -> K+ pi- D*(2007)0', 'B0 -> K- pi+ D*(2007)0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDst0KPi_Dst0ToD0Gamma_D0ToHH_line(
        name='Hlt2B2OC_BdToDst0KPi_Dst0ToD0Gamma_D0ToHH_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_bachelor_pions()
    bachelork = basic_builder.make_soft_bachelor_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2dhh(
        particles=[bachelork, bachelorpi, dzerost],
        descriptors=['B0 -> K+ pi- D*(2007)0', 'B0 -> K- pi+ D*(2007)0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0KPiWS_D0ToHH_line(name='Hlt2B2OC_BdToD0KPiWS_D0ToHH_Line',
                            prescale=1):
    bachelorpi = basic_builder.make_soft_bachelor_pions()
    bachelork = basic_builder.make_soft_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2dhh(
        particles=[bachelork, bachelorpi, d],
        descriptors=['B0 -> K+ pi+ D0', 'B0 -> K- pi- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0KK_D0ToHH_line(name='Hlt2B2OC_BdToD0KK_D0ToHH_Line', prescale=1):
    bachelors = basic_builder.make_soft_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2dhh(
        particles=[bachelors, d], descriptors=['B0 -> K+ K- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0KKWS_D0ToHH_line(name='Hlt2B2OC_BdToD0KKWS_D0ToHH_Line', prescale=1):
    bachelors = basic_builder.make_soft_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2dhh(
        particles=[bachelors, d],
        descriptors=['B0 -> K+ K+ D0', 'B0 -> K- K- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH_line(
        name='Hlt2B2OC_BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH_Line',
        prescale=1):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_bachelor_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma()
    line_alg = b_builder.make_b2x(
        particles=[dsst, pion, ks_ll],
        descriptors=['B0 -> D*_s- KS0 pi+', 'B0 -> D*_s+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH_line(
        name='Hlt2B2OC_BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH_Line',
        prescale=1):
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_bachelor_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma()
    line_alg = b_builder.make_b2x(
        particles=[dsst, pion, ks_dd],
        descriptors=['B0 -> D*_s- KS0 pi+', 'B0 -> D*_s+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmKsDDPi_DsmToHHH_line(name='Hlt2B2OC_BdToDsmKsDDPi_DsmToHHH_Line',
                                prescale=1):
    ds = d_builder.make_dsplus_to_hhh()
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_bachelor_pions()
    line_alg = b_builder.make_b2x(
        particles=[ds, ks_dd, pion],
        descriptors=['B0 -> D_s- KS0 pi+', 'B0 -> D_s+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmKsLLPi_DsmToHHH_line(name='Hlt2B2OC_BdToDsmKsLLPi_DsmToHHH_Line',
                                prescale=1):
    ds = d_builder.make_dsplus_to_hhh()
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_bachelor_pions()
    line_alg = b_builder.make_b2x(
        particles=[ds, ks_ll, pion],
        descriptors=['B0 -> D_s- KS0 pi+', 'B0 -> D_s+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmKsDDPi_DmToHHH_line(name='Hlt2B2OC_BdToDmKsDDPi_DmToHHH_Line',
                              prescale=1):
    d = d_builder.make_dplus_to_hhh()
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_bachelor_pions()
    line_alg = b_builder.make_b2x(
        particles=[d, ks_dd, pion],
        descriptors=['B0 -> D- KS0 pi+', 'B0 -> D+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmKsLLPi_DmToHHH_line(name='Hlt2B2OC_BdToDmKsLLPi_DmToHHH_Line',
                              prescale=1):
    d = d_builder.make_dplus_to_hhh()
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_bachelor_pions()
    line_alg = b_builder.make_b2x(
        particles=[d, ks_ll, pion],
        descriptors=['B0 -> D- KS0 pi+', 'B0 -> D+ KS0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
