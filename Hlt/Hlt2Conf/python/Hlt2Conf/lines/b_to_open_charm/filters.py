###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* B-meson BDT filters:
* _ b_bdt_filter: could be applied to ANY line
* _ b_to_dh_bdt_filter: to be applied to Dh lines (D0/D+/Ds+/... + pi/K/...)
* _ BDT .xml files are in https://gitlab.cern.ch/lhcb-datapkg/ParamFiles/data
"""
from __future__ import absolute_import, division, print_function

from Hlt2Conf.algorithms import require_all, ParticleFilterWithTMVA

import math


def b_bdt_filter(particles, MVACut=-0.2):
    BDTWeightsFile = "$PARAMFILESROOT/data/b_bdt_weights.xml"
    BDTVars = {
        "log_B_PT": "math.log(PT)",
        "B_ETA": "ETA",
        "log_B_DIRA": "math.log(1.-BPVDIRA())",
        "log_B_ENDVERTEX_CHI2": "math.log(VFASPF(VCHI2/VDOF))",
        "log_B_IPCHI2_OWNPV": "math.log(BPVIPCHI2())",
        "log_B_IP_OWNPV": "math.log(BPVIP())"
    }
    code = require_all("VALUE('LoKi::Hybrid::DictValue/BeautyBDT')>{MVACut}"
                       ).format(MVACut=MVACut)
    return ParticleFilterWithTMVA(
        name="B2OCBBDTFilter",
        particles=particles,
        mva_code=code,
        mva_name="BeautyBDT",
        xml_file=BDTWeightsFile,
        bdt_vars=BDTVars)


def b_to_dh_bdt_filter(particles, MVACut=-0.2):
    BDTWeightsFile = "$PARAMFILESROOT/data/b_to_dh_bdt_weights.xml"
    BDTVars = {
        "log_B_PT": "math.log(PT)",
        "B_ETA": "ETA",
        "log_B_DIRA": "math.log(1.-BPVDIRA())",
        "log_B_ENDVERTEX_CHI2": "math.log(VFASPF(VCHI2/VDOF))",
        "log_B_IPCHI2_OWNPV": "math.log(BPVIPCHI2())",
        "log_B_IP_OWNPV": "math.log(BPVIP())",
        "log_D_PT": "math.log(CHILD(PT,1))",
        "log_Bac_PT": "math.log(CHILD(PT,2))",
        "log_D_MINIPCHI2": "math.log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "log_Bac_MINIPCHI2": "math.log(CHILD(MIPCHI2DV(PRIMARY),2))"
    }
    code = require_all("VALUE('LoKi::Hybrid::DictValue/B2DHBDT')>{MVACut}"
                       ).format(MVACut=MVACut)
    return ParticleFilterWithTMVA(
        name="B2OCB2DHBDTFilter",
        particles=particles,
        mva_code=code,
        mva_name="B2DHBDT",
        xml_file=BDTWeightsFile,
        bdt_vars=BDTVars)
