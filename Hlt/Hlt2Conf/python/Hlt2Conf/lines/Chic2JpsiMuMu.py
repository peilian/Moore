###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the Chic2JpsiMuMu line
"""

from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reco_objects_from_file import (make_pvs, upfront_reconstruction)

from ..algorithms import require_all, ParticleFilterWithPVs, ParticleCombinerWithPVs
from ..framework import configurable

from Moore.config import HltLine, register_line_builder

# get the basic particles
from ..standard_particles import make_dimuon_base, make_ismuon_long_muon

# get the dimuon filters
from .dimuon import dimuon_prefilters, make_dimuon, make_jpsi

all_lines = {}


@configurable
def make_chic2jpsimumu(name,
                       Jpsi,
                       dimuons,
                       pvs,
                       massWind_Chic=20 * MeV,
                       maxVertexChi2=25):
    """Make the Chi_c --> Jpsi Mu Mu candidate"""

    # mass window around (chic_1 + chic_2)/2.
    combination_cut = require_all("ADAMASS(3530.0) < {massWind_Chic}").format(
        massWind_Chic=massWind_Chic)

    mother_cut = require_all("VFASPF(VCHI2PDOF) < {maxVertexChi2}").format(
        maxVertexChi2=maxVertexChi2)

    return ParticleCombinerWithPVs(
        name=name,
        particles=[Jpsi, dimuons],
        pvs=pvs,
        DecayDescriptors=["chi_c1(1P) -> J/psi(1S) mu+ mu-"],
        CombinationCut=combination_cut,
        MotherCut=mother_cut)


###########
# Definition of the line
###########


@register_line_builder(all_lines)
@configurable
def chic2jpsimumu_line(name="Hlt2DiMuonChicJpsiDiMuonLine", prescale=1):
    """Chic --> Jpsi Mu Mu line
    This lines requires very loose (if none) pt requirements."""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    base_dimuons = make_dimuon_base()
    filtered_dimuons = make_dimuon(dimuons=base_dimuons, pvs=pvs)

    # get the Jpsi, without pt requirements
    Jpsi = make_jpsi(
        name="JpsiMaker_ChicJpsiDiMuonLine",
        dimuons=filtered_dimuons,
        pvs=pvs,
        massWind_Jpsi=80 * MeV,
        minPt_Jpsi=0 * MeV)

    # get the basic muons
    base_muons = make_ismuon_long_muon()

    # make the candidate, out of the Jpsi and the basic dimuons
    Chic2JpsiMuMu = make_chic2jpsimumu(
        name="Chic2JpsiMuMuMaker_ChicJpsiDiMuonLine",
        Jpsi=Jpsi,
        dimuons=base_muons,
        pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + dimuon_prefilters() + [Chic2JpsiMuMu],
        prescale=prescale,
    )
