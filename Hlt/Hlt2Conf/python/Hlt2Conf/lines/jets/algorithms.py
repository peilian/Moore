###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of a prototype jet line

The ultimate goal is to create a line that does the same thing as the previous
suite of Hlt2Lines/Jets lines.
"""
from __future__ import absolute_import, division, print_function
import string

from PyConf.components import force_location

from PyConf.Algorithms import HltJetBuilder, HltParticleFlow

###############################################################################
"""Define a wrapper for original HltParticleFlow algorithm.

This involves some duplicaton of code in ..algorithms, but this may be
unavoidable due to unusual features of the original HltParticleFlow algorithm:
  - It inherits from GaudiHistoAlg rather than from DVAlgorithm, so it does
    not have some of the properties set by the ..algorithms wrappers.
  - Its Inputs property is not just a list of TES locations.  It is a list of
    3-tuples of strings, ('ObjectClass', 'processing type', 'location').
    In the C++ it is implemented as
      vector< vector<string> > m_inLocs;

It looks like in order to use HltParticleFlow
we need an input_transform from an object
that can be interpreted as a simple location to the 3-tuple expected by
the algorithm.  I am not sure how to do this in a general way.
"""


def _hltparticleflow_inputs(inparts=[], inprots=[], inclust=[]):  # {
    """Return a dict suitable for a HltParticleFlow input transform."""
    lpa = [['Particle', 'particle', pin] for pin in inparts]
    lpr = [['ProtoParticle', 'best', pin] for pin in inprots]
    lcl = [['CaloCluster', 'gamma', pin] for pin in inclust]
    d = {'Inputs': lpa + lpr + lcl, 'ExtraInputs': inparts + inprots + inclust}
    return d


# }


def _hltparticleflow_inputs_1_0_0(ParticlesA):
    return _hltparticleflow_inputs(inparts=[ParticlesA])


def _hltparticleflow_inputs_2_0_0(ParticlesA, ParticlesB):
    return _hltparticleflow_inputs(inparts=[ParticlesA, ParticlesB])


def _hltparticleflow_inputs_3_0_0(ParticlesA, ParticlesB, ParticlesC):
    return _hltparticleflow_inputs(
        inparts=[ParticlesA, ParticlesB, ParticlesC])


def _hltparticleflow_inputs_4_0_0(ParticlesA, ParticlesB, ParticlesC,
                                  ParticlesD):
    return _hltparticleflow_inputs(
        inparts=[ParticlesA, ParticlesB, ParticlesC, ParticlesD])


def _hltparticleflow_inputs_0_1_0(ProtoParticlesA):
    return _hltparticleflow_inputs(inprots=[ProtoParticlesA])


def _hltparticleflow_inputs_0_2_0(ProtoParticlesA, ProtoParticlesB):
    return _hltparticleflow_inputs(inprots=[ProtoParticlesA, ProtoParticlesB])


def _hltparticleflow_inputs_1_1_0(ParticlesA, ProtoParticlesA):
    return _hltparticleflow_inputs(
        inparts=[ParticlesA], inprots=[ProtoParticlesA])


def _hltparticleflow_inputs_1_2_0(ParticlesA, ProtoParticlesA,
                                  ProtoParticlesB):
    return _hltparticleflow_inputs(
        inparts=[ParticlesA], inprots=[ProtoParticlesA, ProtoParticlesB])


def _hltparticleflow_inputs_0_0_1(CaloClustersA):
    return _hltparticleflow_inputs(inclust=[CaloClustersA])


## etc.  Unfortunately, a new input transform is needed for every unique
## combination of input types.  Not sure how to make an automated transform
## builder.


def _hltparticleflow_output(Output):  # {
    """Return a dict suitable for a HltParticleFlow output transform."""
    d = {'Output': Output, 'ExtraOutputs': [Output]}
    return d


# }

hpf_input_transforms = {
    (1, 0, 0): _hltparticleflow_inputs_1_0_0,
    (2, 0, 0): _hltparticleflow_inputs_2_0_0,
    (3, 0, 0): _hltparticleflow_inputs_3_0_0,
    (4, 0, 0): _hltparticleflow_inputs_4_0_0,
    (0, 1, 0): _hltparticleflow_inputs_0_1_0,
    (0, 2, 0): _hltparticleflow_inputs_0_2_0,
    (1, 1, 0): _hltparticleflow_inputs_1_1_0,
    (1, 2, 0): _hltparticleflow_inputs_1_2_0,
    (0, 0, 1): _hltparticleflow_inputs_0_0_1
}


def ParticleFlow(inParts=[], inProts=[], inClust=[], **kwargs):  # {
    """Return a particle flow algorithm that takes 'inputs' as inputs.

    Additional keyword arguments are forwarded to HltParticleFlow.

    Unfortunate replication of code from ParticleCombiner can be streamlined,
    but hopefully this will rapidly become obsolete and be removed.
    """
    nins = (len(inParts), len(inProts), len(inClust))

    # Need to dispatch to the right particleflow, based on the number of inputs
    assert hpf_input_transforms.has_key(
        nins), 'Do not have an input_transform for {} inputs'.format(nins)

    input_transform = hpf_input_transforms[nins]

    # Map each input container to an input property name
    inputs = ([('Particles' + letter, p)
               for p, letter in zip(inParts, string.ascii_uppercase)] + [
                   ('ProtoParticles' + letter, p)
                   for p, letter in zip(inProts, string.ascii_uppercase)
               ] + [('CaloClusters' + letter, p)
                    for p, letter in zip(inClust, string.ascii_uppercase)])
    # We need to merge dicts, we make sure we don't have overlapping keys (the
    # caller really shouldn't specify ParticleX keys anyway)
    assert set(dict(inputs)).intersection(kwargs) == set()
    kwargs = dict(inputs + list(kwargs.items()))

    particleflow = HltParticleFlow(
        input_transform=input_transform,
        output_transform=_hltparticleflow_output,
        outputs={'Output': force_location('HltParticleFlow/Particles')},
        **kwargs)
    return particleflow.Output


# }

###############################################################################
"""Define a wrapper for original HltJetBuilder algorithm.

Although HltJetBuilder inherits from DaVinciHistoAlgorithm, the wrappers
defined in ..algorithms (make_dvalgorithm, etc.) are insufficient for
configuring it.  It takes two distinguishable input lists: the usual Inputs
of the base class and an additional InputTags list.  A wrapper must use an
input transform tailored to this heterogenous input.
"""


def _hltjetbuilder_inputs(inparts=[], tags=[]):  # {
    """Return a dict suitable for a HltJetBuilder input transform

    The HltJetBuilder also has an 'ExtraInputs' list and a LoKi::FastJetMaker
    tool with an 'ExtraInputs' which are not supported here.
    """
    d = {'Inputs': inparts, 'InputTags': tags, 'ExtraInputs': inparts + tags}
    return d


# }


def _hltjetbuilder_inputs_1_1(InParticlesA, TagParticlesA):
    return _hltjetbuilder_inputs(inparts=[InParticlesA], tags=[TagParticlesA])


def _hltjetbuilder_inputs_1_2(InParticlesA, TagParticlesA, TagParticlesB):
    return _hltjetbuilder_inputs(
        inparts=[InParticlesA], tags=[TagParticlesA, TagParticlesB])


def _hltjetbuilder_outputs(Output):
    d = {'Output': Output, 'ExtraOutputs': [Output]}
    return d


hjb_input_transforms = {
    (1, 1): _hltjetbuilder_inputs_1_1,
    (1, 2): _hltjetbuilder_inputs_1_2
}


def JetBuilder(inputs=[], inputtags=[], **kwargs):  # {
    """Return a jet builder algorithm that takes inputs and inputtags as inputs.

    Additional keyword arguments are forwarded to HltParticleFlow.

    Unfortunate replication of code from ParticleCombiner can be streamlined,
    but hopefully this will rapidly become obsolete and be removed.
    """
    nins = (len(inputs), len(inputtags))

    # Need to dispatch to the right particleflow, based on the number of inputs
    assert hjb_input_transforms.has_key(
        nins), 'Do not have an input_transform  for {} inputs'.format(nins)

    input_transform = hjb_input_transforms[nins]

    # Map each input container to an input property name
    inputs = ([('InParticles' + letter, p)
               for p, letter in zip(inputs, string.ascii_uppercase)] +
              [('TagParticles' + letter, p)
               for p, letter in zip(inputtags, string.ascii_uppercase)])
    # We need to merge dicts, we make sure we don't have overlapping keys (the
    # caller really shouldn't specify ParticleX keys anyway)
    assert set(dict(inputs)).intersection(kwargs) == set()
    kwargs = dict(inputs + list(kwargs.items()))

    jetbuilder = HltJetBuilder(
        input_transform=input_transform,
        output_transform=_hltjetbuilder_outputs,
        WriteP2PVRelations=False,
        **kwargs)
    return jetbuilder.Output


# }
