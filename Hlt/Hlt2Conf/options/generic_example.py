###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Hlt2Conf.lines.generic import new_lines
from RecoConf.reco_objects_from_file import stateProvider_with_simplified_geom

# some other ldsts you might want to run over, to study phyics perforamnce for instance
# other_input_files = [
#     # MinBias 30000000
#     # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
#     # 'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
#     # 'hlt2_minbias.ldst',
#     # D*-tagged D0 to KK, 27163002
#     # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/27163002/LDST
#     # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000012_2.ldst'
#     # Bu -> Jpsi (mm) K
#     # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/12143001/LDST
#     # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070468/0000/00070468_00000034_2.ldst',
#     # 'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00070468/0000/00070468_00000016_2.ldst',
#     # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070468/0000/00070468_00000003_2.ldst',
# ]
# options.input_type = 'ROOT'
#
# options.simulation = True
# options.data_type = 'Upgrade'
# options.dddb_tag = 'dddb-20171126'
# options.conddb_tag = 'sim-20171127-vc-md100'

options.set_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.set_input_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3


def make_lines():
    return [builder() for builder in new_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
