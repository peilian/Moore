###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a test if a dst file contains particles for all fired trigger lines.

Runs over the output of the hlt2_all_lines test, manually running the unpackers
and DecReports decoder, and checks per event that each firing trigger line has
a non-zero number of Particle objects in the expected location.
"""
import inspect
import json
import os
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (ApplicationMgr, CondDB, LHCbApp,
                           UnpackParticlesAndVertices, HltDecReportsDecoder,
                           HltANNSvc)

ROOT_IN_TES = "/Event/HLT2"

LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

# import HltANNSvc configuration from json file
with open('hlt2_all_lines.annsvc.json', 'r') as inputfile:
    dict1 = json.load(inputfile)

# convert unicode strings in input file to normal strings
HltANNSvc(Hlt2SelectionID={str(k): v for k, v in dict1.items()})

ApplicationMgr(TopAlg=[
    UnpackParticlesAndVertices(InputStream=ROOT_IN_TES),
    HltDecReportsDecoder()
])

IOHelper('ROOT').inputFiles(['hlt2_all_lines.dst'], clear=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)
# Flag to record whether we saw any events
# The test can't check anything if no event fired
found_events = False
while TES['/Event']:
    print('Checking next event.')
    # get decReport
    decRep = TES['/Event/Hlt/DecReports'].decReports()
    for name, report in decRep.items():
        if report.decision():
            print('Checking line {}'.format(name))
            container = TES['/Event/HLT2/{}/Particles'.format(
                name[:-len("Decision")])]
            assert container, "ERROR: no container in trigger line {}".format(
                name)
            assert container.size(
            ) > 0, "ERROR: no particles in trigger line {}".format(name)
            found_events = True
    appMgr.run(1)

if not found_events:
    exit('no positive decisions found')
