###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define the HLT2 track reconstruction outputs for use by lines."""

from PyConf import configurable

from RecoConf.hlt1_tracking import (
    make_VPClus_hits,
    make_PrStoreUTHit_hits,
    make_PrStoreFTHit_hits,
    make_PrStoreSciFiHits_hits,
    get_global_ut_hits_tool,
    get_track_master_fitter,
    make_hlt1_tracks,
    make_hlt1_fitted_tracks,
)

from PyConf.Algorithms import (
    PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN, PrLongLivedTracking,
    TrackBestTrackCreator, TracksFTConverter, PrResidualSciFiHits,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
    MakeZipContainer__Track_v2)

from PyConf.Tools import UpgradeGhostId


@configurable
def make_PrForwardTrackingVelo_tracks(input_tracks,
                                      make_ft_hits=make_PrStoreSciFiHits_hits,
                                      ut_hits_tool=get_global_ut_hits_tool):
    """Makes forward tracks for HLT2 (long tracks from Velo seeds) with PrForwardTrackingVelo.

       Args:
           input_tracks (dict): velo tracks, needs ``'Pr'`` tracks, e.g. from  `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreSciFiHits_hits <RecoConf.hlt1_tracking.make_PrStoreSciFiHits_hits>`.
           ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

       Returns:
           DataHandle: PrForwardTrackingVelo's OutputName.

       Note:
           PrForwardTrackingVelo's defaults have been overridden in this maker with ``UseMomentumEstimate=False``.
    """
    return PrForwardTrackingVelo(
        InputName=input_tracks["Pr"],
        ForwardHitsLocation=make_ft_hits(),
        AddUTHitsToolName=ut_hits_tool()).OutputName

@configurable
def all_hlt2_forward_track_types(
        input_tracks, make_forward_tracks=make_PrForwardTrackingVelo_tracks):
    """Helper function to get all types of HLT2 forward tracks.

    Args:
        input_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
        make_forward_tracks (DataHandle): maker of forward tracks, defaults to `make_PrForwardTrackingVelo_tracks`.

    Returns:
        A dict mapping Pr, v1, v2 and v2Zip HLT2 forward tracks to ``'Pr'``, ``'v1'``, ``'v2'`` and ``'v2Zip'`` respectively.
    """
    forward_tracks_pr = make_forward_tracks(input_tracks=input_tracks)
    forward_tracks_v2 = TracksFTConverter(
        TracksUTLocation=input_tracks["v2"],
        TracksFTLocation=forward_tracks_pr).OutputTracksLocation
    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName
    forward_tracks_v2_zip = MakeZipContainer__Track_v2(
        Input=forward_tracks_v2).OutputSelection
    return {
        "Pr": forward_tracks_pr,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1,
        "v2Zip": forward_tracks_v2_zip,
    }

@configurable
def make_PrHybridSeeding_tracks(make_ft_hits=make_PrStoreFTHit_hits):
    """Makes seed tracks with PrHybridSeeding [1]_.

    Args:
        make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreFTHit_hits <RecoConf.hlt1_tracking.make_PrStoreFTHit_hits>`.

    Returns:
        A dict mapping v1 and v2 SciFi seeding tracks to ``'v1'`` and ``'v2'`` respectively.

    .. [1] https://cds.cern.ch/record/2027531/

    """
    scifi_tracks_v2 = PrHybridSeeding(FTHitsLocation=make_ft_hits()).OutputName
    scifi_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=scifi_tracks_v2).OutputTracksName

    return {"v2": scifi_tracks_v2, "v1": scifi_tracks_v1}


@configurable
def make_PrMatchNN_tracks(velo_tracks,
                          scifi_tracks,
                          ut_hits_tool=get_global_ut_hits_tool):
    """Makes long tracks from SciFi seed tracks, velo tracks and UT hits using PrMatchNN.

    Args:
        velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
        scifi_tracks (dict): SciFi seeding tracks, needs ``'v2'`` tracks, e.g. from `make_PrHybridSeeding_tracks`.
        ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

    Returns:
        A dict mapping v1 and v2 long tracks from track-matching to ``'v1'`` and ``'v2'`` respectively.
    """

    match_tracks_v2 = PrMatchNN(
        VeloInput=velo_tracks["v2"],
        SeedInput=scifi_tracks["v2"],
        AddUTHitsToolName=ut_hits_tool()).MatchOutput

    match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=match_tracks_v2).OutputTracksName

    return {"v2": match_tracks_v2, "v1": match_tracks_v1}


@configurable
def make_PrLongLivedTracking_tracks(scifi_tracks,
                                    make_ut_hits=make_PrStoreUTHit_hits):
    """Makes downstream tracks from SciFi seed tracks and UT hits using PrLongLivedTracking.

    Args:
        scifi_tracks (dict): SciFi seeding tracks, needs ``'v2'`` tracks, e.g. from `make_PrHybridSeeding_tracks`.
        ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

    Returns:
        A dict mapping v1 and v2 downstream tracks to ``'v1'`` and ``'v2'`` respectively.
    """
    downstream_tracks_v2 = PrLongLivedTracking(
        InputLocation=scifi_tracks["v2"], UTHits=make_ut_hits()).OutputLocation

    downstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=downstream_tracks_v2).OutputTracksName

    return {"v2": downstream_tracks_v2, "v1": downstream_tracks_v1}


@configurable
def get_UpgradeGhostId_tool(velo_hits=make_VPClus_hits,
                            ut_hits=make_PrStoreUTHit_hits):
    """Returns instance of UpgradeGhostId given VP and UT hits.

    Args:
        velo_hits (DataHandle): maker of velo hits, defaults to `make_VPClus_hits <RecoConf.hlt1_tracking.make_VPClus_hits>`.
        ut_hits (DataHandle): maker of UT hits, defaults to `make_PrStoreUTHit_hits <RecoConf.hlt1_tracking.make_PrStoreUTHit_hits>`.

    Returns:
        Instance of UpgradeGhostId
    """
    return UpgradeGhostId(
        VPClusterLocation=velo_hits(), UTClusterLocation=ut_hits())


def make_ReduceSciFiHits_fromMatch():
    """
    Funtion to remove SciFi hits used by PrMatchNN algorithm ans create a new SciFiHits container for the residual SciFi hits, which are later used as input to `PrForwardTrackingVelo`
    """
    hlt1_tracks = make_hlt1_tracks()
    scifi_tracks = make_PrHybridSeeding_tracks()
    match_tracks = make_PrMatchNN_tracks(hlt1_tracks["Velo"], scifi_tracks)
    return PrResidualSciFiHits(
        TracksLocation=match_tracks["v1"],
        SciFiHitsLocation=make_PrStoreSciFiHits_hits()).SciFiHitsOutput

@configurable
def make_ForwardTrack_ReduceSciFiHits(input_tracks):
    
    return PrForwardTrackingVelo(
        InputName=input_tracks["Pr"],
        ForwardHitsLocation=make_ReduceSciFiHits_fromMatch(),
        AddUTHitsToolName=get_global_ut_hits_tool()).OutputName


def get_default_hlt2_tracks():
    """Function to get default set of tracks reconstructed in HLT2, which are later used as input to `make_TrackBestTrackCreator_tracks`.

    Returns:
        A dict mapping all types of velo, upstream, HLT1 forward fitted, HLT2 forward, SciFi seeding, downstream and matched long tracks to ``'Velo'``, ``'Upstream'``, ``'ForwardFastFitted'``, ``'Forward'``, ``'Seed'``, ``'Downstream'`` and ``'Match'``` respectively.
    """
    hlt1_tracks = make_hlt1_tracks()
    fitted_hlt1_tracks = make_hlt1_fitted_tracks(hlt1_tracks)
    #hlt2_forward_tracks = all_hlt2_forward_track_types(hlt1_tracks["Velo"])
    scifi_tracks = make_PrHybridSeeding_tracks()
    downstream_tracks = make_PrLongLivedTracking_tracks(scifi_tracks)
    match_tracks = make_PrMatchNN_tracks(hlt1_tracks["Velo"], scifi_tracks)
    hlt2_forward_tracks = all_hlt2_forward_track_types(hlt1_tracks["Velo"], make_forward_tracks=make_ForwardTrack_ReduceSciFiHits)
    return {
        "Velo": hlt1_tracks["Velo"],
        "Upstream": hlt1_tracks["Upstream"],
        "ForwardFastFitted": fitted_hlt1_tracks,
        "Forward": hlt2_forward_tracks,
        "Seed": scifi_tracks,
        "Downstream": downstream_tracks,
        "Match": match_tracks
    }


def get_default_track_list_for_TrackBestTrackCreator():
    """Function to set the default order of tracks which are used as input to `make_TrackBestTrackCreator_tracks`.

    Returns:
        A list of strings.
    """
    #TODO: the list has a random order, pls fix
    return [
        "Velo", "Forward", "ForwardFastFitted", "Upstream", "Downstream",
        "Match", "Seed"
    ]


@configurable
def make_TrackBestTrackCreator_tracks(
        tracks,
        track_version,
        get_tracklist=get_default_track_list_for_TrackBestTrackCreator,
        get_fitter_tool=get_track_master_fitter,
        get_ghost_tool=get_UpgradeGhostId_tool,
        do_not_refit=False):
    """Persists best quality tracks, calls track fitters, kills clones and adds neural-net response for fake-track (a.k.a. ghost) rejection.

    Args:
        tracks (dict): reconstructed tracks, e.g. from `get_default_hlt2_tracks`.
        track_version (string): track version of input and output tracks.
        get_tracklist (list of strings): sets the list of `tracks` which is used as input to TrackBestTrackCreator's ``TracksInContainers``. Be aware that changing the order of tracks in that list has an impact on the output and performance of TrackBestTrackCreator. Defaults to `get_default_track_list_for_TrackBestTrackCreator`
        get_fitter_tool: track fitting tool, defaults to `get_track_master_fitter <RecoConf.hlt1_tracking.get_track_master_fitter>`.
        get_ghost_tool: ghostId tool, adding a neural-net response that has been trained to reject fake (a.k.a. ghost) tracks.
        do_not_refit (bool): Wheter or not to not refit input tracks.

    Returns:
        DataHandle: Best tracks

    Note:
        TrackBestTrackCreator's defaults have been overridden in this maker with ``InitTrackStates=False, DoNotRefit=do_not_refit, FitTracks=True``
    """
    track_list = [
        tracks[track_type][track_version] for track_type in get_tracklist()
    ]
    return TrackBestTrackCreator(
        TracksInContainers=track_list,
        Fitter=get_fitter_tool(),
        GhostIdTool=get_ghost_tool(),
        DoNotRefit=do_not_refit,
        AddGhostProb=True,
        FitTracks=True).TracksOutContainer


def make_hlt2_tracks():
    """Function to get all types of tracks reconstructed in HLT2

    Returns:
        A dict mapping all types of velo, upstream, HLT1 forward fitted, HLT2 forward, SciFi seeding, downstream, matched long and best tracks to ``'Velo'``, ``'Upstream'``, ``'ForwardFastFitted'``, ``'Forward'``, ``'Seed'``, ``'Downstream'``, ``'Match'`` and ``'Best'`` respectively.
    """
    track_dict = get_default_hlt2_tracks()
    track_version = "v1"
    best_track_container = make_TrackBestTrackCreator_tracks(
        track_dict, track_version)
    track_dict["Best"] = {track_version: best_track_container}
    return track_dict
