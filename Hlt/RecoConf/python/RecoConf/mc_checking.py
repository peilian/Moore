###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
'''
In this file, algorithms needed to run the MC reconstruction checking (PrTrackChecker) are defined.
'''
from PyConf.tonic import (configurable)
from PyConf.dataflow import DataHandle

from PyConf.components import Tool
from PyConf.application import default_raw_event
from PyConf.application import make_data_with_FetchDataFromFile

from PyConf.Algorithms import (
    VPFullCluster2MCParticleLinker, PrLHCbID2MCParticle,
    PrLHCbID2MCParticleVPUTFTMU, PrTrackAssociator, PrTrackChecker,
    PrUTHitChecker, TrackListRefiner, TrackResChecker,
    TrackIPResolutionCheckerNT, DataPacking__Unpack_LHCb__MCVPHitPacker_)
from PyConf.Algorithms import LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertices as FromVectorLHCbRecVertex

from PyConf.Tools import LoKi__Hybrid__MCTool
from PyConf.Tools import LoKi__Hybrid__TrackSelector as LoKiTrackSelector

from RecoConf.hlt1_tracking import (make_PrStoreUTHit_hits,
                                    make_FTRawBankDecoder_clusters,
                                    make_velo_full_clusters)

from Hlt2Conf.data_from_file import mc_unpackers

from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask, categories

from RecoConf.hlt1_muonid import make_muon_hits
from PyConf.Tools import VisPrimVertTool


def get_item(x, key):
    """Return `key` from `x` if `x` is a dict, otherwise return `x`.

    TODO: This helper function can be replaces once Moore!63 has been addressed.
    """
    if isinstance(x, DataHandle): return x
    return x[key]


@configurable
def make_links_veloclusters_mcparticles():
    return VPFullCluster2MCParticleLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCParticleLinksLocation=make_data_with_FetchDataFromFile(
            "/Event/Link/Raw/VP/Digits")).OutputLocation


@configurable
def make_links_lhcbids_mcparticles_tracking_system():
    return PrLHCbID2MCParticle(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/UT/Clusters'),
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/FT/LiteClusters')).TargetName


@configurable
def make_links_lhcbids_mcparticles_tracking_and_muon_system():
    """Match lhcbIDs of hits in the tracking stations and muon stations to MCParticles.
    """
    return PrLHCbID2MCParticleVPUTFTMU(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/UT/Clusters'),
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/FT/LiteClusters'),
        MuonHitsLocation=make_muon_hits(),
        MuonHitsLinkLocation=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/Muon/Digits')).TargetName


@configurable
def make_links_tracks_mcparticles(InputTracks, LinksToLHCbIDs):
    return PrTrackAssociator(
        SingleContainer=get_item(InputTracks, "v1"),
        LinkerLocationID=LinksToLHCbIDs,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation


@configurable
def monitor_tracking_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        HitTypesToCheck,
        WriteHistos=1,
):
    """ Setup tracking efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType,
        HitTypesToCheck=HitTypesToCheck,
        MyCuts=MCCategories,
        WriteHistos=WriteHistos,
    )
    return PrTrackChecker(
        name=TrackType + "TrackChecker",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def monitor_uthit_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        WriteHistos=1,
):
    """ Setup UT hit efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType + "UTHits",
        WriteHistos=WriteHistos,
        MyCuts=MCCategories,
    )

    return PrUTHitChecker(
        name=TrackType + "UTHitsChecker",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def get_track_checkers(
        types_and_locations,
        uthit_efficiency_types=["Forward", "Downstream", "Match"],
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):
    """ Setup track and UT hit efficiency checkers

    The track type has to correspond to a key mc_checking_categories.categories. This defines which MC particle categories are tested.

    Args:
        types_and_locations (dict): Types and locations of track containers.
        uthit_efficiency_types (list of str): Types for which dedicated UT hit efficiency checker is setup.
        make_links_lhcbids_mcparticles (function): Maker that returns ???
    """

    assert isinstance(
        types_and_locations,
        dict), "Please provide a dictionary of track type and tracks"

    efficiency_checkers = []
    links_to_lhcbids = make_links_lhcbids_mcparticles()
    for track_type, tracks in types_and_locations.items():
        assert track_type in categories, track_type + " unknown. Please chose from " + ", ".join(
            categories)

        links_to_tracks = make_links_tracks_mcparticles(
            InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)

        pr_checker = monitor_tracking_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type),
            HitTypesToCheck=get_hit_type_mask(track_type),
        )
        efficiency_checkers.append(pr_checker)

        if not track_type in uthit_efficiency_types:
            continue
        uthit_checker = monitor_uthit_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type + "UTHits"),
        )
        efficiency_checkers.append(uthit_checker)
    return efficiency_checkers


@configurable
def get_best_tracks_checkers(
        BestTracks,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):

    links_to_lhcbids = make_links_lhcbids_mcparticles()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=BestTracks, LinksToLHCbIDs=links_to_lhcbids)

    long_tracks = make_track_filter(
        InputTracks=BestTracks, code="(~TrINVALID) & TrLONG ")
    downstream_tracks = make_track_filter(
        InputTracks=BestTracks, code="(~TrINVALID) & TrDOWNSTREAM ")
    good_long_tracks = make_track_filter(
        InputTracks=BestTracks,
        code="(~TrINVALID) & TrLONG & (TrGHOSTPROB < 0.5) ")
    good_downstream_tracks = make_track_filter(
        InputTracks=BestTracks,
        code="(~TrINVALID) & TrDOWNSTREAM & (TrGHOSTPROB < 0.5)")

    efficiency_checkers = []
    for tracks, tr_key, mc_key, hit_key in [
        (BestTracks, "Best", "Best", "Best"),
        (long_tracks, "BestLong", "BestLong", "BestLong"),
        (good_long_tracks, "LongGhostFiltered", "BestLong", "BestLong"),
        (downstream_tracks, "BestDownstream", "BestDownstream",
         "BestDownstream"),
        (good_downstream_tracks, "DownstreamGhostFiltered", "BestDownstream",
         "BestDownstream"),
    ]:
        checker = monitor_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)
    return efficiency_checkers


def make_track_filter(InputTracks, code):
    selector = LoKiTrackSelector(Code=code, StatPrint=True)
    filtered_tracks = TrackListRefiner(
        inputLocation=InputTracks["v1"], Selector=selector).outputLocation
    return filtered_tracks


def monitor_track_resolution(InputTracks):
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks["v1"], LinksToLHCbIDs=links_to_lhcbids)
    mcpart = mc_unpackers()["MCParticles"]
    mchead = make_data_with_FetchDataFromFile('/Event/MC/Header')
    res_checker = TrackResChecker(
        TracksInContainer=InputTracks["v1"],
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_tracks,
        VisPrimVertTool=VisPrimVertTool(MCHeader=mchead, MCParticles=mcpart))

    return res_checker


def monitor_IPresolution(InputTracks, InputPVs, VeloTracks):

    vertexConverter = FromVectorLHCbRecVertex(
        InputVerticesName=InputPVs,
        InputTracksName=VeloTracks).OutputVerticesName
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks, LinksToLHCbIDs=links_to_lhcbids)
    IPres_checker = TrackIPResolutionCheckerNT(
        TrackContainer=InputTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCHeaderLocation=make_data_with_FetchDataFromFile("/Event/MC/Header"),
        LinkerLocation=links_to_tracks,
        PVContainer=vertexConverter,
        NTupleLUN="FILE1")
    return IPres_checker
