###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable

from PyConf.Algorithms import (
    MuonMatchVeloUTSoA,
    TracksVPConverter,
    TracksUTConverter,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
    PrFilterIPSoA,
    SciFiTrackForwarding,
    PrVeloUT,
)

from hlt1_tracking import (
    make_VeloClusterTrackingSIMD_hits,
    all_velo_track_types,
    make_pvs,
    all_upstream_track_types,
    all_hlt1_forward_track_types,
)

from hlt1_muonid import (
    make_muon_hits, )


@configurable
def make_IPselected_tracks(tracks,
                           pvs,
                           ip_cut,
                           make_velo_hits=make_VeloClusterTrackingSIMD_hits):
    output_tracks = PrFilterIPSoA(
        IPcut=ip_cut, Input=tracks["Pr"], InputVertices=pvs).Output
    output_tracks_v2 = TracksVPConverter(
        TracksLocation=output_tracks,
        HitsLocation=make_velo_hits()).OutputTracksLocation
    output_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=output_tracks_v2).OutputTracksName

    return {
        "Pr": output_tracks,
        "v2": output_tracks_v2,
        "v1": output_tracks_v1
    }


@configurable
def make_muon_match_tracks(velo_tracks,
                           upstream_tracks,
                           make_muon_hits=make_muon_hits):
    muon_match_tracks = MuonMatchVeloUTSoA(
        InputTracks=upstream_tracks["Pr"],
        InputMuonHits=make_muon_hits()).OutputTracks
    muon_match_tracks_v2 = TracksUTConverter(
        TracksVPLocation=velo_tracks["v2"],
        TracksUTLocation=muon_match_tracks).OutputTracksLocation

    muon_match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=muon_match_tracks_v2).OutputTracksName

    return {
        "Pr": muon_match_tracks,
        "v2": muon_match_tracks_v2,
        "v1": muon_match_tracks_v1
    }


def make_tracks_with_muonmatch():
    velo_tracks = all_velo_track_types()
    velo_ut_tracks = all_upstream_track_types(velo_tracks)
    muon_match_tracks = make_muon_match_tracks(velo_tracks, velo_ut_tracks)
    forward_tracks = all_hlt1_forward_track_types(muon_match_tracks)
    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "MuonMatch": muon_match_tracks,
        "Forward": forward_tracks,
    }


def make_tracks_with_muonmatch_ipcut(velo_track_min_ip, tracking_min_pt):
    velo_tracks = all_velo_track_types()
    pvs = make_pvs()
    ipselected_tracks = make_IPselected_tracks(velo_tracks, pvs,
                                               velo_track_min_ip)

    with PrVeloUT.bind(minPT=tracking_min_pt, minPTFinal=tracking_min_pt):
        velo_ut_tracks = all_upstream_track_types(ipselected_tracks)
    muon_match_tracks = make_muon_match_tracks(ipselected_tracks,
                                               velo_ut_tracks)

    with SciFiTrackForwarding.bind(MinPt=tracking_min_pt, PreSelectionPT=0.):
        forward_tracks = all_hlt1_forward_track_types(muon_match_tracks)

    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "MuonMatch": muon_match_tracks,
        "Forward": forward_tracks,
    }
