###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.tonic import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from .hlt1_tracking import (
    require_gec, require_pvs, make_reco_pvs, make_pvs, make_hlt1_tracks,
    make_VeloKalman_fitted_tracks, all_velo_track_types,
    make_PrForwardTracking_tracks, make_PatPV3DFuture_pvs,
    all_hlt1_forward_track_types, make_TrackEventFitter_fitted_tracks,
    make_hlt1_fitted_tracks)
from RecoConf.hlt1_muonmatch import make_tracks_with_muonmatch_ipcut
from .hlt1_muonid import make_muon_id, make_tracks_with_muon_id
from .hlt2_tracking import make_hlt2_tracks, make_TrackBestTrackCreator_tracks
from .calorimeter_reconstruction import make_calo
from .mc_checking import get_track_checkers, get_best_tracks_checkers
from .reco_objects_from_file import reconstruction
from PyConf.application import default_raw_event
from .rich_reconstruction import make_rich_pids, make_rich_pixels, default_rich_reco_options
from .rich_data_monitoring import (make_rich_pixel_monitors,
                                   make_rich_track_monitors,
                                   default_rich_monitoring_options)
from .rich_mc_checking import make_rich_checkers, default_rich_checking_options
from .calo_data_monitoring import add_clusters_moni
from PyConf.Algorithms import Rich__Future__Rec__TrackFilter as TrackFilter

from GaudiKernel.SystemOfUnits import MeV, mm
from Moore.config import Reconstruction


def reco_prefilters():
    return [require_gec()]


@configurable
def standalone_hlt1_reco(do_mc_checking=False):
    """ Run the Hlt1 reconstruction, i.e. tracking and muon id, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
        Returns:
            Reconstruction: Data and control flow of Hlt1 reconstruction.

    """
    hlt1_tracks = make_hlt1_tracks()
    pvs = make_pvs()
    fitted_tracks = make_VeloKalman_fitted_tracks(hlt1_tracks)
    muon_ids = make_muon_id(hlt1_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_tracks, muon_ids)

    data = [pvs, fitted_tracks["Pr"], tracks_with_muon_id]

    if do_mc_checking:
        types_and_locations_for_checkers = {
            "Velo": hlt1_tracks["Velo"],
            "Upstream": hlt1_tracks["Upstream"],
            "Forward": hlt1_tracks["Forward"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    return Reconstruction('hlt1_reco', data, reco_prefilters())


def standalone_hlt1_reco_velo_only():
    """ Run the default Hlt1 Velo reconstruction
        Returns:
            Reconstruction: Data and control flow of Hlt1 reconstruction.

    """
    return Reconstruction('hlt1_velo_reco', [make_hlt1_tracks()["Velo"]["Pr"]],
                          reco_prefilters())


@configurable
def standalone_hlt1_muonmatching_reco(do_mc_checking=False,
                                      velo_track_min_ip=0.4 * mm,
                                      tracking_min_pt=80. * MeV):
    all_tracks = make_tracks_with_muonmatch_ipcut(
        velo_track_min_ip=velo_track_min_ip, tracking_min_pt=tracking_min_pt)
    fitted_tracks = make_hlt1_fitted_tracks(all_tracks)
    muon_ids = make_muon_id(all_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_tracks, muon_ids)
    data = [fitted_tracks["Pr"], tracks_with_muon_id]
    if do_mc_checking:
        types_and_locations_for_checkers = {
            "Velo": all_tracks["Velo"],
            "Upstream": all_tracks["Upstream"],
            "Forward": all_tracks["Forward"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    return Reconstruction('hlt1_muonmatching_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_full_track_reco(do_mc_checking=False):
    """ Run the Hlt2 track reconstruction, i.e. pattern recognition and track fit, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
        Returns:
            Reconstruction: Data and control flow of Hlt2 track reconstruction.

    """
    hlt2_tracks = make_hlt2_tracks()
    best_tracks = hlt2_tracks["Best"]
    pvs = make_pvs()
    data = [best_tracks["v1"], pvs]
    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)
        data += get_best_tracks_checkers(best_tracks)

    return Reconstruction('hlt2_track_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_reco(do_mc_checking=False, do_data_monitoring=False):
    hlt2_tracks = make_hlt2_tracks()
    best_tracks = hlt2_tracks["Best"]
    pvs = make_pvs()
    data = [best_tracks["v1"], pvs]
    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)
        data += get_best_tracks_checkers(best_tracks)
    calo = make_calo(best_tracks["v1"])
    data += [
        calo["ecalSplitClusters"], calo["ecalPIDmu"], calo["hcalPIDe"],
        calo["hcalPIDmu"], calo["ecalPIDe"], calo["bremPIDe"], calo["clusChi2"]
    ]
    # Add RICH
    data += add_hlt2_rich(
        best_tracks=best_tracks,
        do_mc_checking=do_mc_checking,
        do_data_monitoring=do_data_monitoring)

    # Add monitoring
    if do_data_monitoring:
        data += add_clusters_moni(calo)

    return Reconstruction('hlt2_reco', data, reco_prefilters())


def standalone_hlt2_reco_calo_from_dst(do_mc_checking=False):

    data = []

    # read tracks from input file
    best_tracks = reconstruction()["Tracks"]

    calo = make_calo(best_tracks)

    data += [
        calo["ecalSplitClusters"], calo["ecalPIDmu"], calo["hcalPIDe"],
        calo["hcalPIDmu"], calo["ecalPIDe"], calo["bremPIDe"], calo["clusChi2"]
    ]

    return Reconstruction('hlt2_standalone_calo_reco', data, reco_prefilters())


def add_hlt2_rich(best_tracks, do_mc_checking=False, do_data_monitoring=False):
    """
    Returns the nodes to add for the RICH HLT2 reco
    """

    nodes = []

    # First, extract seperate selections for the Long, Downstream and Upstream tracks
    # Eventually this should not be done, as the best container should go and
    # the tracks should be provided from source in seperate containers.
    richTkFilt = TrackFilter(InTracksLocation=best_tracks["v1"])

    # Create the confs for each RICH reco for Long, Upstream and Downstream tracks
    reco_opts = default_rich_reco_options()  # default configuration options
    richRecConfs = {
        "Long":
        make_rich_pids(
            track_name="Long",
            input_tracks=richTkFilt.OutLongTracksLocation,
            options=reco_opts),
        "Downstream":
        make_rich_pids(
            track_name="Downstream",
            input_tracks=richTkFilt.OutDownTracksLocation,
            options=reco_opts),
        "Upstream":
        make_rich_pids(
            track_name="Upstream",
            input_tracks=richTkFilt.OutUpTracksLocation,
            options=reco_opts)
    }
    # Add the RichPIDs to required nodes
    nodes += [richRecConfs[key]["RichPIDs"] for key in richRecConfs.keys()]

    # RICH data monitoring
    if do_data_monitoring:
        moni_nodes = []
        # data monitoring options
        moni_opts = default_rich_monitoring_options()
        # Rich pixel level data monitoring
        richPixelConf = make_rich_pixels(options=reco_opts)
        richPixelMonitors = make_rich_pixel_monitors(
            conf=richPixelConf, reco_opts=reco_opts, moni_opts=moni_opts)
        moni_nodes += [
            richPixelMonitors[key] for key in richPixelMonitors.keys()
        ]
        # Rich track level data monitoring
        for key, conf in richRecConfs.iteritems():
            riTkMoni = make_rich_track_monitors(
                conf=conf, reco_opts=reco_opts, moni_opts=moni_opts)
            moni_nodes += [riTkMoni[key] for key in riTkMoni.keys()]
        nodes += [
            CompositeNode(
                name='RICH_data_monitoring',
                children=moni_nodes,
                combineLogic=NodeLogic.NONLAZY_OR,
                forceOrder=False)
        ]

    # RICH MC checking
    if do_mc_checking:
        mc_nodes = []
        # Default MC checking options
        check_opts = default_rich_checking_options()
        for key, conf in richRecConfs.iteritems():
            # MC checkers require original 'v1' container before filtering
            # To Be removed...
            conf["OriginalV1Tracks"] = best_tracks["v1"]
            riMCcheck = make_rich_checkers(
                conf=conf, reco_opts=reco_opts, check_opts=check_opts)
            mc_nodes += [riMCcheck[key] for key in riMCcheck.keys()]
        nodes += [
            CompositeNode(
                name='RICH_mc_checking',
                children=mc_nodes,
                combineLogic=NodeLogic.NONLAZY_OR,
                forceOrder=False)
        ]

    return nodes


@configurable
def standalone_hlt2_reco_brunel(do_mc_checking=False,
                                do_data_monitoring=False):
    """ Run the Hlt2 track reconstruction as it was defined in Brunel, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
            do_data_monitoring (bool): Enable data monitoring.
        Returns:
            Reconstruction: Data and control flow of Hlt2 track reconstruction.

    """

    def get_Brunel_track_list_for_TrackBestTrackCreator():
        return [
            "Velo", "ForwardFastFitted", "Forward", "Upstream", "Downstream",
            "Match", "Seed"
        ]

    # we modify the velo tracks consistently for the pvs and the best tracks
    with all_hlt1_forward_track_types.bind(make_forward_tracks=make_PrForwardTracking_tracks),\
         make_hlt1_fitted_tracks.bind(make_forward_fitted_tracks=make_TrackEventFitter_fitted_tracks),\
         make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
         require_gec.bind(cut=0),\
         make_TrackBestTrackCreator_tracks.bind(get_tracklist=get_Brunel_track_list_for_TrackBestTrackCreator,do_not_refit=True):
        return standalone_hlt2_reco(
            do_mc_checking=do_mc_checking,
            do_data_monitoring=do_data_monitoring)
