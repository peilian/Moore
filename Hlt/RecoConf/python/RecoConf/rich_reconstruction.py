###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable

from PyConf.application import default_raw_event, make_odin

from PyConf.Algorithms import (Rich__Future__RawBankDecoder as RichDecoder,
                               Rich__Future__SmartIDClustering as
                               RichClustering)

from PyConf.Algorithms import (
    Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels,
    Rich__Future__Rec__DetailedTrSegMakerFromTracks as SegmentCreator,
    Rich__Future__Rec__RayTraceTrackGlobalPoints as TkPanelGlobalPoints,
    Rich__Future__Rec__RayTraceTrackLocalPoints as TkPanelLocalPoints,
    Rich__Future__Rec__EmittedPhotonYields as EmittedYields,
    Rich__Future__Rec__TrackEmittedCherenkovAngles as EmittedCKAngles,
    Rich__Future__Rec__RayTraceCherenkovCones as EmittedMassCones,
    Rich__Future__Rec__DetectablePhotonYields as DetectableYields,
    Rich__Future__Rec__GeomEffCKMassRings as GeomEff,
    Rich__Future__Rec__SelectTrackSegments as SelectTrackSegments,
    Rich__Future__Rec__SignalPhotonYields as SignalYields,
    Rich__Future__Rec__TrackSignalCherenkovAngles as SignalCherenkovAngles,
    Rich__Future__Rec__TrackFunctionalCherenkovResolutions as TrackCKRes,
    Rich__Future__Rec__SIMDQuarticPhotonReco as PhotonReco,
    Rich__Future__Rec__SIMDPhotonPredictedPixelSignal as PhotonPredSignal,
    Rich__Future__Rec__SIMDRecoSummary as RecSummary,
    Rich__Future__Rec__GlobalPID__InitialisePIDInfo as GPIDInit,
    Rich__Future__Rec__GlobalPID__WriteRichPIDs as WriteRichPIDs,
    Rich__Future__Rec__SIMDPixelBackgroundsEstiAvHPD as PixelBackgrounds,
    Rich__Future__Rec__GlobalPID__SIMDLikelihoodMinimiser as LkMinimiser)

from Configurables import Rich__Future__ParticleProperties as PartProps
#from PyConf.Tools import Rich__Future__ParticleProperties as PartProps

from PyConf.Tools import TrackSTEPExtrapolator

###############################################################################


def default_rich_reco_options():
    """
    Returns a dict of the default RICH reconstruction options
    """

    # General options. Those most likely users will want to tweak..
    opts = {
        # The mass hypotheses to consider
        "Particles": [
            "electron", "muon", "pion", "kaon", "proton", "deuteron",
            "belowThreshold"
        ],
        # The RICH radiators to use
        "Radiators": ["Rich1Gas", "Rich2Gas"],
        # Tay tracing ring points
        "NRayTracingRingPoints": (96, 96, 96),
        # Maximum number of clusters GEC cut
        "MaxPixelClusters":
        200000,
        # Maximum number of tracks GEC cut
        "MaxTracks":
        10000,
        # PID Version
        "PIDVersion":
        2
    }

    # More technical options
    opts.update({

        #===========================================================
        # Pixel treatment options
        #===========================================================

        # Should pixel clustering be run, for either ( RICH1, RICH2 )
        "ApplyPixelClustering": (False, False),

        #===========================================================
        # Track treatment options
        #===========================================================

        # Track Extrapolator type
        "TrackExtrapolator":
        TrackSTEPExtrapolator,

        #===========================================================
        # Settings for the global PID minimisation
        #===========================================================

        # Number of iterations of the global PID background and
        # likelihood minimisation
        "nLikelihoodIterations":
        2,

        # The following are technical settings per iteration.
        # Do not change unless you know what you are doing ;)
        # Array size must be at least as big as the number of iterations

        # Pixel background options

        # Ignore the expected signals based on the track information
        "PDBackIgnoreExpSignals": [True, False, False, False],

        # Minimum allowed pixel background value (RICH1,RICH2)
        "PDBackMinPixBackground": [(0, 0), (0, 0), (0, 0), (0, 0)],

        # Maximum allowed pixel background value (RICH1,RICH2)
        "PDBackMaxPixBackground": [(999, 999), (999, 999), (999, 999),
                                   (999, 999)],

        # Group Size for PDs  RICH1 RICH2
        "PDGroupSize": (4, 4),

        # Likelihood minimizer options

        # Freeze out DLL values
        "TrackFreezeOutDLL": [2, 4, 5, 6],

        # Run a final check on the DLL values
        "FinalDLLCheck": [False, True, True, True],

        # Force change DLL values
        "TrackForceChangeDLL": [-1, -2, -3, -4],

        # likelihood threshold
        "LikelihoodThreshold": [-1e-2, -1e-3, -1e-4, -1e-5],

        # Maximum tracks that can change per iteration
        "MaxTrackChangesPerIt": [5, 5, 4, 3],

        # The minimum DLL signal value
        "MinSignalForNoLLCalc": [1e-3, 1e-3, 1e-3, 1e-3]
    })

    return opts


###############################################################################


def get_radiator_bool_opts(options):
    # Radiators ( Aerogel not supported here )
    return (
        False,  #
        "Rich1Gas" in options["Radiators"],
        "Rich2Gas" in options["Radiators"])


###############################################################################


def get_detector_bool_opts(options):
    # Detectors ( Aerogel not supported here )
    return ("Rich1Gas" in options["Radiators"],
            "Rich2Gas" in options["Radiators"])


###############################################################################


def configure_rich_particle_properties(options, GroupName=""):
    # Old style from Run II conf
    ppToolName = "RichPartProp" + GroupName
    tool = PartProps(
        name="ToolSvc." + ppToolName, ParticleTypes=options["Particles"])
    return tool
    # Need to work on getting this working ...
    #return PartProps(public=True, ParticleTypes=options["Particles"])


###############################################################################


@configurable
def make_rich_pixels(options, make_raw=default_raw_event):
    """
    Return pixel specific RICH data.

    Args:
        options (dict): The processing options to use
        make_raw      : The LHCb RawEvent data object

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    part_props = configure_rich_particle_properties(options)

    # The conf dict to return
    results = {}

    # Get the raw event
    rawEvent = make_raw()

    # Get the ODIN object
    odin = make_odin()

    # Get the detector options
    det_opts = get_detector_bool_opts(options)

    # document raw event and Odin used
    results["RawEvent"] = rawEvent
    results["ODIN"] = odin

    # Decode the Raw event to RichSmartIDs
    richDecode = RichDecoder(
        name="RichRawDecoder",
        OdinLocation=odin,
        RawEventLocation=rawEvent,
        Detectors=det_opts)
    results["RichDecodedData"] = richDecode.DecodedDataLocation

    # Run clustering
    doClus = options["ApplyPixelClustering"]
    richClus = RichClustering(
        name="RichPixelClustering",
        ApplyPixelClustering=doClus,
        MaxClusters=options["MaxPixelClusters"],
        DecodedDataLocation=richDecode.DecodedDataLocation,
        Detectors=det_opts)
    results["RichClusters"] = richClus.RichPixelClustersLocation

    # Make the SIMD pixel objects
    simdPixels = RichSIMDPixels(
        name="RichSIMDPixels",
        NoClustering=(not doClus[0] and not doClus[1]),
        RichPixelClustersLocation=richClus.RichPixelClustersLocation)
    results["RichSIMDPixels"] = simdPixels.RichSIMDPixelSummariesLocation

    return results


###############################################################################


@configurable
def make_rich_tracks(track_name, input_tracks, options):
    """
    Return tracking specific RICH data from input tracks.

    Args:
        track_name    (str):  The track name to assign to this configuration
        input_tracks (dict): Input track objects to process.
        n_ring_point (3-tuple of int): Number points for mass hypothesis ring ray tracing (Aero,R1Gas,R2Gas).
        options       (dict): The processing options to use

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    part_props = configure_rich_particle_properties(options)

    # The conf map to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # Keep tabs on the input tracks used for this conf.
    results["InputTracks"] = input_tracks

    # The radiator options
    rad_opts = get_radiator_bool_opts(options)

    # Create radiator segments from input tracks
    segments = SegmentCreator(
        name="RichTrackSegments" + track_name,
        TracksLocation=input_tracks,
        MaxTracks=options["MaxTracks"],
        TrackExtrapolator=options["TrackExtrapolator"](),
        Radiators=rad_opts)
    results["TrackSegments"] = segments.TrackSegmentsLocation
    results["InitialTrackToSegments"] = segments.TrackToSegmentsLocation
    results["SegmentToTracks"] = segments.SegmentToTrackLocation

    # Intersections of segments with PD panels
    tkGloPnts = TkPanelGlobalPoints(
        name="RichTrackGloPoints" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation)
    tkLocPnts = TkPanelLocalPoints(
        name="RichTrackLocPoints" + track_name,
        TrackGlobalPointsLocation=tkGloPnts.TrackGlobalPointsLocation)
    results["TrackGlobalPoints"] = tkGloPnts.TrackGlobalPointsLocation
    results["TrackLocalPoints"] = tkLocPnts.TrackLocalPointsLocation

    # Emitted photon yields
    emitY = EmittedYields(
        name="RichEmittedYields" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation)
    results["EmittedYields"] = emitY.EmittedPhotonYieldLocation
    results["EmittedSpectra"] = emitY.EmittedPhotonSpectraLocation

    # Expected CK angles using emitted photon spectra
    emitChAngles = EmittedCKAngles(
        name="RichEmittedCKAngles" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        EmittedPhotonYieldLocation=emitY.EmittedPhotonYieldLocation,
        EmittedPhotonSpectraLocation=emitY.EmittedPhotonSpectraLocation)
    results["EmittedCKAngles"] = emitChAngles.EmittedCherenkovAnglesLocation

    # Cherenkov mass cones using emitted spectra
    emitMassCones = EmittedMassCones(
        name="RichMassCones" + track_name,
        NRingPoints=options["NRayTracingRingPoints"],
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        CherenkovAnglesLocation=emitChAngles.EmittedCherenkovAnglesLocation)
    results["EmittedCKRings"] = emitMassCones.MassHypothesisRingsLocation

    # Detectable photon yields
    detY = DetectableYields(
        name="RichDetectableYields" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        EmittedSpectraLocation=emitY.EmittedPhotonSpectraLocation,
        MassHypothesisRingsLocation=emitMassCones.MassHypothesisRingsLocation)
    results["DetectableYields"] = detY.DetectablePhotonYieldLocation
    results["DetectableSpectra"] = detY.DetectablePhotonSpectraLocation

    # Geometrical Eff.
    geomEff = GeomEff(
        name="RichGeomEff" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        CherenkovAnglesLocation=emitChAngles.EmittedCherenkovAnglesLocation,
        MassHypothesisRingsLocation=emitMassCones.MassHypothesisRingsLocation)
    results["GeomEffs"] = geomEff.GeomEffsLocation
    results["GeomEffsPerPD"] = geomEff.GeomEffsPerPDLocation
    results["SegmentPhotonFlags"] = geomEff.SegmentPhotonFlagsLocation

    # Select final track segments
    tkSel = SelectTrackSegments(
        name="RichTkSegmentSel" + track_name,
        InTrackToSegmentsLocation=segments.TrackToSegmentsLocation,
        GeomEffsLocation=geomEff.GeomEffsLocation)
    results["SelectedTrackToSegments"] = tkSel.OutTrackToSegmentsLocation

    # Signal Photon Yields
    sigYields = SignalYields(
        name="RichSignalYields" + track_name,
        DetectablePhotonYieldLocation=detY.DetectablePhotonYieldLocation,
        DetectablePhotonSpectraLocation=detY.DetectablePhotonSpectraLocation,
        GeomEffsLocation=geomEff.GeomEffsLocation)
    results["SignalPhotonYields"] = sigYields.SignalPhotonYieldLocation
    results["SignalPhotonSpectra"] = sigYields.SignalPhotonSpectraLocation

    # Signal Cherenkov angles
    sigChAngles = SignalCherenkovAngles(
        name="RichSignalCKAngles" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        SignalPhotonSpectraLocation=sigYields.SignalPhotonSpectraLocation,
        SignalPhotonYieldLocation=sigYields.SignalPhotonYieldLocation)
    results["SignalCKAngles"] = sigChAngles.SignalCherenkovAnglesLocation

    # Track Resolutions
    tkRes = TrackCKRes(
        name="RichCKResolutions" + track_name,
        TrackSegmentsLocation=segments.TrackSegmentsLocation,
        SignalCherenkovAnglesLocation=sigChAngles.
        SignalCherenkovAnglesLocation,
        MassHypothesisRingsLocation=emitMassCones.MassHypothesisRingsLocation)
    results["CherenkovResolutions"] = tkRes.CherenkovResolutionsLocation

    return results


###############################################################################


@configurable
def make_rich_photons(track_name,
                      input_tracks,
                      options,
                      make_raw=default_raw_event):
    """
    Return reconstructed photon specific RICH data.

    Args:
        track_name    (str):  The name to assign to this configuration
        input_tracks  (dict): The input tracks to process
        options       (dict): The processing options to use
        make_raw            : The entity that provides the RawEvent to use (??)

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    part_props = configure_rich_particle_properties(options)

    # The conf dict to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # pixel and track reco
    pixel_conf = make_rich_pixels(make_raw=make_raw, options=options)
    track_conf = make_rich_tracks(
        track_name=track_name, input_tracks=input_tracks, options=options)

    # include in returned configuration
    results.update(pixel_conf)
    results.update(track_conf)

    # Photon Reco.
    photReco = PhotonReco(
        name="RichPhotonReco" + track_name,
        TrackSegmentsLocation=track_conf["TrackSegments"],
        CherenkovAnglesLocation=track_conf["SignalCKAngles"],
        CherenkovResolutionsLocation=track_conf["CherenkovResolutions"],
        TrackLocalPointsLocation=track_conf["TrackLocalPoints"],
        TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
        SegmentPhotonFlagsLocation=track_conf["SegmentPhotonFlags"],
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"])
    results["CherenkovPhotons"] = photReco.CherenkovPhotonLocation
    results["PhotonToParents"] = photReco.PhotonToParentsLocation

    # Predicted pixel signals
    photPredSig = PhotonPredSignal(
        name="RichPredPixelSignal" + track_name,
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"],
        TrackSegmentsLocation=track_conf["TrackSegments"],
        CherenkovPhotonLocation=photReco.CherenkovPhotonLocation,
        PhotonToParentsLocation=photReco.PhotonToParentsLocation,
        CherenkovAnglesLocation=track_conf["SignalCKAngles"],
        CherenkovResolutionsLocation=track_conf["CherenkovResolutions"],
        PhotonYieldLocation=track_conf["DetectableYields"])
    results["PhotonSignals"] = photPredSig.PhotonSignalsLocation

    return results


###############################################################################


@configurable
def make_rich_pids(track_name,
                   input_tracks,
                   options,
                   make_raw=default_raw_event):
    """
    Return RICH PID data.

    Args:
        track_name    (str):  The name to assign to this configuration
        input_tracks  (dict): The input tracks to process
        options       (dict): The processing options to use
        make_raw            : The entity that provides the RawEvent to use (??)

    Returns:
        dict of useful data locations.
    """

    # Configure the particle properties
    part_props = configure_rich_particle_properties(options)

    # conf dict to return
    results = {}

    # save the name for this set of results
    results["TrackName"] = track_name

    # pixel and track reco
    pixel_conf = make_rich_pixels(make_raw=make_raw, options=options)
    track_conf = make_rich_tracks(
        track_name=track_name, input_tracks=input_tracks, options=options)
    photon_conf = make_rich_photons(
        track_name=track_name,
        input_tracks=input_tracks,
        make_raw=make_raw,
        options=options)

    # include in returned configuration
    results.update(pixel_conf)
    results.update(track_conf)
    results.update(photon_conf)

    # Create working event summary of reco info
    recSum = RecSummary(
        name="RichRecSummary" + track_name,
        TrackSegmentsLocation=track_conf["TrackSegments"],
        TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
        PhotonToParentsLocation=photon_conf["PhotonToParents"],
        DetectablePhotonYieldLocation=track_conf["DetectableYields"],
        SignalPhotonYieldLocation=track_conf["SignalPhotonYields"],
        PhotonSignalsLocation=photon_conf["PhotonSignals"],
        RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"])
    results["SummaryTracks"] = recSum.SummaryTracksLocation
    results["Summarypixels"] = recSum.SummaryPixelsLocation

    # ==================================================================
    # RICH Global PID
    # ==================================================================

    # Initalise some default locations
    gpidInit = GPIDInit(
        name="RichGPIDInit" + track_name,
        SummaryTracksLocation=recSum.SummaryTracksLocation)

    # Cache PID and DLL locations by iteration
    PIDs = {}
    DLLs = {}
    PIDs[0] = gpidInit.TrackPIDHyposLocation
    DLLs[0] = gpidInit.TrackDLLsLocation

    # PID iterations
    for it in range(0, options["nLikelihoodIterations"]):

        itN = "It" + ` it `

        # Pixel backgrounds
        pixBkgs = PixelBackgrounds(
            name="RichPixBackgrounds" + itN + track_name,
            # Settings
            PDGroupSize=options["PDGroupSize"],
            IgnoreExpectedSignals=options["PDBackIgnoreExpSignals"][it],
            MinPixelBackground=options["PDBackMinPixBackground"][it],
            MaxPixelBackground=options["PDBackMaxPixBackground"][it],
            # Input data
            TrackPIDHyposLocation=PIDs[it],
            TrackToSegmentsLocation=track_conf["SelectedTrackToSegments"],
            TrackSegmentsLocation=track_conf["TrackSegments"],
            GeomEffsPerPDLocation=track_conf["GeomEffsPerPD"],
            DetectablePhotonYieldLocation=track_conf["DetectableYields"],
            RichSIMDPixelSummariesLocation=pixel_conf["RichSIMDPixels"])

        # Likelihood minimiser
        like = LkMinimiser(
            name="RichGPIDLikelihood" + itN + track_name,
            # Settings
            TrackFreezeOutDLL=options["TrackFreezeOutDLL"][it],
            FinalDLLCheck=options["FinalDLLCheck"][it],
            TrackForceChangeDLL=options["TrackForceChangeDLL"][it],
            LikelihoodThreshold=options["LikelihoodThreshold"][it],
            MaxTrackChangesPerIt=options["MaxTrackChangesPerIt"][it],
            MinSignalForNoLLCalc=options["MinSignalForNoLLCalc"][it],
            # Input data
            SummaryTracksLocation=recSum.SummaryTracksLocation,
            SummaryPixelsLocation=recSum.SummaryPixelsLocation,
            PixelBackgroundsLocation=pixBkgs.PixelBackgroundsLocation,
            TrackDLLsInputLocation=DLLs[it],
            TrackPIDHyposInputLocation=PIDs[it],
            PhotonToParentsLocation=photon_conf["PhotonToParents"],
            PhotonSignalsLocation=photon_conf["PhotonSignals"])

        # Save the outputs for the next it
        PIDs[it + 1] = like.TrackPIDHyposOutputLocation
        DLLs[it + 1] = like.TrackDLLsOutputLocation

        # save the last pixel backgrounds to the conf (for monitoring)
        results["RichPixelBackgrounds"] = pixBkgs.PixelBackgroundsLocation

    # Write the file RichPID objects
    writePIDs = WriteRichPIDs(
        name="RichPIDsWriter" + track_name,
        # Settings
        PIDVersion=options["PIDVersion"],
        # Inputs
        TracksLocation=track_conf["InputTracks"],
        SummaryTracksLocation=recSum.SummaryTracksLocation,
        TrackPIDHyposInputLocation=PIDs[options["nLikelihoodIterations"]],
        TrackDLLsInputLocation=DLLs[options["nLikelihoodIterations"]])
    results["RichPIDs"] = writePIDs.RichPIDsLocation

    # return the final dict
    return results


###############################################################################
