###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import Gaudi__Examples__IntDataConsumer, Gaudi__Examples__IntDataProducer
from PyConf.Algorithms import Gaudi__Hive__FetchDataFromFile
from PyConf.Algorithms import (
    CaloFutureRawToDigits, FutureCellularAutomatonAlg, CaloFutureShowerOverlap,
    CaloFutureClusterCovarianceAlg, InEcalFutureAcceptanceAlg,
    FuturePhotonMatchAlg, ClassifyPhotonElectronAlg, CaloFutureMergedPi0,
    InHcalFutureAcceptanceAlg, FutureInBremFutureAcceptanceAlg,
    FutureElectronMatchAlg, BremMatchAlgFuture, FutureTrack2EcalEAlg,
    FutureTrack2HcalEAlg, FutureClusChi22ID, FutureEcalChi22ID,
    BremChi22IDFuture, FutureEcalPIDeAlg, BremPIDeAlgFuture, FutureHcalPIDeAlg,
    FutureEcalPIDmuAlg, FutureHcalPIDmuAlg)
from PyConf.Tools import CaloFutureECorrection, CaloFutureSCorrection, CaloFutureLCorrection
from PyConf.application import default_raw_event
from Gaudi.Configuration import DEBUG
from RecoConf.hlt2_tracking import make_hlt2_tracks
from RecoConf.hlt1_tracking import make_pvs


#@configurable
def make_digits(raw):
    adc_alg = CaloFutureRawToDigits(
        name='FutureEcalZSup',
        RawEventLocation=raw,
        DetectorLocation='/dd/Structure/LHCb/DownstreamRegion/Ecal',
        ZSupMethod='2D',
        ZSupNeighbour=-5,
        ZSupThreshold=20)
    adcEcal = adc_alg.OutputAdcData
    digitsEcal = adc_alg.OutputDigitData

    adc_alg_Hcal = CaloFutureRawToDigits(
        name='FutureHcalZSup',
        RawEventLocation=raw,
        DetectorLocation='/dd/Structure/LHCb/DownstreamRegion/Hcal',
        ZSupMethod='1D',
        ZSupThreshold=4)
    adcHcal = adc_alg_Hcal.OutputAdcData
    digitsHcal = adc_alg_Hcal.OutputDigitData

    return {
        "adcEcal": adcEcal,
        "digitsEcal": digitsEcal,
        "adcHcal": adcHcal,
        "digitsHcal": digitsHcal
    }


def make_clusters(digits):
    ecalClustersRaw = FutureCellularAutomatonAlg(InputData=digits).OutputData

    ecalClustersOverlap = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw).OutputData

    ecalClusters = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersOverlap).OutputData

    return ecalClusters


def make_photons_and_electrons(tracks, clusters):
    inECALFuture = InEcalFutureAcceptanceAlg(Inputs=tracks).Output

    clusterMatch = FuturePhotonMatchAlg(
        Calos=clusters, Tracks=tracks, Filter=inECALFuture).Output

    photonElectronAlg = ClassifyPhotonElectronAlg(
        InputTable=clusterMatch,
        InputClusters=clusters,
        ElectrMaxChi2=25.0,
        ElectrMinEt=50.0,
        MinDigits=2,
        PhotonMinChi2=4.0,
        PhotonMinEt=50.0)
    photons = photonElectronAlg.OutputPhotons
    electrons = photonElectronAlg.OutputElectrons

    return {
        "inECAL": inECALFuture,
        "clusterMatch": clusterMatch,
        "photons": photons,
        "electrons": electrons,
    }


def make_acceptance(tracks):
    return {
        "inAccHcal": InHcalFutureAcceptanceAlg(Inputs=tracks).Output,
        "inAccEcal": InEcalFutureAcceptanceAlg(Inputs=tracks).Output,
        "inAccBrem": FutureInBremFutureAcceptanceAlg(Inputs=tracks).Output
    }


def make_track_calo(tracks, digitsEcal, digitsHcal, inAccEcal, inAccHcal):
    acc = make_acceptance(tracks)

    ecalE = FutureTrack2EcalEAlg(
        Inputs=tracks, Filter=inAccEcal, Digits=digitsEcal).Output

    hcalE = FutureTrack2HcalEAlg(
        Inputs=tracks, Filter=inAccHcal, Digits=digitsHcal).Output

    return {"ecalE": ecalE, "hcalE": hcalE, "acc": acc}


def make_merged_pi0(ecalClusters):
    mergedPi0 = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        PhotonTools=[
            CaloFutureECorrection(),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ])
    return {
        "ecalSplitClusters": mergedPi0.SplitClusters,
        "mergedPi0s": mergedPi0.MergedPi0s,
        "splitPhotons": mergedPi0.SplitPhotons
    }


def make_electron_and_brem_match(electrons, photons, tracks, inECAL,
                                 inAccBrem):
    electronMatch = FutureElectronMatchAlg(
        Calos=electrons, Tracks=tracks, Filter=inECAL)

    bremMatch = BremMatchAlgFuture(
        Calos=photons, Tracks=tracks, Filter=inAccBrem)

    return {"electronMatch": electronMatch, "bremMatch": bremMatch}


def make_chi2_and_pid(tracks, clusterMatch, electronMatch, bremMatch, ecalE,
                      hcalE):
    clusChi2 = FutureClusChi22ID(
        Tracks=tracks,
        Input=clusterMatch,
    ).Output

    ecalChi2 = FutureEcalChi22ID(
        Tracks=tracks,
        Input=electronMatch,
    )

    bremChi2 = BremChi22IDFuture(
        Tracks=tracks,
        Input=bremMatch,
    )

    ecalPIDe = FutureEcalPIDeAlg(Input=ecalChi2).Output

    bremPIDe = BremPIDeAlgFuture(Input=bremChi2).Output

    hcalPIDe = FutureHcalPIDeAlg(Input=hcalE).Output

    ecalPIDmu = FutureEcalPIDmuAlg(Input=ecalE).Output

    hcalPIDmu = FutureHcalPIDmuAlg(Input=hcalE).Output

    return {
        "clusChi2": clusChi2,
        "ecalPIDe": ecalPIDe,
        "bremPIDe": bremPIDe,
        "hcalPIDe": hcalPIDe,
        "ecalPIDmu": ecalPIDmu,
        "hcalPIDmu": hcalPIDmu,
    }


def make_calo(best_tracks, make_raw=default_raw_event):
    rawEvent = make_raw(["HcalPacked", "EcalPacked"])
    rawToDigitsOutput = make_digits(rawEvent)
    adcEcal = rawToDigitsOutput["adcEcal"]
    digitsEcal = rawToDigitsOutput["digitsEcal"]
    adcHcal = rawToDigitsOutput["adcHcal"]
    digitsHcal = rawToDigitsOutput["digitsHcal"]

    ecalClusters = make_clusters(digitsEcal)

    PhElOutput = make_photons_and_electrons(best_tracks, ecalClusters)
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]
    inECAL = PhElOutput["inECAL"]
    clusterMatch = PhElOutput["clusterMatch"]

    mergePi0Out = make_merged_pi0(ecalClusters)
    ecalSplitClusters = mergePi0Out["ecalSplitClusters"]
    mergedPi0s = mergePi0Out["mergedPi0s"]
    splitPhotons = mergePi0Out["splitPhotons"]

    acc = make_acceptance(best_tracks)
    track2CaloEOut = make_track_calo(best_tracks, digitsEcal, digitsHcal,
                                     acc["inAccEcal"], acc["inAccHcal"])
    ecalE = track2CaloEOut["ecalE"]
    hcalE = track2CaloEOut["hcalE"]
    acc = track2CaloEOut["acc"]

    matchAlgOut = make_electron_and_brem_match(electrons, photons, best_tracks,
                                               inECAL, acc["inAccBrem"])
    electronMatch = matchAlgOut["electronMatch"]
    bremMatch = matchAlgOut["bremMatch"]

    chi2PidOut = make_chi2_and_pid(best_tracks, clusterMatch, electronMatch,
                                   bremMatch, ecalE, hcalE)
    ecalPIDe = chi2PidOut["ecalPIDe"]
    bremPIDe = chi2PidOut["bremPIDe"]
    hcalPIDe = chi2PidOut["hcalPIDe"]
    ecalPIDmu = chi2PidOut["ecalPIDmu"]
    hcalPIDmu = chi2PidOut["hcalPIDmu"]
    clusChi2 = chi2PidOut["clusChi2"]

    return {
        "ecalClusters": ecalClusters,
        "ecalSplitClusters": ecalSplitClusters,
        "ecalPIDmu": ecalPIDmu,
        "hcalPIDe": hcalPIDe,
        "hcalPIDmu": hcalPIDmu,
        "ecalPIDe": ecalPIDe,
        "bremPIDe": bremPIDe,
        "clusChi2": clusChi2
    }
