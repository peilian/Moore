###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
 Script for accessing tuples of IP resolution checker to obtain the values of mean and width of IP resolution
 as well as a check of the IP error distribution and efficiencies in different chi2 cut.

 The resolution is fitted by a Gaussian function

 author: Peilian Li(peilian.li@cern.ch)
 date:   01/2020
'''

import os, sys
import argparse
import ROOT
from ROOT import TMultiGraph, TMath, TAxis, TH1, TLatex, TGaxis, TROOT, TSystem, TCanvas, TFile, TTree, TObject
from ROOT import kDashed, kRed, kGreen, kBlue, kCyan, kMagenta, kBlack, kOrange, kAzure, kTRUE, kFALSE, gPad, TH1F, TH2F, TF1
from ROOT import gDirectory, gROOT, gStyle, TStyle, TPaveText, TString
from ROOT import ROOT, vector, TGraphAsymmErrors
gROOT.SetBatch(True)


def get_colors():
    return [kBlack, kBlue, kGreen + 3, kMagenta + 2, kOrange, kCyan + 2]


def get_markers():
    return [20, 24, 21, 22, 23, 25]


def get_fillstyles():
    return [3004, 3003, 3325, 3144, 3244, 3444]


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def get_trees(tr, tf, label):
    for lab in label:
        tr[lab] = tf[lab].Get("TrackIPResolutionCheckerNT/tracks")
    return tr


def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--filename',
        nargs='+',
        default=[],
        help='input tuple files, including path')
    parser.add_argument(
        '--label',
        nargs='+',
        default=["IPchecker"],
        help='label of input files')
    parser.add_argument(
        '--outfile',
        default='IPresolution_plots.root',
        help='output file name ')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    return parser


def get_resolution(resx, resy, tr, label):

    resIPx = {}
    resIPy = {}
    resIPx_1 = {}
    resIPy_1 = {}
    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()
    cutAcc = "trueeta<5 && trueeta>2 && typeofprefix==0"
    print("Velo track from PV in acceptance: " + cutAcc)
    for i, lab in enumerate(label):
        ### typeofprefix==0 for track from PV and matched with MC
        resIPx[lab] = TH2F("resIPx_" + lab, "IPx:1/pt", 20, 0, 2.0, 100, -300,
                           300)
        resIPy[lab] = TH2F("resIPy_" + lab, "IPy:1/pt", 20, 0, 2.0, 100, -300,
                           300)
        resIPx[lab].SetMarkerStyle(1)
        resIPy[lab].SetMarkerStyle(1)
        varx = "recIPx*1000:1.0/truept"
        vary = "recIPy*1000:1.0/truept"
        print("IPx var: " + varx + " " + " IPy var: " + vary)
        tr[lab].Draw(varx + ">>resIPx_" + lab, cutAcc)
        tr[lab].Draw(vary + ">>resIPy_" + lab, cutAcc)
        resIPx[lab].FitSlicesY()
        resIPy[lab].FitSlicesY()
        resx[lab] = gDirectory.Get("resIPx_" + lab + "_2")
        resy[lab] = gDirectory.Get("resIPy_" + lab + "_2")
        resIPx_1[lab] = gDirectory.Get("resIPx_" + lab + "_1")
        resIPy_1[lab] = gDirectory.Get("resIPy_" + lab + "_1")
        for i in range(1, resIPx_1[lab].GetNbinsX()):
            print(
                lab + ": in 1/pT region: (" +
                format(resIPx_1[lab].GetBinLowEdge(i), '.2f') + ", " + format(
                    resIPx_1[lab].GetBinLowEdge(i) +
                    resIPx_1[lab].GetBinWidth(i), '.2f') + ") [c/GeV]" +
                " ---- " + "resIPx for mean : " +
                format(resIPx_1[lab].GetBinContent(i), '.2f') +
                "[\mum] width : " + format(resx[lab].GetBinContent(i), '.2f') +
                "[\mum] ---- " + "resIPy for mean : " +
                format(resIPy_1[lab].GetBinContent(i), '.2f') +
                "[\mum] width : " + format(resy[lab].GetBinContent(i), '.2f') +
                "[\mum]")

    return resx, resy


def PrCheckerIPresolution(filename, label, outfile, savepdf, plotstyle):

    sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
    from LHCbStyle import setLHCbStyle, set_style
    setLHCbStyle()
    from Legend import place_legend
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.05)

    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()
    tf = {}
    tr = {}
    tf = get_files(tf, filename, label)
    tr = get_trees(tr, tf, label)

    outputfile = TFile(outfile, "recreate")
    outputfile.cd()

    gr_resx = {}
    gr_resy = {}
    gr_resx, gr_resy = get_resolution(gr_resx, gr_resy, tr, label)

    canvas1 = TCanvas("resIPx_pT", "resIPx v.s. 1/pT")
    canvas2 = TCanvas("resIPy_pT", "resIPy v.s. 1/pT")
    canvas = TCanvas("IPChi2", "IPChi2")
    canvas.SetLogy()

    canvas1.cd()
    latex.DrawLatex(0.7, 0.85, "LHCb simultaion")

    polx = {}
    Par0x = {}
    Par1x = {}
    Funcx = {}
    gr_resx[label[0]].GetYaxis().SetTitle("#sigma_{IPx} [#mum]")
    gr_resx[label[0]].GetXaxis().SetTitle("1/p_{T} [c/GeV]")
    gr_resx[label[0]].SetTitle("#sigma(IP_{x}):1/p_{T}")
    for idx, lab in enumerate(label):
        set_style(gr_resx[lab], colors[idx], markers[idx], 0)
        if idx == 0:
            gr_resx[lab].Draw("E1 p1")
        else:
            gr_resx[lab].Draw("E1 p1 same")
        polx[lab] = TF1("polx_" + lab, "pol1", 0, 3)
        polx[lab].SetLineColor(colors[idx])
        print(lab + ": Fit to Resolution of IPx v.s. 1/pT:")
        gr_resx[lab].Fit("polx_" + lab, "R")
        Par0x[lab] = format(polx[lab].GetParameter(0), '.2f')
        Par1x[lab] = format(polx[lab].GetParameter(1), '.2f')
        Funcx[lab] = lab + ": " + Par0x[lab] + "+" + Par1x[lab] + "/p_{T}"
        gr_resx[lab].SetTitle(Funcx[lab])
    place_legend(
        canvas1, 0.5, 0.2, 0.85, 0.45, header="LHCb simulation", option="lep")
    canvas1.SetRightMargin(0.05)
    canvas1.Write()
    if savepdf:
        canvas1.SaveAs("resIPx_pt.pdf")

    canvas2.cd()
    latex.DrawLatex(0.7, 0.85, "LHCb simultaion")
    poly = {}
    Par0y = {}
    Par1y = {}
    Funcy = {}
    gr_resy[label[0]].GetYaxis().SetTitle("#sigma_{IPy} [#mum]")
    gr_resy[label[0]].GetXaxis().SetTitle("1/p_{T} [c/GeV]")
    gr_resy[label[0]].SetTitle("#sigma(IP_{y}):1/p_{T}")
    for idx, lab in enumerate(label):
        set_style(gr_resy[lab], colors[idx], markers[idx], 0)
        if idx == 0:
            gr_resy[lab].Draw("E1 p1")
        else:
            gr_resy[lab].Draw("E1 p1 same")
        poly[lab] = TF1("poly_" + lab, "pol1", 0, 3)
        poly[lab].SetLineColor(colors[idx])
        print(lab + ": Fit to Resolution of IPy v.s. 1/pT:")
        gr_resy[lab].Fit("poly_" + lab, "R")
        Par0y[lab] = format(poly[lab].GetParameter(0), '.2f')
        Par1y[lab] = format(poly[lab].GetParameter(1), '.2f')
        Funcy[lab] = lab + ": " + Par0y[lab] + "+" + Par1y[lab] + "/p_{T}"
        gr_resy[lab].SetTitle(Funcy[lab])
    place_legend(
        canvas2, 0.5, 0.2, 0.85, 0.45, header="LHCb simulation", option="lep")
    canvas2.SetRightMargin(0.05)
    canvas2.Write()
    if savepdf:
        canvas2.SaveAs("resIPy_pt.pdf")

    #### rec IP chi2 distribution
    recIPChi2 = {}
    var = "recIPChi2"
    cutAcc = "trueeta<5 && trueeta>2 && typeofprefix==0"
    for i, lab in enumerate(label):
        recIPChi2[lab] = TH1F("recIPChi2_" + lab, lab, 100, 0, 200)
        tr[lab].Draw(var + ">>recIPChi2_" + lab, cutAcc)
        recIPChi2[lab].SetLineColor(colors[i])
        recIPChi2[lab].SetMarkerColor(colors[i])
    canvas.cd()
    latex.DrawLatex(0.7, 0.85, "LHCb simultaion")
    recIPChi2[label[0]].Draw()
    recIPChi2[label[0]].SetXTitle("#chi^{2}_{IP}")
    recIPChi2[label[0]].SetYTitle("N_{tracks}")
    for lab in label[1:]:
        recIPChi2[lab].Draw("hist same")

    place_legend(canvas, 0.5, 0.5, 0.8, 0.7, "", "lep")
    canvas.SetRightMargin(0.05)
    canvas.Write()
    if savepdf:
        canvas.SaveAs("recIPchi2.pdf")

    for lab in label:
        ### print efficiency of different IPchi2 cut
        Numerator = TH1F("Numerator", "Numerator", 100, 0, 200)
        Denominator = TH1F("Denominator", "Denominator", 100, 0, 200)
        tr[lab].Draw(var + ">>Denominator", cutAcc)
        for cut in [100, 25, 16, 9, 4]:
            cutChi2 = " && recIPChi2<" + format(cut)
            cutNum = cutAcc + cutChi2
            tr[lab].Draw(var + ">>Numerator", cutNum)
            if Numerator.GetEntries() == 0 or Denominator.GetEntries() == 0:
                continue
            Eff = Numerator.GetEntries() / (Denominator.GetEntries() + 0.0)
            print(lab + ": Efficiency of IPchi2<" + format(cut) + " : " +
                  format(Eff, '.2%'))

    outputfile.Write()
    outputfile.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    PrCheckerIPresolution(**vars(args))
