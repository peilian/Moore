###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# Script for accessing histograms of reconstructible and
# reconstructed tracks for different tracking categories
# created by hlt1_reco_baseline_with_mcchecking.py and
# hlt1_reco_baseline_and_Allen_with_mcchecking.py
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Dorothea vom Bruch (dorothea.vom.bruch@cern.ch)
# date:   10/2018
# updated by Peilian Li
#

import os, sys
import argparse
import ROOT
from ROOT import TMultiGraph, TMath, TAxis, TH1, TLatex, TGaxis, TROOT, TSystem, TCanvas, TFile, TTree, TObject
from ROOT import kDashed, kGray, kOrange, kMagenta, kRed, kGreen, kCyan, kBlue, kBlack, kAzure, kTRUE, kFALSE, gPad, TPad
from ROOT import gROOT, gStyle, TStyle, TPaveText, TH1F
from ROOT import ROOT, TH1D, TH1F, TGraphAsymmErrors, TEfficiency
import logging
import subprocess
gROOT.SetBatch(True)


def getEfficiencyHistoNames():
    return ["eta", "p", "pt", "phi", "nPV"]


def getOriginFolders():
    basedict = {"Forward": {}, "MuonMatch": {}}
    basedict["Forward"]["folder"] = "ForwardTrackChecker/"
    basedict["MuonMatch"]["folder"] = "MuonMatchTrackChecker/"

    return basedict


def get_colors():
    return [kBlack, kBlue, kGreen + 3, kMagenta + 2, kOrange, kCyan + 2]


def get_markers():
    return [20, 24, 21, 22, 23, 25]


def get_fillstyles():
    return [3004, 3003, 3325, 3144, 3244, 3444]


def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--filename',
        type=str,
        default=['PrChecker_MuonID_MiniBias.root'],
        nargs='+',
        help='input files, including path')
    parser.add_argument(
        '--outfile',
        type=str,
        default='MuonIDefficiency_plots.root',
        help='output file')
    parser.add_argument(
        '--label', nargs='+', default=["MuonIDEff"], help='label for files')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    parser.add_argument(
        '--printval',
        default=True,
        action='store_true',
        help='print out the muon ID efficiency in momentum bins ')
    return parser


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def get_eff(eff, hist, tf, histoName, histoName_Den, label, var, printval):
    eff = {}
    hist = {}
    for i, lab in enumerate(label):
        numeratorName = histoName + "_reconstructed"
        numerator = tf[lab].Get(numeratorName)
        denominatorName = histoName_Den + "_reconstructed"
        denominator = tf[lab].Get(denominatorName)
        if numerator.GetEntries() == 0 or denominator.GetEntries() == 0:
            continue

        teff = TEfficiency(numerator, denominator)
        teff.SetStatisticOption(7)
        eff[lab] = teff.CreateGraph()
        eff[lab].SetName(lab)
        eff[lab].SetTitle(lab + "not elec.")
        if (histoName.find('strange') != -1):
            eff[lab].SetTitle(lab + " from stranges")
        if (histoName.find('electron') != -1):
            eff[lab].SetTitle(lab + " electron")
        hist[lab] = denominator.Clone()
        hist[lab].SetName("h_numerator_notElectrons")
        hist[lab].SetTitle(var + " histo. not elec.")
        if (histoName.find('strange') != -1):
            hist[lab].SetTitle(var + " histo. stranges")
        if (histoName.find('electron') != -1):
            hist[lab].SetTitle(var + " histo. electron")

        #print the muonID efficiency in each p bin
        if var == "p" and printval:
            for i in range(1, numerator.GetNbinsX()):
                if denominator.GetBinContent(i) == 0:
                    continue
                Eff = numerator.GetBinContent(i) / (
                    denominator.GetBinContent(i) + 0.0)
                print(lab + ": Efficiency of muon ID in (" +
                      format(numerator.GetBinLowEdge(i), '.2f') + ", " +
                      format(
                          numerator.GetBinLowEdge(i) +
                          numerator.GetBinWidth(i), '.2f') + ") [GeV/c] : " +
                      format(Eff, '.2%'))

    return eff, hist


def PrCheckerMuonIDEff(filename, outfile, label, plotstyle, savepdf, printval):
    sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
    from LHCbStyle import setLHCbStyle, set_style
    from ConfigHistos import (efficiencyHistoDict, categoriesDict, getCuts)
    from Legend import place_legend
    setLHCbStyle()

    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()

    tf = {}
    tf = get_files(tf, filename, label)
    outputfile = TFile(outfile, "recreate")

    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.055)

    efficiencyHistoDict = efficiencyHistoDict()
    efficiencyHistos = getEfficiencyHistoNames()
    categories = categoriesDict()
    cuts = getCuts()
    folders = getOriginFolders()

    tracker = "MuonMatch"
    outputfile.cd()
    trackerDir = outputfile.mkdir(tracker)
    trackerDir.cd()

    for cut in cuts[tracker]:
        cutDir = trackerDir.mkdir(cut)
        cutDir.cd()
        folder = folders[tracker]["folder"]
        print(folder)
        histoBaseName = "Track/" + folder + tracker + "/" + cut + "_"
        histoBaseName_den = "Track/" + "ForwardTrackChecker/Forward/" + cut + "_"

        # calculate efficiency
        for histo in efficiencyHistos:
            canvastitle = "efficiency vs. " + histo + ", " + categories[
                tracker][cut]["title"]
            name = "efficiency_" + histo + "_" + cut
            # get efficiency for not electrons category
            histoName = histoBaseName + "" + efficiencyHistoDict[histo][
                "variable"]
            histoName_den = histoBaseName_den + "" + efficiencyHistoDict[
                histo]["variable"]
            print("not electrons: " + histoName)
            eff = {}
            hist_den = {}
            eff, hist_den = get_eff(eff, hist_den, tf, histoName,
                                    histoName_den, label, histo, printval)

            canvas = TCanvas(name, canvastitle)
            mg = TMultiGraph()
            for i, lab in enumerate(label):
                mg.Add(eff[lab])
                set_style(eff[lab], colors[i], markers[i], styles[i])
            mg.Draw("AP")
            mg.GetYaxis().SetRangeUser(0, 1.05)
            xtitle = efficiencyHistoDict[histo]["xTitle"]
            mg.GetXaxis().SetTitle(xtitle)
            mg.GetYaxis().SetTitle("Muon ID Efficiency")
            for i, lab in enumerate(label):
                rightmax = 1.05 * hist_den[lab].GetMaximum()
                scale = gPad.GetUymax() / rightmax
                hist_den[lab].Scale(scale)
                if i == 0:
                    set_style(hist_den[lab], kGray + 1, markers[i], styles[i])
                else:
                    set_style(hist_den[lab], colors[i] - 10, markers[i],
                              styles[i])
                hist_den[lab].Draw("hist SAME")

            canvas.PlaceLegend()
            for lab in label:
                eff[lab].Draw("P SAME")
            cutName = categories[tracker][cut]["title"]
            latex.DrawLatex(0.35, 0.3, cutName)
            latex.DrawLatex(0.7, 0.85, "LHCb simulation")

            canvas.SetRightMargin(0.05)
            canvas.Write()
            if savepdf:
                canvasName = "MuonIDEff_" + cut + "_" + histo + ".pdf"
                canvas.SaveAs(canvasName)
            canvas.SetRightMargin(0.05)
            canvas.Write()

    outputfile.Write()
    outputfile.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    PrCheckerMuonIDEff(**vars(args))
